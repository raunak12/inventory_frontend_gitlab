# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"
set :application, "inventory-frontend"
set :repo_url, "git@bitbucket.org:codysseynepal/inventory-frontend.git"
set :user, 'root'
# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, 'master'
# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :dist, 'build'
# set :dist, "../dist"
# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true
set :use_sudo,        false
set :deploy_via,      :remote_cache

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
set :keep_releases, 5
# set :scm, nil
# Uncomment the following to require manually verifying the host key before first deploy.
set :ssh_options,     forward_agent: true, user: fetch(:user), keys: %w[~/.ssh/id_rsa.pub]
set :build, "build"
