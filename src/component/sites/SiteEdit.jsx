import React, { Component } from 'react';
import { Form, Input } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import {siteActions} from '../../actions/SiteActions';

class SiteEdit extends Component {
  constructor() {
    super();
    this.state = { site: {} };

  }

  componentDidMount() {
    siteActions.showSite(this.props.match.params.id).then( response => {
      this.setState({site: response.data});
    }).catch(error => console.log('error', error));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
    if(!err) {
      siteActions.updateSite(this.state.site.id, values).then( response => {
        if(response.status === 204){
          this.props.history.push(`/sites`);
          success('Site was successfully Updated!')
        }else{
          error('Unable to Update the site!')
        }
      }).catch(error => console.log('error', error));
      }
    })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCancel() {
    this.props.history.push(`/sites`);
  }

  render() {
    const { form } =  this.props
    return (
      <CustomLayout>
        <h1>Edit Site</h1>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <Form.Item label= 'Name' >
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input site name!'}],
                initialValue: this.state.site.name
              })(
                <Input
                  placeholder='Site Name'
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Contact Person'>
            {  
              form.getFieldDecorator('contact_person', {
                rules: [{ required: true, message: 'Please input contact person to this site!'}],
                initialValue: this.state.site.contact_person
              })(
                <Input
                  placeholder='Full Name'
                  name='contact_person'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Phone Number'>
            {  
              form.getFieldDecorator('phone_no', {
                rules: [{ required: true, message: 'Please input valid phone number!'}],
                initialValue: this.state.site.phone_no
              })(
                <Input
                  placeholder='Phone Number'
                  name='phone_no'
                />
              )
            }
          </Form.Item>

          <div className="btn-group">
            <button type="submit" className="btn btn-dark">Update Site</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(SiteEdit);
