import React, { Component } from 'react';
import { Form, Input } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { siteActions } from '../../actions/SiteActions';

class SiteAdd extends Component {
  constructor() {
    super();
    this.state = { name: '', contact_person: '', phone_no: ''};
  }

  handleSubmit(event) {
  event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if(!err) {
        siteActions.createSite(values).then( response => {
          if(response.status === 201){
            success('Site was successfully created!')
            this.props.history.push(`/sites`);
          }else{
            error('Unable to create site!')
          }
        }).catch(error => console.log('error', error));
      }
    })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCancel() {
    this.props.history.push("/sites");
  }

  render() {
    const {form} = this.props
    return (
      <CustomLayout> 
        <Form onSubmit= { this.handleSubmit.bind(this) }>
          <Form.Item label='Name' className='short-text-field'>
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input site name!' }],
              })(
                <Input
                  placeholder="Site Name"
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Contact Person' className='short-text-field'>
            {
              form.getFieldDecorator('contact_person', {
                rules: [{ required: true, message: 'Please input contact person to this site!' }],
              })(
                <Input
                  placeholder="Full Name"
                  name='contact_person'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Phone Number' className='short-text-field'>
            {
              form.getFieldDecorator('phone_no', {
                rules: [{ required: true, message: 'Please input valid contact number!' }],
              })(
                <Input
                  placeholder="Phone Number"
                  name='phone_no'
                />
              )
            }
          </Form.Item>
          <div className='btn-group'>
            <button type="submit" className="btn btn-dark">Create Site</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(SiteAdd);
