import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CustomLayout from '../Layouts/CustomLayout'
import Table from './table'
import {siteActions} from '../../actions/SiteActions'
import { isAdmin } from '../../helpers/userPolicy'
import LoadingSpinner from '../../helpers/LoadingSpinner';

class SitesList extends Component {
  constructor() {
    super()
    this.state = { sites: [], isLoading: true }
  }

  componentDidMount() {
    siteActions.fetchSites().then( response => {
      this.setState({ sites: response.data, isLoading: false })
    }).catch(error => console.log('error', error))
  }

   showTable() {
    const { sites } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <Table table_data={sites} />
      )
    }
  }

  render() {
    const { sites } = this.state
    if (sites === null) return null
    return (
      <CustomLayout sidebarSelectedKey='sites'>
        <h1>Sites</h1>
        { isAdmin() ? 
        <Link to="/sites/new" className="btn btn-outline-primary mar-pad float-right">
          Create Site
        </Link>
        : ''
        }
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default SitesList
