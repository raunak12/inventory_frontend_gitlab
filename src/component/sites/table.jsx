import React, { Component } from "react";
import {
  Table,
  Icon,
  Divider,
  Modal,
  Button,
  Row,
  Col,
  Descriptions
} from "antd";
import { Link } from "react-router-dom";
import { success, error } from "../../helper/notifications";
import { siteActions } from "../../actions/SiteActions";
import { sortText } from "../../helpers/sort";
import { getColumnSearchProps } from "../../helpers/tableSearch";
import { isAdmin } from '../../helpers/userPolicy'

const { confirm } = Modal;

class SiteTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      sites: props.table_data,
      site: {}
    };
  }

  handleDelete(siteId) {
    siteActions.deleteSite(siteId).then(response => {
      if (response.status === 204) {
        siteActions
          .fetchSites()
          .then(response => {
            this.setState({ sites: response.data });
          })
          .catch(error => console.log("error", error));
        success("Site was successfully deleted!");
      } else {
        error("Unable to delete Site!");
      }
    });
  }
  showDeleteConfirm(siteId) {
    confirm({
      className: "show-modal-delete",
      title: "Are you sure to delete this Site?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk: () => this.handleDelete(siteId),
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }

  setSite(response) {
    
  }

  showModal = value => {
    siteActions.showSite(value).then(response => {
      this.setState({
        site: response.data,
        visible: true
      });
    });
   
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  render() {
    const columns = [
      {
        title: "Site Name",
        dataIndex: "name",
        key: "name",
        sorter: (a, b) => sortText(a.name, b.name),
        ...getColumnSearchProps("name", this)
      },
      {
        title: "Contact Person",
        dataIndex: "contact_person",
        key: "contact_person",
        sorter: (a, b) => sortText(a.contact_person, b.contact_person),
        ...getColumnSearchProps("contact_person", this)
      },
      {
        title: "Phone Number",
        dataIndex: "phone_no",
        key: "phone_no",
        ...getColumnSearchProps("phone_no", this)
      },
      {
        title: "",
        width: "8%",
        key: "action",
        render: (text, record) => {
          return (
            <span>
              {
                isAdmin() ?
                  <Link to={`sites/${record.id}/edit`}>
                    <Icon title="Edit" type="edit" />
                  </Link> : ''
              }
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon onClick={() => this.showModal(`${record.id}`)} type="snippets" title="View" />
              </span>
              <Divider type="vertical" />
              {
                isAdmin() ?
                  <span style={{ color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent' }}>
                    <Icon
                      type="delete"
                      title="Delete"
                      onClick={() => this.showDeleteConfirm(record.id)}
                    />
                  </span> : ''
              }
              
            </span>
          );
        }
      }
    ];
    const sites = this.state.sites;
    const view_table =
      sites && sites.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={sites} size="middle" rowKey={column=> column.name} />
        </div>
      ) : (
        "No Data Found"
      );
    return (
      <div>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>
          ]}
        >
          <Row>
            <Col span={24} offset={6}>
              <h3> SITE DETAIL</h3>
            </Col>
          </Row>
          <Descriptions title="" bordered>
            <Descriptions.Item label="Site Name">
              {this.state.site.name}
            </Descriptions.Item>
            <Descriptions.Item label="Contact Person">
              {this.state.site.contact_person}
            </Descriptions.Item>
            <Descriptions.Item label="Phone Number">
              {this.state.site.phone_no}
            </Descriptions.Item>
          </Descriptions>
        </Modal>
        {view_table}
      </div>
    );
  }
}
export default SiteTable;
