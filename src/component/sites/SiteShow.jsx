import React, { Component } from 'react';
import CustomLayout from '../Layouts/CustomLayout';
import { Link } from 'react-router-dom';
import { success, error } from '../../helper/notifications';
import { Modal } from 'antd';
import { siteActions } from '../../actions/SiteActions'
import { isAdmin } from '../../helpers/userPolicy'

const { confirm } = Modal;

class SiteShow extends Component {
  constructor() {
    super();
    this.state = { site: {} };
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    siteActions.showSite(this.props.match.params.id).then(response => {
      this.setState({ site: response.data })
    }).catch(error => console.log('error', error));
  }

  handleDelete() {
    siteActions.deleteSite(this.state.site.id).then(response => {
      if (response.status === 204) {
        success('Site was successfully deleted!')
        this.props.history.push("/sites")
      } else {
        error('Unable to delete a site!')
      }
    }).catch(error => console.log('error', error));
  }

  showDeleteConfirm() {
    confirm({
      title: 'Are you sure to delete this Site?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleDelete.bind(this),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  render() {
    return (
      <CustomLayout>
        <table className="table table-sm view-table">
          <tr>
            <td><h5>Contact Person:</h5></td>
            <td><h5>{this.state.site.contact_person}</h5></td>
            <td><h5>Site Name: </h5></td>
            <td><h5>{this.state.site.name}</h5></td>
          </tr>
          <tr>
            <td><h5>Contractor Contact Number:</h5></td>
            <td><h5>{this.state.site.phone_no}</h5></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>
              {
                isAdmin() ?
                  <p>
                    <Link to={`/sites/${this.state.site.id}/edit`} className="btn btn-primary">Edit</Link>
                    <button onClick={this.showDeleteConfirm.bind(this)} className="btn btn-danger">Delete</button>
                  </p> : ''
              }
              <Link to="/sites" className="btn btn-primary">Back</Link>
            </td>
            <td></td>
            <td></td>
            <td></td>
          </tr>

        </table>
      </CustomLayout>
    )
  }
}

export default SiteShow;