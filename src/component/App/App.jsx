import React, { Component } from 'react'
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom'
import { history } from '../../helpers/history'
import {PrivateRoute} from '../PriavteRoute.jsx'
import LoginPage from '../Login/LoginForm'

import Dashboard from '../dashboard/Dashboard'
import CategoriesList from '../categories/CategoriesList'
import CategoriesAdd from '../categories/CategoriesAdd'
import CategoryShow from '../categories/CategoryShow'
import CategoryEdit from '../categories/CategoryEdit'

import ProductList from '../products/ProductList'
import ProductAdd from '../products/ProductAdd'
import ProductShow from '../products/ProductShow'
import ProductEdit from '../products/ProductEdit'

import VendorLists from '../vendors/VendorLists'
import VendorAdd from '../vendors/VendorAdd'
import VendorShow from '../vendors/VendorShow'
import VendorEdit from '../vendors/VendorEdit'

import SiteList from '../sites/SitesList'
import SiteAdd from '../sites/SiteAdd'
import SiteShow from '../sites/SiteShow'
import SiteEdit from '../sites/SiteEdit'

import ContractorLists from '../contractors/ContractorLists'
import ContractorAdd from '../contractors/ContractorAdd'
import ContractorShow from '../contractors/ContractorShow'
import ContractorEdit from '../contractors/ContractorEdit'

import RepatriationList from '../repatriations/RepatriationList'
import AddRepatriation from '../repatriations/AddRepatriation'
import ShowRepatriation from '../repatriations/ShowRepatriation'
import EditRepatriation from '../repatriations/EditRepatriation'

import PurchaseLists from '../BillForm/PurchaseLists'
import AddBill from '../BillForm/AddBill'
import ShowPurchase from '../BillForm/ShowPurchase'
import EditPurchase from '../BillForm/EditPurchase'

import DispatchList from '../Dispatch/DispatchList'
import DispatchAdd from '../Dispatch/DispatchAdd'
import DispatchShow from '../Dispatch/DispatchShow'
import DispatchEdit from '../Dispatch/DispatchEdit'

import Reportlists from '../reports/Reportlists'
import ConsumedLists from '../consumed/ConsumedLists';
import ConsumedAdd from '../consumed/ConsumedAdd';
import ViewConsumed from '../consumed/ViewConsumed';
import ConsumedEdit from '../consumed/ConsumedEdit';

class App extends Component {
  componentDidMount() {
    document.title = "INVENTORY MANAGEMENT SYSTEM"
  }

  render() {
    return (
      <Router>
        <Switch history={history}>
          <Route exact path="/login" component={LoginPage} />
          <PrivateRoute exact path="/" component={Dashboard} />
          <PrivateRoute exact path="/categories" component={CategoriesList} />
          <PrivateRoute exact path="/categories/new" component={CategoriesAdd} />
          <PrivateRoute exact path="/categories/:id/show" component={CategoryShow} />
          <PrivateRoute exact path="/categories/:id/edit" component={CategoryEdit} />

          <PrivateRoute exact path="/products" component={ProductList} />
          <PrivateRoute exact path="/products/new" component={ProductAdd} />
          <PrivateRoute exact path="/products/:id/show" component={ProductShow} />
          <PrivateRoute exact path="/products/:id/edit" component={ProductEdit} />

          <PrivateRoute exact path="/vendors" component={VendorLists} />
          <PrivateRoute exact path="/vendors/new" component={VendorAdd} />
          <PrivateRoute exact path="/vendors/:id/show" component={VendorShow} />
          <PrivateRoute exact path="/vendors/:id/edit" component={VendorEdit} />

          <PrivateRoute exact path="/sites" component={SiteList} />
          <PrivateRoute exact path="/sites/new" component={SiteAdd} />
          <PrivateRoute exact path="/sites/:id/show" component={SiteShow} />
          <PrivateRoute exact path="/sites/:id/edit" component={SiteEdit} />

          <PrivateRoute exact path="/contractors" component={ContractorLists} />
          <PrivateRoute exact path="/contractors/new" component={ContractorAdd} />
          <PrivateRoute exact path="/contractors/:id/show" component={ContractorShow} />
          <PrivateRoute exact path="/contractors/:id/edit" component={ContractorEdit} />

          <PrivateRoute exact path="/repatriations" component={RepatriationList} />
          <PrivateRoute exact path="/repatriations/new" component={AddRepatriation} />
          <PrivateRoute exact path="/repatriated_items/:id/show" component={ShowRepatriation} />
          <PrivateRoute exact path="/repatriations/:id/edit" component={EditRepatriation} />

          <PrivateRoute exact path="/bills" component={PurchaseLists} />
          <PrivateRoute exact path="/bills/new" component={AddBill} />
          <PrivateRoute exact path="/bill_items/:id/show" component={ShowPurchase} />
          <PrivateRoute exact path="/bills/:id/edit" component={EditPurchase} />


          <PrivateRoute exact path="/dispatches" component={DispatchList} />
          <PrivateRoute exact path="/dispatches/new" component={DispatchAdd} />
          <PrivateRoute exact path="/dispatched_items/:id/show" component={DispatchShow} />
          <PrivateRoute exact path="/dispatches/:id/edit" component={DispatchEdit} />

          <PrivateRoute exact path="/consumptions" component={ConsumedLists} />
          <PrivateRoute exact path="/consumptions/new" component={ConsumedAdd} />
          <PrivateRoute exact path="/consumed_items/:id/show" component={ViewConsumed} />
          <PrivateRoute exact path="/consumptions/:id/edit" component={ConsumedEdit} />


          <PrivateRoute exact path="/reports" component={Reportlists} />

          <PrivateRoute path="/events/:id?" component={ProductAdd} />
        </Switch>
      </Router>

    )
  }
}

export default App
