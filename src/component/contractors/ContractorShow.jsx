import React, { Component } from 'react';
import CustomLayout from '../Layouts/CustomLayout';
import { Link } from 'react-router-dom';
import { success, error } from '../../helper/notifications';
import { Modal } from 'antd';
import { contractorActions } from '../../actions/ContractorActions';
import { isAdmin } from '../../helpers/userPolicy';

const { confirm } = Modal;

class ContractorShow extends Component {
  constructor() {
    super();
    this.state = { contractor: {} };
  }

  componentDidMount() {
    contractorActions.showContractor(this.props.match.params.id).then(response => {
      this.setState({ contractor: response.data });
    }).catch(error => console.log('error', error));
  }

  handleDelete() {
    contractorActions.deleteContractor(this.state.contractor.id).then(response => {
      if (response.status === 204) {
        success('Contractor was successfully deleted!')
        this.props.history.push("/contractors")
      } else {
        error('Unable to delete a contractor!')
      }
    }).catch(error => console.log('error', error));
  }

  showDeleteConfirm() {
    confirm({
      title: 'Are you sure to delete this Contractor?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleDelete.bind(this),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  render() {
    return (
      <CustomLayout>
        <table className="table table-sm view-table">
          <tr>
            <td><h5>Contractor Name:</h5></td>
            <td><h5>{this.state.contractor.name}</h5></td>
            <td><h5>Contractor Address: </h5></td>
            <td><h5>{this.state.contractor.address}</h5></td>
          </tr>
          <tr>
            <td><h5>Contractor Contact Number:</h5></td>
            <td><h5>{this.state.contractor.phone_no}</h5></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>
              {
                isAdmin() ? 
                <p>
                <Link to={`/contractors/${this.state.contractor.id}/edit`} className="btn btn-primary">Edit</Link>
              <button onClick={this.showDeleteConfirm.bind(this)} className="btn btn-danger">Delete</button> 
              </p>: ''
              }
              <Link to="/contractors" className="btn btn-primary">Back</Link>
            </td>
            <td></td>
            <td></td>
            <td></td>
          </tr>

        </table>
        <p>

        </p>
        <hr />
      </CustomLayout>
    )
  }
}

export default ContractorShow;