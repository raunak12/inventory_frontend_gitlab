import React, { Component } from "react";
import {
  Table,
  Icon,
  Divider,
  Modal,
  Button,
  Row,
  Col,
  Descriptions
} from "antd";
import { Link } from "react-router-dom";
import { success, error } from "../../helper/notifications";
import { contractorActions } from "../../actions/ContractorActions";
import { sortText } from "../../helpers/sort";
import { getColumnSearchProps } from "../../helpers/tableSearch";
import { isAdmin } from '../../helpers/userPolicy'

const { confirm } = Modal;

class ContractorTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      contractors: props.table_data,
      contractor: {}
    };
  }

  handleDelete(contractorId) {
    contractorActions.deleteContractor(contractorId).then(response => {
      if (response.status === 204) {
        contractorActions
          .fetchContractors()
          .then(response => {
            this.setState({ contractors: response.data });
          })
          .catch(error => console.log("error", error));
        success("Contractor was successfully deleted!");
      } else {
        error("Unable to delete Contractor!");
      }
    });
  }
  showDeleteConfirm(contractorId) {
    confirm({
      className: "show-modal-delete",
      title: "Are you sure to delete this Contractor?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk: () => this.handleDelete(contractorId),
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }

  showModal = value => {
    contractorActions.showContractor(value).then(response=> {
      this.setState({
        contractor: response.data,
        visible: true
      });
    });
    
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  render() {
    const columns = [
      {
        title: "Contractor Name",
        dataIndex: "name",
        key: "name",
        sorter: (a, b) => sortText(a.name, b.name),
        ...getColumnSearchProps("name", this)
      },
      {
        title: "Address",
        dataIndex: "address",
        key: "address",
        sorter: (a, b) => sortText(a.address, b.address),
        ...getColumnSearchProps("address", this)
      },
      {
        title: "Phone Number",
        dataIndex: "phone_no",
        key: "phone_no",
        ...getColumnSearchProps("address", this)
      },
      {
        title: "",
        width: "8%",
        key: "action",
        render: (text, record) => {
          return (
            <span>
              {
                isAdmin() ? 
                  <Link to={`contractors/${record.id}/edit`}>
                    <Icon title="Edit" type="edit" />
                  </Link>
                : ''
              }
              
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon 
                  type="snippets" 
                  title="View"
                  onClick={() => this.showModal(record.id)}
                />
              </span>
              <Divider type="vertical" />
              {
                isAdmin() ?
                  <span style={{ color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent' }}>
                    <Icon
                      type="delete"
                      title="Delete"
                      onClick={() => this.showDeleteConfirm(record.id)}
                    />
                  </span>
                :''
              }
            
            </span>
          );
        }
      }
    ];
    const contractors = this.state.contractors;
    const view_table =
      contractors && contractors.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={contractors} size="middle" rowKey={key=> key.name} />
        </div>
      ) : (
        "No Data Found"
      );
    return (
      <div>
        {view_table}
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>
          ]}
        >
          <Row>
            <Col span={24} offset={6}>
              <h3> SITE DETAIL</h3>
            </Col>
          </Row>
          <Descriptions title="" bordered>
            <Descriptions.Item label="Contractor Name">
              {this.state.contractor.name}
            </Descriptions.Item>
            <Descriptions.Item label="Address">
              {this.state.contractor.address}
            </Descriptions.Item>
            <Descriptions.Item label="Phone Number">
              {this.state.contractor.phone_no}
            </Descriptions.Item>
          </Descriptions>
        </Modal>
      </div>
    );
  }
}
export default ContractorTable;
