import React, { Component } from 'react';
import { Form, Input } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { contractorActions } from '../../actions/ContractorActions';

class ContractorAdd extends Component {
  constructor() {
    super();
    this.state = { name: '', address: '', phone_no: '' };
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if(!err) {
        contractorActions.createContractor(values).then( response => {
          if(response.status === 201){
            success('Contractor was successfully Created!')
            this.props.history.push(`/contractors`);
          }else{
            error('Unable to create Contractor!')
          }
        }).catch(error => console.log('error', error));
      }
    })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCancel() {
    this.props.history.push("/contractors");
  }

  render() {
    const {form} = this.props
    return (
      <CustomLayout> 
        <Form onSubmit= { this.handleSubmit.bind(this) }>
          <Form.Item label='Contractor Name' className='short-text-field'>
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input contractor name!' }],
              })(
                <Input
                  placeholder="Contractor Name"
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Contractor Address' className='short-text-field'>
            {
              form.getFieldDecorator('address', {
                rules: [{ required: true, message: 'Please input contractor address!' }],
              })(
                <Input
                  placeholder="Contractor Address"
                  name='address'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Contractor Contact' className='short-text-field'>
            {
              form.getFieldDecorator('phone_no', {
                rules: [{ required: true, message: 'Please input valid contact number!' }],
              })(
                <Input
                  placeholder="Contractor Phone Number"
                  name='phone_no'
                />
              )
            }
          </Form.Item>
          <div className='btn-group'>
            <button type="submit" className="btn btn-dark">Create Contractor</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(ContractorAdd);
