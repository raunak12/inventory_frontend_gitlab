import React, { Component } from 'react';
import { Form, Input } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import {contractorActions} from '../../actions/ContractorActions'

class ContractorEdit extends Component {
  constructor() {
    super();
    this.state = { contractor: {} };

  }

  componentDidMount() {
    contractorActions.showContractor(this.props.match.params.id).then( response => {
      this.setState({contractor: response.data});
    }).catch(error => console.log('error', error));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
    if(!err) {
      contractorActions.updateContractor(this.state.contractor.id, values).then( response => {
        if(response.status === 204){
          this.props.history.push(`/contractors`);
          success('Contractor was successfully Updated!')
          return response.json()
        }else{
          error('Unable to Edit Contractor!')
        }
      }).catch(error => console.log('error', error));
      }
    })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCancel() {
    this.props.history.push(`/contractors`);
  }

  render() {
    const { form } =  this.props
    return (
      <CustomLayout>
        <h1>Edit Contractor</h1>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <Form.Item label= 'Contractor Name' >
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input contractor name!'}],
                initialValue: this.state.contractor.name
              })(
                <Input
                  placeholder='Contractor Name'
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Contractor Address'>
            {  
              form.getFieldDecorator('address', {
                rules: [{ required: true, message: 'Please input contractor address!'}],
                initialValue: this.state.contractor.address
              })(
                <Input
                  placeholder='Contractor Address'
                  name='address'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Contractor Phone Number'>
            {  
              form.getFieldDecorator('phone_no', {
                rules: [{ required: true, message: 'Please input valid phone number!'}],
                initialValue: this.state.contractor.phone_no
              })(
                <Input
                  placeholder='Phone Number'
                  name='phone_no'
                />
              )
            }
          </Form.Item>

          <div className="btn-group">
            <button type="submit" className="btn btn-dark">Update Contractor</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(ContractorEdit);
