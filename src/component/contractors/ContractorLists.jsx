import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CustomLayout from '../Layouts/CustomLayout'
import Table from './table'
import { contractorActions} from '../../actions/ContractorActions'
import {isAdmin} from '../../helpers/userPolicy'
import LoadingSpinner from '../../helpers/LoadingSpinner';

class ContractorLists extends Component {
  constructor() {
    super()
    this.state = { contractors: [], isLoading: true }
  }

  componentDidMount() {
    contractorActions.fetchContractors().then( response => {
      this.setState({ contractors: response.data, isLoading: false })
    }).catch(error => console.log('error', error))
  }

  showTable() {
    const { contractors } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
         <Table table_data={contractors} />
      )
    }
  }

  render() {
    const { contractors } = this.state
    if (contractors === null) return null
    return (
      <CustomLayout sidebarSelectedKey='contractors'>
        <h1>Contractors</h1>
        {
          isAdmin() ?  <Link to="/contractors/new" className="btn btn-outline-primary mar-pad float-right">
          Create Contractor
        </Link> : ''
        }
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default ContractorLists
