import React, { Component } from 'react';
import { Form, Input } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { vendorActions} from '../../actions/VendorActions';

class VendorEdit extends Component {
  constructor() {
    super();
    this.state = { vendor: {} };

  }

  componentDidMount() {
    vendorActions.showVendor(this.props.match.params.id).then( response => {
      this.setState({vendor: response.data});
    }).catch(error => console.log('error', error));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
    if(!err) {
      vendorActions.updateVendor(this.state.vendor.id, values).then( response => {
        if(response.status === 204){
          this.props.history.push(`/vendors`);
          success('Vendor was successfully Updated!')
          return response.json()
        }else{
          error('Unable to Edit Vendor!')
        }
      }).catch(error => console.log('error', error));
      }
    })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCancel() {
    this.props.history.push(`/vendors`);
  }

  render() {
    const { form } =  this.props
    return (
      <CustomLayout>
        <h1>Edit Vendor</h1>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <Form.Item label= 'Vendor Name' >
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input vendor name!'}],
                initialValue: this.state.vendor.name
              })(
                <Input
                  placeholder='Vendor Name'
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Vendor Address'>
            {  
              form.getFieldDecorator('address', {
                rules: [{ required: true, message: 'Please input vendor address!'}],
                initialValue: this.state.vendor.address
              })(
                <Input
                  placeholder='Vendor Address'
                  name='address'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Vendor Phone Number'>
            {  
              form.getFieldDecorator('phone_no', {
                rules: [{ required: true, message: 'Please input vendor phone number!'}],
                initialValue: this.state.vendor.phone_no
              })(
                <Input
                  placeholder='Vendor Phone Number'
                  name='phone_no'
                />
              )
            }
          </Form.Item>

          <div className="btn-group">
            <button type="submit" className="btn btn-dark">Update Vendor</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(VendorEdit);
