import React, { Component } from 'react';
import { Form, Input } from 'antd';
import CustomLayout from '../Layouts/CustomLayout'
import { vendorActions} from '../../actions/VendorActions';
import { success, error } from '../../helper/notifications';

class VendorAdd extends Component {
  constructor() {
    super();
    this.state = { name: '', address: '', phone_no: ''};
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if(!err) {
        vendorActions.createVendor(values).then( response => {
          if(response.status === 201){
            this.props.history.push(`/vendors`);
            success('Vendor was successfully Created!')
            return response.json()
          }else{
            error('Unable to create Vendor!')
          }
        }).catch(error => console.log('error', error));
        }
    })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCancel() {
    this.props.history.push("/vendors");
  }

  render() {
    const {form} = this.props
    return (
      <CustomLayout> 
        <Form onSubmit= { this.handleSubmit.bind(this) }>
          <Form.Item label='Vendor Name' className='short-text-field'>
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input Vendor name!' }],
              })(
                <Input
                  placeholder="Vendor Name"
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Vendor Address' className='short-text-field'>
            {
              form.getFieldDecorator('address', {
                rules: [{ required: true, message: 'Please input Vendor address!' }],
              })(
                <Input
                  placeholder="Vendor Address"
                  name='address'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Vendor Phone Number' className='short-text-field'>
            {
              form.getFieldDecorator('phone_no', {
                rules: [{ required: true, message: 'Please input Vendor phone number!' }],
              })(
                <Input
                  placeholder="Vendor Phone Number"
                  name='phone_no'
                />
              )
            }
          </Form.Item>
          <div className='btn-group'>
            <button type="submit" className="btn btn-dark">Create Vendor</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(VendorAdd);
