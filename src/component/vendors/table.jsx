import React, { Component } from "react";
import { Table, Icon, Divider, Modal, Button, Row, Col, Descriptions} from "antd";
import { Link } from "react-router-dom";
import { success, error } from "../../helper/notifications";
import { vendorActions } from "../../actions/VendorActions";
import { sortText } from "../../helpers/sort";
import { getColumnSearchProps } from "../../helpers/tableSearch";
import { isAdmin } from '../../helpers/userPolicy'

const { confirm } = Modal;

class VendorTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      vendors: props.table_data,
      vendor: {}
    };
  }

  handleDelete(vendorId) {
    vendorActions.deleteVendor(vendorId).then(response => {
      if (response.status === 204) {
        vendorActions
          .fetchVendors()
          .then(response => {
            this.setState({ vendors: response.data });
          })
          .catch(error => console.log("error", error));
        success("Vendor was successfully deleted!");
      } else {
        error("Unable to delete Vendor!");
      }
    });
  }

  showDeleteConfirm(vendorId) {
    confirm({
      className: "show-modal-delete",
      title: "Are you sure to delete this Vendor?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk: () => this.handleDelete(vendorId),
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }

  showModal = value => {
    vendorActions.showVendor(value).then(response => {
      this.setState({
        vendor: response.data,
        visible: true
      });
    });
   
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  render() {
    const columns = [
      {
        title: "Vendor Name",
        dataIndex: "name",
        key: "name",
        sorter: (a, b) => sortText(a.name, b.name),
        ...getColumnSearchProps("name", this)
      },
      {
        title: "Address",
        dataIndex: "address",
        key: "address",
        sorter: (a, b) => sortText(a.address, b.address),
        ...getColumnSearchProps("address", this)
      },
      {
        title: "",
        width: "8%",
        key: "action",
        render: (text, record) => {
          return (
            <span>
              {
                isAdmin() ?
                  <Link to={`vendors/${record.id}/edit`}>
                    <Icon title="Edit" type="edit" />
                  </Link> : ''
              }
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon title="View" onClick={() => this.showModal(`${record.id}`)} type="snippets" />
              </span>
              <Divider type="vertical" />
              {
                isAdmin() ? 
                  <span style={{ color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent' }}>
                    <Icon
                      type="delete"
                      title="Delete"
                      onClick={() => this.showDeleteConfirm(record.id)}
                    />
                  </span> : ''
              }
              
            </span>
          );
        }
      }
    ];
    const vendors = this.state.vendors;
    const view_table =
      vendors && vendors.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={vendors} size="middle" rowKey={column=> column.name}/>
        </div>
      ) : (
        "No Data Found"
      );
    return (
      <div>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>
          ]}
        >
          <Row>
            <Col span={24} offset={6}>
              <h3> VENDOR DETAIL</h3>
            </Col>
          </Row>
          <Descriptions title="" bordered>
            <Descriptions.Item label="Vendor Name">
              {this.state.vendor.name}
            </Descriptions.Item>
            <Descriptions.Item label="Address">
              {this.state.vendor.address}
            </Descriptions.Item>
          </Descriptions>
        </Modal>
        {view_table}
      </div>
    );
  }
}
export default VendorTable;
