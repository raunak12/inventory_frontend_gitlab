import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CustomLayout from '../Layouts/CustomLayout'
import Table from './table'
import {vendorActions} from '../../actions/VendorActions'
import { isAdmin } from '../../helpers/userPolicy'
import LoadingSpinner from '../../helpers/LoadingSpinner';

class VendorLists extends Component {
  constructor() {
    super()
    this.state = { vendors: [], isLoading: true }
  }

  componentDidMount() {
    vendorActions.fetchVendors().then(response => {
      this.setState({ vendors: response.data, isLoading: false })
    })
  }

  showTable() {
    const { vendors } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <Table table_data={vendors} />
      )
    }
  }

  render() {
    const { vendors } = this.state
    if (vendors === null) return null
    return (
      <CustomLayout sidebarSelectedKey='vendors'>
        <h1>Vendors</h1>
        {
          isAdmin() ?
        <Link to="/vendors/new" className="btn btn-outline-primary mar-pad float-right">
          Create Vendor
        </Link>
        : ''
        }
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default VendorLists
