import React, { Component } from 'react';
import CustomLayout from '../Layouts/CustomLayout';
import { Link } from 'react-router-dom';
import { success, error } from '../../helper/notifications';
import { Modal } from 'antd';
import { vendorActions } from '../../actions/VendorActions'
import { isAdmin } from '../../helpers/userPolicy';

const { confirm } = Modal;

class VendorShow extends Component {
  constructor() {
    super();
    this.state = { vendor: {} };
  }

  componentDidMount() {
    vendorActions.showVendor(this.props.match.params.id).then(response => {
      this.setState({ vendor: response.data });
    }).catch(error => console.log('error', error));
  }

  handleDelete() {
    vendorActions.deleteVendor(this.state.vendor.id).then(response => {
      if (response.status === 204) {
        success('Vendor was successfully deleted!')
        this.props.history.push("/vendors")
      } else {
        error('Unable to delete a vendor!')
      }
    }).catch(error => console.log('error', error));
  }

  showDeleteConfirm() {
    confirm({
      title: 'Are you sure to delete this Vendor?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleDelete.bind(this),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  render() {
    return (
      <CustomLayout>
        <table className="table table-sm view-table">
          <tr>
            <td><h5>Vendor Name:</h5> </td>
            <td><h5>{this.state.vendor.name}</h5></td>
            <td><h5>Vendor`s Address:</h5></td>
            <td><h5>{this.state.vendor.address}</h5></td>
          </tr>
          <tr>
            <td><h5>Vendor`s Phone Number:</h5></td>
            <td><h5> {this.state.vendor.phone_no}</h5></td>
          </tr>

          <tr>
            <td>
              {
                isAdmin() ?
                  <p>
                    <Link to={`/vendors/${this.state.vendor.id}/edit`} className="btn btn-primary">Edit</Link>
                    <button onClick={this.showDeleteConfirm.bind(this)} className="btn btn-danger">Delete</button>
                  </p>
                  : ''}
              <Link to="/vendors" className="btn btn-primary">Back</Link>
            </td>
          </tr> 
        </table>
      </CustomLayout>
    )
  }
}

export default VendorShow;
