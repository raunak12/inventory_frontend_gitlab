import React, { Component } from 'react'
import { DatePicker, Row, Col, Select, Button } from 'antd'
import ProductwiseReportTable from './ProductwiseReportTable'
import { productActions } from '../../actions/ProductActions';
import { reportActions } from '../../actions/ReportActions'

const { Option } = Select

class ReportsProduct extends Component {
  constructor(props) {
    super(props)
    this.state = {
      key: 'products',
      start_date: '',
      end_date: '',
      productwisereportdata: [],
      products: [],
      product_id: ''
    }
  }

  componentWillMount() {
    productActions.fetchproducts().then(response => {
      this.setState({ products: response.data})
    }).catch(error => console.log('error', error))
  }

  handleChangedate(date, dateString) {
    this.setState({
      end_date: dateString,
    })
  }

  handleChangeProduct = e => {
    this.setState({
      product_id: e.key === 'item_0' ? '' : e.key
    })
  }

  handleSubmit = e => {
    reportActions.productReport(this.state.start_date, this.state.end_date, this.state.product_id).then( response => {
      this.setState({ productwisereportdata: response.data})
    })
  }

  render() {    
    return (
     <div>
        <Row>
          <Col span={4}>
            <h6> Select Date:</h6>
            <DatePicker
              onChange={this.handleChangedate.bind(this)}
            />
          </Col>

          <Col span={4} offset={1}>
            <h6>Select Product</h6>
            <Select
              showSearch
              style={{ width: 250 }}
              placeholder="Select Product"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="" key="" onClick={this.handleChangeProduct.bind(this)}>
                ALL
              </Option>
              {
                this.state.products.map(product => {
                  return (
                    <Option value={product.id} key={product.id} onClick={this.handleChangeProduct.bind(this)}>
                      {product.name}
                    </Option>
                  )
                })
              }
            </Select>
          </Col>

          <Col span={4} offset={2}>
            <br />
            <Button type="primary" icon="search" onClick={this.handleSubmit.bind(this)}>
              Filter Results
            </Button>
          </Col>
        </Row>
        <div>
          <ProductwiseReportTable data_table={this.state.productwisereportdata} />
        </div>
      </div>
    )
  }
}

export default ReportsProduct
