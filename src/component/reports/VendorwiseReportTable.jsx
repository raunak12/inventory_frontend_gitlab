import React, { Component } from 'react'
import { Table } from 'antd'

class VendorwiseReportTable extends Component {
  render() {
    const columns = [
      {
        title: 'Vendor',
        dataIndex: 'vendor_name'
      },
      {
        title: 'Product',
        dataIndex: 'product_name'
      },
      {
        title: 'Quantity',
        dataIndex: 'quantity'
      },
      {
        title: 'Unit',
        dataIndex: 'unit'
      }
    ]
    const data = this.props.data_table
    const view_table =
      data && data.length > 0 ? (
        <div>
          <h4>Filtered Report</h4>
          <Table columns={columns} dataSource={data} size="middle" rowkey="uid"/>
        </div>
      ) : (
        'No Data Found'
      )
    return <div>{view_table}</div>
  }
}

export default VendorwiseReportTable