import React, { Component } from 'react'
import { Table } from 'antd'

class ProductwiseReportTable extends Component {
  render() {
    const columns = [
      {
        title: 'Site Name',
        dataIndex: 'site_name'
      },
      {
        title: 'Product Name',
        dataIndex: 'product_name'
      },
      {
        title: 'Quantity',
        dataIndex: 'closing'
      },
      {
        title: 'Unit',
        dataIndex: 'unit'
      },
    ]
    const data = this.props.data_table
    const view_table =
      data && data.length > 0 ? (
        <div>
          <h4>Filtered Report</h4>
          <Table columns={columns}
           dataSource={data} size="middle"/>
        </div>
      ) : (
        'No Data Found'
      )
    return <div>{view_table}</div>
  }
}

export default ProductwiseReportTable
