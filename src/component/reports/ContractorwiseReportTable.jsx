import React, { Component } from 'react'
import { Table } from 'antd'

class ContractorwiseReportTable extends Component {
  render() {
    const columns = [
      {
        title: 'Contractor',
        dataIndex: 'contractor_name'
      },
      {
        title: 'Product',
        dataIndex: 'product_name'
      },
      {
        title: 'Total Quantity Taken',
        dataIndex: 'consumption'
      },
      {
        title: 'Repatriated Quantity',
        dataIndex: 'repatriation'
      },
      {
        title: 'Consumed Item',
        dataIndex: 'quantity'
      },
      {
        title: 'Unit',
        dataIndex: 'unit'
      }
    ]
    const data = this.props.data_table
    const view_table =
      data && data.length > 0 ? (
        <div>
          <h4>Filtered Report</h4>
          <Table columns={columns} dataSource={data} size="middle" />
        </div>
      ) : (
        'No Data Found'
      )
    return <div>{view_table}</div>
  }
}

export default ContractorwiseReportTable