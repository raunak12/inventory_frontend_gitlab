import React, { Component } from 'react'
import { DatePicker, Row, Col, Select, Button } from 'antd'
import ContractorwiseReportTable from './ContractorwiseReportTable';
import { productActions } from '../../actions/ProductActions';
import { contractorActions } from '../../actions/ContractorActions';
import { reportActions } from '../../actions/ReportActions';

const { Option } = Select

class ReportsContractor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      key: 'contractor',
      start_date: '',
      end_date: '',
      contractors: [],
      contractor_id: '',
      products: [],
      product_id: '',
      contractorwisereportdata: []
    }
  }


  componentWillMount() {
    contractorActions.fetchContractors().then(response => {
      this.setState({ contractors: response.data })
    }).catch(error => console.log('error', error))

    productActions.fetchproducts().then(response => {
      this.setState({ products: response.data})
      }).catch(error => console.log('error', error))
  }

  handleChangedate(date, dateString) {
    this.setState({
      end_date: dateString
    })
  }

  handleChangeContractor = e => {
    this.setState({
      contractor_id: e.key === 'item_0' ? '' : e.key
    })
  }

  handleChangeProduct = e =>{
      this.setState({
          product_id: e.key === 'item_0' ? '' : e.key
      })
  }

  handleSubmit = e => {
    reportActions.contractorReport(this.state.start_date, this.state.end_date, this.state.product_id, this.state.contractor_id).then( response => {
      this.setState({ contractorwisereportdata: response.data})
    })
  }

  render() {    
    return (
     <div>
        <Row>
          <Col span={4}>
            <h6> Select Date:</h6>
            <DatePicker
              onChange={this.handleChangedate.bind(this)}
            />
          </Col>

          <Col span={6} offset = {1}>
            <h6>Select Contractor</h6>
            <Select
              showSearch
              style={{ width: 250 }}
              placeholder="Select Contractor"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="" key="" onClick={this.handleChangeContractor.bind(this)}>
                ALL
              </Option>
              {
                this.state.contractors.map(contractor => {
                  return (
                    <Option value={contractor.id} key={contractor.id} onClick={this.handleChangeContractor.bind(this)}>
                      {contractor.name}
                    </Option>
                  )
                })
              }
            </Select>
          </Col>

          <Col span={6}>
            <h6>Select Product</h6>
            <Select
              showSearch
              style={{ width: 250 }}
              placeholder="Select Product"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="" key="" onClick={this.handleChangeProduct.bind(this)}>
                ALL
              </Option>
              {
                this.state.products.map(product => {
                  return (
                    <Option value={product.id} key={product.id} onClick={this.handleChangeProduct.bind(this)}>
                      {product.name}
                    </Option>
                  )
                })
              }
            </Select>
          </Col>

          <Col span={4}>
            <br />
            <Button type="primary" icon="search" onClick={this.handleSubmit.bind(this)}>
              Filter Results
            </Button>
          </Col>
        </Row>
        <div>
          <ContractorwiseReportTable data_table={this.state.contractorwisereportdata} />
        </div>
      </div>
    )
  }
}

export default ReportsContractor
