import React, { Component } from 'react'
import { DatePicker, Row, Col, Select, Button } from 'antd'
import moment from 'moment'
import ReportsStockTable from './ReportsStockTable'
import { productActions } from '../../actions/ProductActions'
import { siteActions } from '../../actions/SiteActions'
import { reportActions } from '../../actions/ReportActions'
const { Option } = Select


class ReportsStock extends Component {
  constructor(props) {
    super(props)
    this.state = {
      start_date: '',
      end_date: '',
      sitewisereport: [],
      products: [],
      sites: [],
      site_id: '',
      product_id: ''
    }
  }

  componentWillMount() {
    productActions.fetchproducts().then(response => {
      this.setState({ products: response.data})
    }).catch(error => console.log('error', error))

    siteActions.fetchSites().then( response => {
      this.setState( { sites: response.data})
    }).catch(error => console.log('error', error))
  }

  handleChangedate (date, dateString) {
    this.setState({
      start_date: dateString[0],
      end_date: dateString[1]
    })
  }

  handleChangeProduct = e => {
    this.setState({
      product_id: e.key === 'item_0' ? '' : e.key
    })
  }

  handleChangeSite = e => {
    this.setState({
      site_id: e.key === 'item_0' ? '' : e.key
    })
  }

  handleSubmit = e => {
     reportActions.stockReport(this.state.start_date, this.state.end_date, this.state.product_id, this.state.site_id).then( response => {
      this.setState({ sitewisereportdata: response.data})
    })
  }

  render() {
    const RangePicker = DatePicker.RangePicker

    return (
      <div>
        <Row>
          <Col span={6}>
            <h6> Start and End Date:</h6>
            <RangePicker
              defaultValue={[moment().subtract(1, 'month'), moment()]}
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')]
              }}
              onChange={this.handleChangedate.bind(this)}
            />
          </Col>

          <Col span={6} offset = {1}>
            <h6>Select Product</h6>
            <Select
              showSearch
              style={{ width: 250 }}
              placeholder="Select a product"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="" key="" onClick={this.handleChangeProduct.bind(this)}>
                ALL
              </Option>
              {this.state.products.map(product => {
                return (
                  <Option value={product.id} key={product.id} onClick={this.handleChangeProduct.bind(this)}>
                    {product.name}
                  </Option>
                )
              })}
            </Select>
          </Col>
          
          <Col span={6}>
            <h6>Select site</h6>
            <Select
              showSearch
              style={{ width: 250 }}
              placeholder="Select a Site"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="" key="" onClick={this.handleChangeSite.bind(this)}>
                ALL
              </Option>
              {this.state.sites.map(site => {
                return (
                  <Option value={site.id} key={site.id} onClick={this.handleChangeSite.bind(this)}>
                    {site.name}
                  </Option>
                )
              })}
            </Select>
          </Col>

          <Col span={4}>
            <br />
            <Button type="primary" icon="search" onClick={this.handleSubmit.bind(this)}>
              Filter Results
            </Button>
          </Col>
        </Row>
        <div>
          <ReportsStockTable data_table={this.state.sitewisereportdata} />
        </div>
      </div>
    )
  }
}

export default ReportsStock
