import React, { Component } from 'react'
import CustomLayout from '../Layouts/CustomLayout'
import { Tabs } from 'antd'
import ReportsStock from './ReportsStock'
import ReportsProduct from './ReportsProduct'
import ReportsVendor from './ReportsVendor';
import ReportsContractor from './ReportsContractor';
import ReportsStockEndDate from './ReportsStockEndDate'

const { TabPane } = Tabs

const hstyle = {
  padding: '1%'
}

class Reportlists extends Component {
  render() {
    return (
      <CustomLayout sidebarSelectedKey="reports">
        <div className="container">
          <h4 style={hstyle}> Filter Reports</h4>
          <Tabs defaultActiveKey= 'products'>
            <TabPane tab="Product" key="products">
              <ReportsProduct />
            </TabPane>
            <TabPane tab="Stock" key="endstocks">
              <ReportsStockEndDate />
            </TabPane>
            <TabPane tab="Vendor" key="vendors">
              <ReportsVendor />
            </TabPane>
            <TabPane tab="Contractor" key="contractors">
               <ReportsContractor />
            </TabPane>
            <TabPane tab="Monthly Report" key="stocks">
              <ReportsStock />
            </TabPane>
          </Tabs>
        </div>
      </CustomLayout>
    )
  }
}

export default Reportlists
