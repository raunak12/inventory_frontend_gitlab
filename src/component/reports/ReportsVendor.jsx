import React, { Component } from 'react'
import { DatePicker, Row, Col, Select, Button } from 'antd'
import VendorwiseReportTable from './VendorwiseReportTable';
import { productActions } from '../../actions/ProductActions';
import { vendorActions } from '../../actions/VendorActions';
import { reportActions } from '../../actions/ReportActions';

const { Option } = Select

class ReportsVendor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      key: 'vendors',
      start_date: '',
      end_date: '',
      vendors: [],
      vendor_id: '',
      vendorwisereportdata: [],
      products: [],
      product_id: ''
    }
  }


  componentWillMount() {
    vendorActions.fetchVendors().then(response => {
      this.setState({ vendors: response.data})
    }).catch(error => console.log('error', error))

    productActions.fetchproducts().then(response => {
      this.setState({ products: response.data})
    }).catch(error => console.log('error', error))
  }

  handleChangedate(date, dateString) {
    this.setState({
      end_date: dateString
    })
  }

  handleChangeVendor = e => {
    this.setState({
      vendor_id: e.key === 'item_0' ? '' : e.key
    })
  }

  handleChangeProduct = e =>{
      this.setState({
          product_id: e.key === 'item_0' ? '' : e.key
      })
  }

  handleSubmit = e => {
    reportActions.vendorReport(this.state.start_date, this.state.end_date, this.state.product_id, this.state.vendor_id).then( response => {
      this.setState({ vendorwisereportdata: response.data})
    })
  }

  render() {    
    return (
     <div>
        <Row>
          <Col span={4}>
            <h6> Select Date:</h6>
            <DatePicker
              onChange={this.handleChangedate.bind(this)}
            />
          </Col>

          <Col span={6} offset = {1}>
            <h6>Select Vendor</h6>
            <Select
              showSearch
              style={{ width: 250 }}
              placeholder="Select Vendor"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="" key="" onClick={this.handleChangeVendor.bind(this)}>
                ALL
              </Option>
              {
                this.state.vendors.map(vendor => {
                  return (
                    <Option value={vendor.id} key={vendor.id} onClick={this.handleChangeVendor.bind(this)}>
                      {vendor.name}
                    </Option>
                  )
                })
              }
            </Select>
          </Col>

          <Col span={6}>
            <h6>Select Product</h6>
            <Select
              showSearch
              style={{ width: 250 }}
              placeholder="Select Product"
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="" key="" onClick={this.handleChangeProduct.bind(this)}>
                ALL
              </Option>
              {
                this.state.products.map(product => {
                  return (
                    <Option value={product.id} key={product.id} onClick={this.handleChangeProduct.bind(this)}>
                      {product.name}
                    </Option>
                  )
                })
              }
            </Select>
          </Col>

          <Col span={4}>
            <br />
            <Button type="primary" icon="search" onClick={this.handleSubmit.bind(this)}>
              Filter Results
            </Button>
          </Col>
        </Row>
        <div>
          <VendorwiseReportTable data_table={this.state.vendorwisereportdata} />
        </div>
      </div>
    )
  }
}

export default ReportsVendor
