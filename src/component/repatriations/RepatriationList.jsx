import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CustomLayout from '../Layouts/CustomLayout'
import Table from './table'
import {repatriationActions} from '../../actions/RepatriationActions'
import LoadingSpinner from '../../helpers/LoadingSpinner';

class RepatriationList extends Component {
  constructor() {
    super()
    this.state = { repatriations: [], isLoading: true }
  }

  componentDidMount() {
    repatriationActions.fetchRepatriatedItems().then(response => {
      this.setState({ repatriations: response.data, isLoading: false})
    }).catch(error => console.log('error', error))
  }

  showTable() {
    const { repatriations } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <Table table_data={repatriations} />
      )
    }
  }

  render() {
    const { repatriations } = this.state
    if (repatriations === null) return null
    return (
      <CustomLayout sidebarSelectedKey='repatriations'>
        <h1>Repatriations</h1>
        <Link to="/repatriations/new" className="btn btn-outline-primary mar-pad float-right">
          Create Repatriation
        </Link>
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default RepatriationList
