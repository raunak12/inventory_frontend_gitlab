import React, { Component } from "react";
import { Form, Input, Select, Button, DatePicker, Icon, Row, Col } from "antd";
import CustomLayout from "../Layouts/CustomLayout";
import moment from "moment";
import { success, error } from "../../helper/notifications";
import { repatriationActions } from "../../actions/RepatriationActions";
import { categoryActions } from "../../actions/CategoryActions";
import { siteActions } from "../../actions/SiteActions";
import { vendorActions } from "../../actions/VendorActions";
import { contractorActions } from "../../actions/ContractorActions";
import { isAdmin } from "../../helpers/userPolicy";
import Cookie from "js.cookie";

const { Option } = Select;
let id = 0;

class AddRepatriation extends Component {
  constructor() {
    super();
    this.state = {
      repatriationForms: [0],
      categories: [],
      products: [],
      category: {},
      source_names: [],
      destination_names: [],
      contractors: [],
      sites: []
    };
  }

  componentDidMount() {
    categoryActions
      .fetchCategories()
      .then(response => {
        this.setState({ categories: response.data });
      })
      .catch(error => console.log("error", error));

    siteActions
      .fetchSites()
      .then(response => {
        this.setState({
          sites: response.data
        });
      })
      .catch(error => console.log("error", error));

    contractorActions
      .fetchContractors()
      .then(response => {
        this.setState({
          contractors: response.data
        });
      })
      .catch(error => console.log("error", error));
  }

  handleChange(value, k) {
    this.props.form.resetFields(
      `repatriated_items_attributes[${k}][shipment_attributes][product_id]`
    );
    categoryActions
      .fetchProductsCategory(value)
      .then(response => {
        this.setState({ products: response.data });
      })
      .catch(error => console.log("error", error));

    categoryActions
      .showCategory(value)
      .then(response => {
        this.setState({ category: response.data });
        this.props.form.setFieldsValue({
          [`repatriated_items_attributes[${k}][shipment_attributes][unit]`]: response
            .data.unit
        });
      })
      .catch(error => console.log("error", error));
  }

  handleChangeSourceType(value) {
    if (value === "Contractor") {
      contractorActions
        .fetchContractors()
        .then(response => {
          this.setState({ source_names: response.data });
        })
        .catch(error => console.log("error", error));
    }

    if (value === "Site") {
      siteActions
        .fetchSites()
        .then(response => {
          this.setState({ source_names: response.data });
        })
        .catch(error => console.log("error", error));
    }

    if (value === "Vendor") {
      vendorActions
        .fetchVendors()
        .then(response => {
          this.setState({ source_names: response.data });
        })
        .catch(error => console.log("error", error));
    }
  }

  handleChangeDestinationType(value) {
    if (value === "Contractor") {
      contractorActions
        .fetchContractors()
        .then(response => {
          this.setState({ destination_names: response.data });
        })
        .catch(error => console.log("error", error));
    }

    if (value === "Site") {
      siteActions
        .fetchSites()
        .then(response => {
          this.setState({ destination_names: response.data });
        })
        .catch(error => console.log("error", error));
    }
  }

  handelSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        repatriationActions
          .createRepatriation(values)
          .then(response => {
            if (response.status === 201) {
              this.props.history.push(`/repatriations`);
              success("Repatriation is sucessfully created.");
            } else {
              error("Unable to create Repatriation!!!");
            }
          })
          .catch(error => console.log("error", error));
      }
    });
  }

  handleCancel() {
    this.props.history.push("/repatriations");
  }

  addForm() {
    const { repatriationForms } = this.state;
    const newKey =
      repatriationForms.length > 0
        ? repatriationForms[repatriationForms.length - 1] + 1
        : 0;
    this.setState({ repatriationForms: [...repatriationForms, newKey] });
  }

  removeForm(key) {
    const { repatriationForms } = this.state;
    this.setState({
      repatriationForms: repatriationForms.filter(k => k !== key)
    });
  }

  remove(k) {
    const { form } = this.props;
    const keys = form.getFieldValue("keys");
    if (keys.length === 1) {
      return;
    }
    form.setFieldsValue({
      keys: keys.filter(key => key !== k)
    });
  }

  add() {
    const { form } = this.props;
    const keys = form.getFieldValue("keys");
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
      keys: nextKeys
    });
  }

  render() {
    const { form } = this.props;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };

    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 }
      }
    };

    getFieldDecorator("keys", { initialValue: [] });
    const keys = getFieldValue("keys");
    const formItems = keys.map((k, index) => (
      <Form.Item
        {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        label={index === 0 ? "Chalan" : ""}
        required={false}
        key={k}
      >
        {getFieldDecorator(`chalan_no[${k}]`, {
          validateTrigger: ["onChange", "onBlur"],
          rules: [
            {
              whitespace: true
            }
          ]
        })(
          <Input
            placeholder="Add Chalan Number"
            style={{ width: "90%", marginRight: 8 }}
          />
        )}
        {keys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
          />
        ) : null}
      </Form.Item>
    ));

    return (
      <CustomLayout>
        <br />
        <Col>
          <h3>ADD NEW REPATRIATION</h3>
        </Col>
        <div>
          <Form onSubmit={this.handelSubmit.bind(this)}>
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label="Repatriation Date">
                    {form.getFieldDecorator("date", {
                      rules: [{ required: true, message: "Please Enter Date" }],
                      initialValue: moment()
                    })(<DatePicker />)}
                  </Form.Item>
                </div>
              </Col>
						</Row>

            {this.state.repatriationForms.map(k => {
              return (
                <div key={k}>
                  <fieldset key={k}>
                    <Row>
                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label="Category:">
                            {form.getFieldDecorator(
                              `repatriated_items_attributes[${k}][shipment_attributes][category_id]`,
                              {
                                rules: [
                                  {
                                    required: true,
                                    message: "Please Select the category"
                                  }
                                ],
                                onChange: value => this.handleChange(value, k)
                              }
                            )(
                              <Select
                                showSearch
                                style={{ width: 200 }}
                                placeholder="Select a Category"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                  option.props.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                                }
                              >
                                {this.state.categories.map(category => {
                                  return (
                                    <Option
                                      value={category.id}
                                      key={category.id}
                                    >
                                      {category.name}
                                    </Option>
                                  );
                                })}
                              </Select>
                            )}
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label="Product">
                            {form.getFieldDecorator(
                              `repatriated_items_attributes[${k}][shipment_attributes][product_id]`,
                              {
                                rules: [
                                  {
                                    required: true,
                                    message: "Please Enter Product Name"
                                  }
                                ]
                              }
                            )(
                              <Select
                                showSearch
                                style={{ width: 200 }}
                                placeholder="Select Product"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                  option.props.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                                }
                              >
                                {this.state.products.map(product => {
                                  return (
                                    <Option value={product.id} key={product.id}>
                                      {product.name}
                                    </Option>
                                  );
                                })}
                              </Select>
                            )}
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label="Remarks">
                          {form.getFieldDecorator(
                            `repatriated_items_attributes[${k}][shipment_attributes][remarks]`,
                            {}
                          )(<Input placeholder="Remarks" name="remarks" />)}
                        </Form.Item>
                      </Col>
                    
                      <Col span={4} offset={1}>
                        <Form.Item label="Quantity">
                          {form.getFieldDecorator(
                            `repatriated_items_attributes[${k}][shipment_attributes][quantity]`,
                            {
                              rules: [
                                {
                                  required: true,
                                  message: "Please Enter Quantity"
                                }
                              ]
                            }
                          )(<Input placeholder="Quantity" name="quantity" />)}
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label="Unit">
                          {form.getFieldDecorator(
                            `repatriated_items_attributes[${k}][shipment_attributes][unit]`,
                            {}
                          )(
                            <Input
                              placeholder="Unit"
                              name="unit"
                              readOnly="readOnly"
                            />
                          )}
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row>
                      <Col span={4} offset={1}>
                        <Button onClick={e => this.removeForm(k)} type="danger">
                          Remove Form
                        </Button>
                      </Col>
                    </Row>
                  </fieldset>
                  <br />
                </div>
              );
            })}
            <br />
            <Row>
              <Col span={23}>
                <Button
                  onClick={e => this.addForm(e)}
                  type="primary"
                  style={{ float: "right" }}
                >
                  Add New Product
                </Button>
              </Col>
            </Row>

            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label="Source Name">
                    {form.getFieldDecorator("source_id", {
                      rules: [
                        { required: true, message: "Please Enter Source Name" }
                      ]
                    })(
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select a Source Name"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.state.contractors.map(contractor => {
                          return (
                            <Option value={contractor.id} key={contractor.id}>
                              {contractor.name}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </Col>

              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="Destination Name">
                    {form.getFieldDecorator("destination_id", {
                      rules: [
                        {
                          required: true,
                          message: "Please Enter Destination Name"
                        }
                      ]
                    })(
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select a Destination Name"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {isAdmin()?
                            this.state.sites.map(destination => {
                              return (
                                <Option
                                  value={destination.id}
                                  key={destination.id}
                                >
                                  {destination.name}
                                </Option>
                              );
                            })
                          :
                          this.state.sites.map(destination => {
                              if (destination.id === Cookie.get("site_id")) {
                                return (
                                  <Option
                                    value={destination.id}
                                    key={destination.id}
                                  >
                                    {destination.name}
                                  </Option>
                                );
                              }else {
                                return null;
                              }
                            })}
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </Col>

              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="Reason For Repatriation:">
                    {form.getFieldDecorator("reason", {
                      rules: [
                        {
                          required: true,
                          message: "Please cause for Repatriation"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Cause for Repatriation"
                        name="reason"
                      />
                    )}
                  </Form.Item>
                </div>
              </Col>

              <Col span={6} offset={1}>
                <Form.Item {...formItemLayoutWithOutLabel}>
                  <Button
                    type="dashed"
                    onClick={this.add.bind(this)}
                    style={{ width: "90%" }}
                  >
                    <Icon type="plus" />
                    Add Chalan Number
                  </Button>
                </Form.Item>
                {formItems}
              </Col>
            </Row>

            <Row>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="">
                    {form.getFieldDecorator("source_type", {
                      initialValue: "Contractor"
                    })(
                      <Input
                        type="hidden"
                        style={{ width: 200 }}
                      />
                    )}
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="">
                    {form.getFieldDecorator("destination_type", {
                      initialValue: "Site"
                    })(
                      <Input
                        placeholder="Site"
                        type="hidden"
                        style={{ width: 200 }}
                      />
                    )}
                  </Form.Item>
                </div>
              </Col>
            </Row>

            <Row>
              <Col>
                <div className="btn-group">
                  <Button htmlType="submit" className="btn btn-dark">
                    Create Repatriation
                  </Button>
                  &nbsp;
                  <Button
                    type="button"
                    onClick={this.handleCancel.bind(this)}
                    className="btn btn-secondary"
                  >
                    Cancel
                  </Button>
                </div>
              </Col>
            </Row>
            <br />
          </Form>
        </div>
      </CustomLayout>
    );
  }
}

export default Form.create({})(AddRepatriation);
