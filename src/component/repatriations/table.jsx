import React, { Component } from "react";
import { Table, Icon, Divider, Modal, Button, Row, Col, Descriptions } from "antd";
import { Link } from "react-router-dom";
import { success, error } from "../../helper/notifications";
import { repatriationActions } from "../../actions/RepatriationActions";
import { shipmentActions } from "../../actions/ShipmentActions";
import { sortText, sortDate } from "../../helpers/sort";
import { getColumnSearchProps } from "../../helpers/tableSearch";

const { confirm } = Modal;

class RepatriationTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      repatriations: props.table_data,
      visible: false,
      repatriated: {}
    };
  }

  handleDelete(shipment_id) {
    shipmentActions.deleteShipment(shipment_id).then(response => {
      if (response.status === 204) {
        repatriationActions
          .fetchRepatriatedItems()
          .then(response => {
            this.setState({ repatriations: response.data });
          })
          .catch(error => console.log("error", error));
        success("Repatriated product was successfully deleted!");
      } else {
        error("Unable to delete a repatriation!");
      }
    });
  }

  showDeleteConfirm(shipment_id) {
    confirm({
      className: "show-modal-delete",
      title: "Are you sure to delete this Repatriation?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk: () => this.handleDelete(shipment_id),
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }

  setRepatriation(response) {
    this.setState({ repatriated: response.data });
  }

  showModal = value => {
    repatriationActions.showRepatriatedItem(
      value,
      this.setRepatriation.bind(this)
    );
    this.setState({
      visible: true
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };
  render() {
    const columns = [
      {
        title: "Date",
        dataIndex: "shipment_date",
        key: "shipment_date",
        sorter: (a, b) => sortDate(a.shipment_date, b.shipment_date),
        ...getColumnSearchProps("shipment_date", this)
      },
      {
        title: "Product Name",
        dataIndex: "product_name",
        key: "product_name",
        sorter: (a, b) => sortText(a.product_name, b.product_name),
        ...getColumnSearchProps("product_name", this)
      },
      {
        title: "Category",
        dataIndex: "category_name",
        key: "category_name"
      },
      {
        title: "Quantity",
        dataIndex: "quantity",
        key: "quantity"
      },
      {
        title: "Unit",
        dataIndex: "unit",
        key: "unit"
      },
      {
        title: "Source",
        dataIndex: "source_name",
        key: "source_name",
        sorter: (a, b) => sortText(a.source_name, b.source_name),
        ...getColumnSearchProps("source_name", this)
      },
      {
        title: "Destination",
        dataIndex: "destination_name",
        key: "destination_name",
        sorter: (a, b) => sortText(a.destination_name, b.destination_name),
        ...getColumnSearchProps("destination_name", this)
      },
      {
        title: "Reason",
        dataIndex: "reason",
        key: "reasom"
      },
      {
        title: "",
        width: "8%",
        key: "action",
        render: (text, record) => {
          return (
            <span>
              <Link to={`repatriations/${record.repatriation_id}/edit`}>
                <Icon title="Edit" type="edit" />
              </Link>
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon title="View" onClick={() => this.showModal(`${record.id}`)} type="snippets" />
              </span>
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon
                  type="delete"
                  title="Delete"
                  onClick={() => this.showDeleteConfirm(record.shipment_id)}
                />
              </span>
            </span>
          );
        }
      }
    ];
    const repatriations = this.state.repatriations;
    const view_table =
      repatriations && repatriations.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={repatriations} size="middle" rowKey={column=> column.id}/>
        </div>
      ) : (
        "No Data Found"
      );
    return (
      <div>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>
          ]}
        >
          <Row>
            <Col span={24} offset={6}>
              <h3>REPATRIATED PRODUCT DETAIL</h3>
            </Col>
          </Row>

          <Row>
            <Col span={10}>
              <h4>Date: {this.state.repatriated.date}</h4>
            </Col>
            <Col span={6} />
            <Col span={8} />
          </Row>
          <Descriptions title="" bordered>
            <Descriptions.Item label="Product Name">
              {this.state.repatriated.product_name}
            </Descriptions.Item>
            <Descriptions.Item label="Quantity">
              {this.state.repatriated.quantity}
            </Descriptions.Item>
            <Descriptions.Item label="Unit">
              {this.state.repatriated.unit}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Category">
              {this.state.repatriated.category_name}
            </Descriptions.Item>
            <Descriptions.Item label="Source Name">
              {this.state.repatriated.source_name}
            </Descriptions.Item>
            <Descriptions.Item label="Destination Name">
              {this.state.repatriated.destination_name}
            </Descriptions.Item>
            <Descriptions.Item label="Reason for Repatriation">
              {this.state.repatriated.reason}
            </Descriptions.Item>
            <Descriptions.Item label="Remarks">
              {this.state.repatriated.remarks}
            </Descriptions.Item>
          </Descriptions>
        </Modal>
        {view_table}
      </div>
    );
  }
}

export default RepatriationTable;
