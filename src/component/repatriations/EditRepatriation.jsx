import React, { Component } from "react";
import {
  Form,
  Input,
  Select,
  Button,
  DatePicker,
  Icon,
  Row,
  Col,
  Modal
} from "antd";
import CustomLayout from "../Layouts/CustomLayout";
import moment from "moment";
import { success, error } from "../../helper/notifications";
import { repatriationActions } from "../../actions/RepatriationActions";
import { categoryActions } from "../../actions/CategoryActions";
import { siteActions } from "../../actions/SiteActions";
import { vendorActions } from "../../actions/VendorActions";
import { contractorActions } from "../../actions/ContractorActions";
import { productActions } from "../../actions/ProductActions";
import { shipmentActions } from "../../actions/ShipmentActions";
import { isAdmin } from "../../helpers/userPolicy";
import Cookie from "js.cookie";

const { confirm } = Modal;
const { Option } = Select;
let id = 0;

class EditRepatriation extends Component {
  constructor() {
    super();
    this.state = {
      repatriationForms: [],
      repatriation: {},
      categories: [],
      products: [],
      category: {},
      source_names: [],
      destination_names: []
    };
  }

  componentDidMount() {
    repatriationActions
      .showRepatriation(this.props.match.params.id)
      .then(response => {
        this.setState({
          repatriation: response.data,
          repatriationForms: response.data.repatriated_items.map(
            (_d, index) => index
          )
        });
        id = response.data.chalan_no.length;
        contractorActions
          .fetchContractors()
          .then(response => {
            this.setState({
              source_names: response.data
            });
          })
          .catch(error => console.log("error", error));

        siteActions
          .fetchSites()
          .then(response => {
            this.setState({
              destination_names: response.data
            });
          })
          .catch(error => console.log("error", error));
      })
      .catch(error => console.log("error", error));

    categoryActions.fetchCategories().then(response => {
      this.setState({ categories: response.data });
    });

    productActions.fetchproducts().then(response => {
      this.setState({ products: response.data });
    });
  }

  handleChange(value, k) {
    this.props.form.setFieldsValue({
      [`repatriated_items_attributes[${k}][shipment_attributes][product_id]`]: ""
    });
    categoryActions
      .fetchProductsCategory(value)
      .then(response => {
        this.setState({ products: response.data });
      })
      .catch(error => console.log("error", error));

    categoryActions
      .showCategory(value)
      .then(response => {
        this.setState({ category: response.data });
        this.props.form.setFieldsValue({
          [`repatriated_items_attributes[${k}][shipment_attributes][unit]`]: response
            .data.unit
        });
      })
      .catch(error => console.log("error", error));
  }

  handleChangeSourceType(value) {
    if (value === "Contractor") {
      contractorActions
        .fetchContractors()
        .then(response => {
          this.setState({ source_names: response.data });
        })
        .catch(error => console.log("error", error));
    }

    if (value === "Site") {
      siteActions
        .fetchSites()
        .then(response => {
          this.setState({ source_names: response.data });
        })
        .catch(error => console.log("error", error));
    }

    if (value === "Vendor") {
      vendorActions
        .fetchVendors()
        .then(response => {
          this.setState({ source_names: response.data });
        })
        .catch(error => console.log("error", error));
    }
  }

  handleChangeDestinationType(value) {
    if (value === "Contractor") {
      contractorActions
        .fetchContractors()
        .then(response => {
          this.setState({ destination_names: response.data });
        })
        .catch(error => console.log("error", error));
    }

    if (value === "Site") {
      siteActions
        .fetchSites()
        .then(response => {
          this.setState({ destination_names: response.data });
        })
        .catch(error => console.log("error", error));
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        repatriationActions
          .updateRepatriation(this.state.repatriation.id, values)
          .then(response => {
            if (response.status === 200) {
              success("Repatriation is successfully Updated!!");
              this.props.history.push(`/repatriations`);
            } else {
              error("Unable to Edit Repatriation!!");
            }
          })
          .catch(error => console.log("error", error));
      }
    });
  }

  handleCancel() {
    this.props.history.push("/repatriations");
  }

  addForm() {
    const { repatriationForms } = this.state;
    const newKey =
      repatriationForms.length > 0
        ? repatriationForms[repatriationForms.length - 1] + 1
        : 0;
    this.setState({ repatriationForms: [...repatriationForms, newKey] });
  }

  removeForm(k) {
    const { repatriationForms } = this.state;
    const repatriatedItem = this.state.repatriation.repatriated_items[k];
    if (repatriatedItem && repatriatedItem.shipment_id) {
      shipmentActions
        .deleteShipment(
          this.state.repatriation.repatriated_items[k].shipment_id
        )
        .then(response => {
          if (response.status === 204) {
            success("Repatrated product was successfully deleted!");
          } else {
            error("Unable to delete a Repatrated Product!");
          }
        })
        .catch(error => console.log("error", error));
    }
    this.setState({
      repatriationForms: repatriationForms.filter(key => k !== key)
    });
  }

  showDeleteConfirm(k) {
    confirm({
      title: "Are you sure to remove this Shipment?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk: () => {
        this.removeForm(k);
      },
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }

  remove(k, key) {
    const { form } = this.props;
    const keys = form.getFieldValue("keys");
    if (keys.length === 1) {
      return;
    }
    form.setFieldsValue({
      keys: keys.filter(key => key !== k)
    });
  }

  add() {
    const { form } = this.props;
    const keys = form.getFieldValue("keys");
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
      keys: nextKeys
    });
  }

  render() {
    const { form } = this.props;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };

    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 }
      }
    };

    const chalanNumbers = this.state.repatriation.chalan_no
      ? this.state.repatriation.chalan_no
      : [];
    const keysInitialValues = chalanNumbers.map((_chalan, index) => index);

    getFieldDecorator("keys", { initialValue: keysInitialValues });
    const keys = getFieldValue("keys");
    const formItems = keys.map((k, index) => (
      <Form.Item
        {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        label={index === 0 ? "Chalan" : ""}
        required={false}
        key={k}
      >
        {getFieldDecorator(`chalan_no[${k}]`, {
          validateTrigger: ["onChange", "onBlur"],
          rules: [
            {
              whitespace: true
            }
          ],
          initialValue: chalanNumbers[k] && String(chalanNumbers[k])
        })(
          <Input
            placeholder="Add Chalan Number"
            style={{ width: "90%", marginRight: 8 }}
          />
        )}
        {keys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
          />
        ) : null}
      </Form.Item>
    ));

    return (
      <CustomLayout>
        <div>
          <Col>
            <h3>EDIT REPATRIATION</h3>
          </Col>
          <Form onSubmit={this.handleSubmit.bind(this)}>
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label="Repatriation Date">
                    {form.getFieldDecorator("date", {
                      rules: [{ required: true, message: "Please Enter Date" }],
                      initialValue: moment.utc(this.state.repatriation.date)
                    })(<DatePicker />)}
                  </Form.Item>
                </div>
              </Col>
            </Row>

            {this.state.repatriationForms.map(k => {
              const repatriationItem = this.state.repatriation
                .repatriated_items[k];
              return (
                <div key={k}>
                  <fieldset key={k}>
                    <Row>
                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label="Category:">
                            {form.getFieldDecorator(
                              `repatriated_items_attributes[${k}][shipment_attributes][category_id]`,
                              {
                                rules: [
                                  {
                                    required: true,
                                    message: "Please Select the category"
                                  }
                                ],
                                initialValue:
                                  repatriationItem &&
                                  repatriationItem["category_id"],
                                onChange: value => this.handleChange(value, k)
                              }
                            )(
                              <Select
                                showSearch
                                style={{ width: 200 }}
                                placeholder="Select a Category"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                  option.props.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                                }
                              >
                                {this.state.categories.map(category => {
                                  return (
                                    <Option
                                      value={category.id}
                                      key={category.id}
                                    >
                                      {category.name}
                                    </Option>
                                  );
                                })}
                              </Select>
                            )}
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label="Product">
                            {form.getFieldDecorator(
                              `repatriated_items_attributes[${k}][shipment_attributes][product_id]`,
                              {
                                rules: [
                                  {
                                    required: true,
                                    message: "Please Enter Product Name"
                                  }
                                ],
                                initialValue:
                                  repatriationItem &&
                                  repatriationItem["product_id"]
                              }
                            )(
                              <Select
                                showSearch
                                style={{ width: 200 }}
                                placeholder="Select Product"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                  option.props.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                                }
                              >
                                {this.state.products.map(product => {
                                  return (
                                    <Option value={product.id} key={product.id}>
                                      {product.name}
                                    </Option>
                                  );
                                })}
                              </Select>
                            )}
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label="Remarks">
                          {form.getFieldDecorator(
                            `repatriated_items_attributes[${k}][shipment_attributes][remarks]`,
                            {
                              initialValue:
                                repatriationItem && repatriationItem["remarks"]
                            }
                          )(<Input placeholder="Remarks" name="remarks" />)}
                        </Form.Item>
                      </Col>
                   
                      <Col span={4} offset={1}>
                        <Form.Item label="Quantity">
                          {form.getFieldDecorator(
                            `repatriated_items_attributes[${k}][shipment_attributes][quantity]`,
                            {
                              initialValue:
                                repatriationItem && repatriationItem["quantity"]
                            }
                          )(<Input placeholder="Quantity" name="quantity" />)}
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label="Unit">
                          {form.getFieldDecorator(
                            `repatriated_items_attributes[${k}][shipment_attributes][unit]`,
                            {
                              initialValue:
                                repatriationItem && repatriationItem["unit"]
                            }
                          )(
                            <Input
                              placeholder="Unit"
                              name="unit"
                              readOnly="readOnly"
                            />
                          )}
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item>
                            {form.getFieldDecorator(
                              `repatriated_items_attributes[${k}][shipment_attributes][id]`,
                              {
                                initialValue:
                                  repatriationItem &&
                                  repatriationItem["shipment_id"]
                              }
                            )(
                              <Input
                                type="hidden"
                                placeholder="Unit"
                                name="unit"
                                readOnly="readOnly"
                              />
                            )}
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item>
                            {form.getFieldDecorator(
                              `repatriated_items_attributes[${k}][id]`,
                              {
                                initialValue:
                                  repatriationItem &&
                                  repatriationItem["repatriation_item_id"]
                              }
                            )(
                              <Input
                                type="hidden"
                                placeholder="Unit"
                                name="unit"
                                readOnly="readOnly"
                              />
                            )}
                          </Form.Item>
                        </div>
                      </Col>
                    </Row>
                    <Col offset={1}>
                      <Button
                        onClick={e => this.showDeleteConfirm(k)}
                        type="danger"
                      >
                        Remove Form
                      </Button>
                    </Col>
                  </fieldset>
                  <br />
                </div>
              );
            })}
            <br />
            <Row>
              <Col span={23}>
                <Button
                  onClick={e => this.addForm(e)}
                  type="primary"
                  style={{ float: "right" }}
                >
                  Add New Product
                </Button>
              </Col>
            </Row>

            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label="Source Name">
                    {form.getFieldDecorator("source_id", {
                      rules: [
                        { required: true, message: "Please Enter Source Name" }
                      ],
                      initialValue: this.state.repatriation.source_id
                    })(
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select a Source Name"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        { this.state.source_names.map(site => {
                              return (
                                <Option value={site.id} key={site.id}>
                                  {site.name}
                                </Option>
                              );
                            })
                        }
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="Destination Name">
                    {form.getFieldDecorator("destination_id", {
                      rules: [
                        {
                          required: true,
                          message: "Please Enter Destination Name"
                        }
                      ],
                      initialValue: this.state.repatriation.destination_id
                    })(
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select a Destination Name"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {isAdmin()?
                          this.state.destination_names.map(destination => {
                          return (
                            <Option value={destination.id} key={destination.id}>
                              {destination.name}
                            </Option>
                          );
                        })
                      :
                      this.state.destination_names.map(destination => {
                        if (destination.id === Cookie.get("site_id")) {
                        return (
                          <Option value={destination.id} key={destination.id}>
                            {destination.name}
                          </Option>
                        );
                        }else {
                          return null;
                        }
                      })
                      }
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="Reason For Repatriation:">
                    {form.getFieldDecorator("reason", {
                      rules: [
                        {
                          required: true,
                          message: "Please cause for Repatriation"
                        }
                      ],
                      initialValue: this.state.repatriation.reason
                    })(
                      <Input
                        placeholder="Cause for Repatriation"
                        name="reason"
                      />
                    )}
                  </Form.Item>
                </div>
              </Col>

              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="">
                    {form.getFieldDecorator("source_type", {
                      rules: [
                        { required: true, message: "Please Enter Site Type" }
                      ],
                      initialValue: "Contractor"
                    })(<input setfieldsvalue="Contractor" type="hidden" />)}
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="">
                    {form.getFieldDecorator("destination_type", {
                      rules: [
                        {
                          required: true,
                          message: "Please Enter Destination Type"
                        }
                      ],
                      initialValue: "Site"
                    })(<input setfieldsvalue="Site" type="hidden" />)}
                  </Form.Item>
                </div>
              </Col>

              <Col span={6} offset={1}>
                <Form.Item {...formItemLayoutWithOutLabel}>
                  <Button
                    type="dashed"
                    onClick={this.add.bind(this)}
                    style={{ width: "90%" }}
                  >
                    <Icon type="plus" />
                    Add Chalan Number
                  </Button>
                </Form.Item>
                {formItems}
              </Col>
            </Row>

            <Row>
              <Col>
                <div className="btn-group">
                  <Button htmlType="submit" className="btn btn-dark">
                    Update Repatriation
                  </Button>
                  &nbsp;
                  <Button
                    type="button"
                    onClick={this.handleCancel.bind(this)}
                    className="btn btn-secondary"
                  >
                    Cancel
                  </Button>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
      </CustomLayout>
    );
  }
}

export default Form.create({})(EditRepatriation);
