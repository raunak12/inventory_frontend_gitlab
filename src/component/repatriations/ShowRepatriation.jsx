import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { Modal } from 'antd';
import { repatriationActions } from '../../actions/RepatriationActions';
import {shipmentActions} from '../../actions/ShipmentActions';

const { confirm } = Modal;

class ShowRepatriation extends Component {
  constructor() {
    super();
    this.state = {}
  }

  componentDidMount() {
    repatriationActions.showRepatriatedItem(this.props.match.params.id).then( response =>  {
      this.setState({ repatriation: response.data})
    }).catch(error => console.log('error', error));
  }

  handleDelete() {
    shipmentActions.deleteShipment(this.state.repatriation.shipment_id).then(response => {
      if (response.status === 204){
        success('Repatriated product was successfully deleted!')
        this.props.history.push("/repatriations")
      }else{
        error('Unable to delete a repatriated Product!')
      }
    })
  }

  showDeleteConfirm() {
    confirm({
      title: 'Are you sure to delete this Repatrated Product?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleDelete.bind(this),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  renderRepatriation() {
    if (this.state.repatriation) {
      return (
        <div>
        <table className="table table-sm ">
          <tr>
            <td><h5>Repatriated Date:</h5> </td>
            <td><h5>{this.state.repatriation.date}</h5></td>
          </tr>
        </table>
        <table className="table table-sm view-table">
          <tr>
            <td><h5>Repatriation Id:</h5> </td>
            <td><h5>{this.state.repatriation.id}</h5></td>
            <td><h5>Repatriated Product:</h5></td>
            <td><h5>{this.state.repatriation.product_name}</h5></td>
          </tr>
         
          <tr>
            <td><h5>Repatriated  Product's Category:</h5></td>
            <td><h5>{this.state.repatriation.category_name}</h5></td>
            <td><h5>Quantity:</h5></td>
            <td><h5>{this.state.repatriation.quantity}</h5></td>
          </tr>
          <tr>
          <td><h5>Unit:</h5></td>
            <td><h5>{this.state.repatriation.unit}</h5></td>
            <td></td>
            <td></td>
          </tr>
        </table>
        <table className="table table-sm view-table">
          <tr>
            <td><h5>Chalan Number:</h5></td>
            <td><h5> {this.state.repatriation.chalan_no}</h5></td>
          </tr>
          <tr>
            <td><h5>Source Type:</h5></td>
            <td><h5>{this.state.repatriation.source_type}</h5></td>
            <td><h5>Source Name:</h5></td>
            <td><h5>{this.state.repatriation.source_name}</h5></td>
          </tr>
          <tr>
            <td><h5>Destination Type:</h5></td>
            <td><h5>{this.state.repatriation.destination_type}</h5></td>
            <td><h5>Destination Name:</h5></td>
            <td><h5>{this.state.repatriation.destination_name}</h5></td>
          </tr>
          <tr>
            <td><h5>Vehicle Number:</h5></td>
            <td><h5>{this.state.repatriation.vehicle_no}</h5></td>
          </tr>
        </table>
        <table className="table  table-sm view-table">
          <tr>
            <td><h5>Remarks:</h5></td>
            <td><h5>{this.state.repatriation.remarks}</h5></td>
            <td><h5>Reason for Return:</h5></td>
            <td><h5>{this.state.repatriation.reason}</h5></td>
          </tr>
        </table>
        <p>
          <button onClick={this.showDeleteConfirm.bind(this)} className="btn btn-danger">Delete</button> 
          <Link to="/repatriations" className="btn btn-primary">Back</Link>
        </p>
        <hr/>
        </div>
      )
    }
  }

  render() {
    return (
      <CustomLayout>
        {
          this.renderRepatriation()
        }
       
      </CustomLayout>
    )
  }
}

export default ShowRepatriation;