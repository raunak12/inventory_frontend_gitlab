
import React from 'react'
import { userService } from '../../services/user.service';

export default class Logout extends React.Component {

   onHandelClick=()=> {
      userService.logout();
   }
    render() {
        return (
              <div className="ant-dropdown-link"  onClick={this.onHandelClick}>
                Logout
                </div>
        )
      }
}