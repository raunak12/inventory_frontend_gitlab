import React from 'react'
import 'antd/dist/antd.css'
import Logout from '../../Logout/LogOut'
import { Layout, Dropdown, Menu } from 'antd';
import Cookie from "js.cookie";
const { Header } = Layout


export default class Navigation extends React.Component {


    render() {
        const menu = (
            <Menu>
              <Menu.Item>
                <Logout />
              </Menu.Item>
            </Menu>
          );

        var dropdownStyle = {
            float: 'right'
        }

        return (
            <Header className="header">
            <div className="logo" />
            <div style={{color: 'white', fontSize: '30px', float: 'left'}}><b> RoadShow Inventory Management System</b></div>
            <div style={dropdownStyle}>
            <Dropdown overlay={menu}>
              <div className="ant-dropdown-link" style={{color: 'white'}} >
                {Cookie.get('uid')}
              </div>
            </Dropdown>
            </div>
          </Header>
        )
      }
}