import React, { Component } from 'react';
import { Layout } from 'antd';
import Sidebar from '../Sidebar/Sidebar';
import 'antd/dist/antd.css';
import Navigation from '../navigation/Navigation';

const { Content } = Layout;

class CustomLayout extends Component {
  render() {
    return (
      <Layout>
         <Navigation />
        <Layout>
          <Sidebar selectedKey={this.props.sidebarSelectedKey} />
          <Layout >
            <Content>
              {this.props.children}
            </Content>
          </Layout>
        </Layout>
      </Layout>
    )
  }
}

export default CustomLayout
