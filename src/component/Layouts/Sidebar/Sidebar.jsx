import React from "react";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";
import { Layout, Menu, Icon } from "antd";
const { Sider } = Layout;

export default class Sidebar extends React.Component {
  constructor() {
    super();
    this.state = { collapsed: false };
  }

  onCollapse(collapsed) {
    console.log(collapsed);
    this.setState({ collapsed });
  }

  render() {
    return (
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse.bind(this)}
        style={{ minHeight: "100vh" }}
      >
        <div className="logo" />
        <Menu
          theme="dark"
          defaultSelectedKeys={[this.props.selectedKey]}
          mode="inline"
        >
          <Menu.Item key="dashboard">
            <Link to="/">
              <Icon type="dashboard" />
              <span>DASHBOARD</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="purchases">
            <Link to="/bills">
              <Icon type="database" />
              <span>PURCHASES</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="dispatches">
            <Link to="/dispatches">
              <Icon type="carry-out" />
              <span>DISPATCHES</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="consumed">
            <Link to="/consumptions">
              <Icon type="code-sandbox" />
              <span>CONSUMPTIONS</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="repatriations">
            <Link to="/repatriations">
              <Icon type="rollback" />
              <span>REPATRIATIONS</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="products">
            <Link to="/products">
              <Icon type="code-sandbox" />
              <span>PRODUCTS</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="categories">
            <Link to="/categories">
              <Icon type="appstore" />
              <span>CATEGORIES</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="sites">
            <Link to="/sites">
              <Icon type="environment" />
              <span>SITES</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="contractors">
            <Link to="/contractors">
              <Icon type="idcard" />
              <span>CONTRACTORS</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="vendors">
            <Link to="/vendors">
              <Icon type="shop" />
              <span>VENDORS</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="reports">
            <Link to="/reports">
              <Icon type="file" />
              <span>REPORTS</span>
            </Link>
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}
