import React, { Component } from 'react';
import { Form, Input, Modal, Select, Button, DatePicker, Row, Col } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import moment from 'moment';
import { success, error } from '../../helper/notifications';
import { shipmentActions } from '../../actions/ShipmentActions';
import { consumptionActions } from '../../actions/ConsumptionActions';
import { contractorActions } from '../../actions/ContractorActions'
import { siteActions } from '../../actions/SiteActions';
import { categoryActions } from '../../actions/CategoryActions';
import { productActions } from '../../actions/ProductActions';
import { isAdmin } from '../../helpers/userPolicy';
import Cookie from "js.cookie";

const { confirm } = Modal;
const { Option } = Select;
let id = 0


class ConsumedEdit extends Component {
  constructor() {
    super();
    this.state = {
      consumedForms: [],
      categories: [],
      sites: [],
      products: [],
      category: [],
      consumption: {},
      destination_names: []
    };
  }

  componentDidMount() {
    consumptionActions.showConsumption(this.props.match.params.id).then(response => {
      this.setState({
        consumption: response.data,
        consumedForms: response.data.consumed_items.map((_d, index) => index)
      });
      if (response.data.destination_type === "Contractor") {
        contractorActions.fetchContractors().then(response => {
          this.setState({ destination_names: response.data });
        }).catch(error => console.log('error', error));
      }
    }).catch(error => console.log('error', error));

    siteActions.fetchSites().then(response => {
      this.setState({ sites: response.data });
    }).catch(error => console.log('error', error));

    productActions.fetchproducts().then(response => {
      this.setState({ products: response.data });
    }).catch(error => console.log('error', error));

    categoryActions.fetchCategories().then(response => {
      this.setState({ categories: response.data });
    }).catch(error => console.log('error', error));

  }

  handleChange(value, k) {
    this.props.form.setFieldsValue({ [`consumed_items_attributes[${k}][shipment_attributes][product_id]`]: '' })
    categoryActions.fetchProductsCategory(value).then(response => {
      this.setState({ products: response.data })
    }).catch(error => console.log('error', error));

    categoryActions.showCategory(value).then(response => {
      this.setState({ category: response.data })
      this.props.form.setFieldsValue({ [`consumed_items_attributes[${k}][shipment_attributes][unit]`]: response.data.unit })
    }).catch(error => console.log('error', error));
  }

  handelSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        consumptionActions.updateConsumption(this.state.consumption.id, values).then(response => {
          if (response.status === 200) {
            success('Successfully Updated the Consumed Item!')
            this.props.history.push(`/consumptions`);
            return response.json()

          } else {
            error('Unable to Edit the consumption!')
          }
        }).catch(error => console.log('error', error));
      }
    })
  }

  handleCancel() {
    this.props.history.push('/consumptions')

  }

  addForm() {
    const { consumedForms } = this.state
    const newKey = (consumedForms.length > 0) ? (consumedForms[consumedForms.length - 1] + 1) : 0
    this.setState({ consumedForms: [...consumedForms, newKey] })
  }

  showDeleteConfirm(k) {
    confirm({
      title: 'Are you sure to remove this Shipment?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () => this.removeForm(k),
    });
  }

  removeForm(k) {
    const { consumedForms } = this.state
    const consumedItem = this.state.consumption.consumed_items[k]
    if (consumedItem && consumedItem.shipment_id) {
      shipmentActions.deleteShipment(this.state.consumption.consumed_items[k].shipment_id).then(response => {
        if (response.status === 204) {
          success('Consumed product was successfully deleted!')
        } else {
          error('Unable to delete a purchase!')
        }
      })
    }
    this.setState({
      consumedForms: consumedForms.filter(key => k !== key)
    })
  }

  remove(k) {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    if (keys.length === 1) {
      return;
    }
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }

  add() {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
      keys: nextKeys,
    });
  }

  handleChangeDestinationType(value) {
    this.props.form.setFieldsValue({ [`destination_id`]: '' })
    if (value === "Contractor") {
      contractorActions.fetchContractors().then(response => {
        this.setState({ destination_names: response.data })
      }).catch(error => console.log('error', error));
    }else{
      this.setState({
        destination_names: []
      })
    }
  }

  render() {
    const { form } = this.props
    return (
      <CustomLayout>
        <br />
        <Col>
          <h3>EDIT CONSUMPTION</h3>
        </Col>
        <div>
          <Form onSubmit={this.handelSubmit.bind(this)}>

            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label='Consumption Date'>
                    {
                      form.getFieldDecorator('date', {
                        rules: [{ required: true, message: 'Please Enter Date' }],
                        initialValue: moment.utc(this.state.consumption.date)
                      })(
                        <DatePicker />
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label='Building Number'>
                    {
                      form.getFieldDecorator('building_no', {
                        initialValue: this.state.consumption.building_no
                      })(
                        <Input
                          placeholder="Building Number"
                          name='building_no'
                        />
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
            </Row>
            {this.state.consumedForms.map((k, index) => {
              const consumedItem = this.state.consumption.consumed_items[k]
              return (
                <div key={k}>
                  <fieldset key={k}>
                    <Row>
                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label='Category:'>
                            {
                              form.getFieldDecorator(`consumed_items_attributes[${k}][shipment_attributes][category_id]`, {
                                rules: [{ required: true, message: 'Please Select the category' }],
                                initialValue: consumedItem && consumedItem['category_id'],
                                 onChange: (value) => this.handleChange(value, k)
                              })(
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select a Category"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }>
                                  {
                                    this.state.categories.map(category => {
                                      return (
                                        <Option value={category.id} key={category.id}>
                                          {category.name}
                                        </Option>
                                      )
                                    })
                                  }
                                </Select>,
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label='Product'>
                            {
                              form.getFieldDecorator(`consumed_items_attributes[${k}][shipment_attributes][product_id]`, {
                                rules: [{ required: true, message: 'Please Enter Product Name' }],
                                initialValue: consumedItem && consumedItem['product_id']
                              })(
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select Product"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }
                                >
                                  {
                                    this.state.products.map(product => {
                                      return (
                                        <Option value={product.id} key={product.id}>
                                          {product.name}
                                        </Option>
                                      )
                                    })
                                  }
                                </Select>,
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>
                      <Col span={4} offset={1}>
                        <Form.Item label='Remarks'>
                          {
                            form.getFieldDecorator(`consumed_items_attributes[${k}][shipment_attributes][remarks]`, {
                              initialValue: consumedItem && consumedItem['remarks']
                            })(
                              <Input
                                placeholder="Remarks"
                                name='remarks'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>
                    
                      <Col span={4} offset={1} >
                        <Form.Item label='Quantity'>
                          {
                            form.getFieldDecorator(`consumed_items_attributes[${k}][shipment_attributes][quantity]`, {
                              rules: [{ required: true, message: 'Please Enter Quantity' }],
                              initialValue: consumedItem && consumedItem['quantity']
                            })(
                              <Input
                                placeholder="Quantity"
                                name='quantity'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>
                      <Col span={4} offset={1} >
                        <Form.Item label='Unit'>
                          {
                            form.getFieldDecorator(`consumed_items_attributes[${k}][shipment_attributes][unit]`, {
                              initialValue: consumedItem && consumedItem['unit']
                            })(
                              <Input
                                placeholder="Unit"
                                name='unit'
                                readOnly='readOnly'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>
                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item>
                            {
                              form.getFieldDecorator(`consumed_items_attributes[${k}][shipment_attributes][id]`, {
                                initialValue: consumedItem && consumedItem['shipment_id']
                              })(
                                <Input
                                  type='hidden'
                                  placeholder="Unit"
                                  name='shipment_id'
                                  readOnly='readOnly'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>
                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item>
                            {
                              form.getFieldDecorator(`consumed_items_attributes[${k}][id]`, {
                                initialValue: consumedItem && consumedItem['consumed_item_id']
                              })(
                                <Input
                                  type='hidden'
                                  placeholder="Unit"
                                  name='consumed_item_id'
                                  readOnly='readOnly'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={4} offset={1}>
                        <Button onClick={(e) => this.showDeleteConfirm(k)} type="danger" >Remove Form</Button>
                      </Col>
                    </Row>
                  </fieldset>
                  <br />
                </div>
              )
            })}
            <br />
            <Row>
              <Col span={23}>
                <Button onClick={(e) => this.addForm(e)} type='primary' style={{ float: 'right' }}>Add New Product</Button>
              </Col>
            </Row>
            <div>
              <Row>
                <Col span={4}>
                  <div>
                    <Form.Item label='Source Name'>
                      {
                        form.getFieldDecorator('source_id', {
                          rules: [{ required: true, message: 'Please Enter Source Name' }],
                          initialValue: this.state.consumption.source_id
                        })(
                          <Select
                            showSearch
                            style={{ width: 200 }}
                            placeholder="Select a Source Name"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {
                              isAdmin() ?
                                this.state.sites.map(site => {
                                  return (
                                    <Option value={site.id} key={site.id}>
                                      {site.name}
                                    </Option>
                                  )
                                })
                                :
                                this.state.sites.map(site => {
                                  if (site.id === Cookie.get('site_id')) {
                                    return (
                                      <Option value={site.id} key={site.id}>
                                        {site.name}
                                      </Option>
                                    )
                                  }else {
                                    return null;
                                  }
                                })
                            }
                          </Select>,
                        )
                      }
                    </Form.Item>
                  </div>
                </Col>

                <Col span={4} offset={1}>
                  <div>
                    <Form.Item label=''>
                      {
                        form.getFieldDecorator('source_type', {
                          initialValue: this.state.consumption.source_type
                        })(
                          <Input placeholder="Site" type="hidden" setfieldsvalue="Site" style={{ width: 200 }} />
                        )
                      }
                    </Form.Item>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col span={4}>
                  <div>
                    <Form.Item label='Destination_type:'>
                      {
                        form.getFieldDecorator('destination_type', {
                          initialValue: this.state.consumption.destination_type
                        })(
                          <Select
                            showSearch
                            style={{ width: 200 }}
                            placeholder="Select Destination type"
                            optionFilterProp="children"
                            onChange={this.handleChangeDestinationType.bind(this)}
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            <Option value="">None</Option>
                            <Option value="Contractor">Contractor</Option>
                          </Select>
                        )
                      }
                    </Form.Item>
                  </div>
                </Col>
                <Col span={4} offset={1}>
                  <div>
                    <Form.Item label='Destination Name'>
                      {
                        form.getFieldDecorator('destination_id', {
                          initialValue: this.state.consumption.destination_id
                        })(
                          <Select
                            showSearch
                            style={{ width: 200 }}
                            placeholder="Select a Destination Name"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {
                              this.state.destination_names.map(destination => {
                                return (
                                  <Option value={destination.id} key={destination.id}>
                                    {destination.name}
                                  </Option>
                                )
                              })
                            }
                          </Select>,
                        )
                      }
                    </Form.Item>
                  </div>
                </Col>
              </Row>
            </div>
            <Row>
              <Col>
                <div className="btn-group">
                  <Button htmlType="submit" className="btn btn-dark">Update Consumption</Button>
                  &nbsp;
                  <Button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</Button>
                </div>
              </Col>
            </Row>
            <br />
          </Form>

        </div>
      </CustomLayout>
    );
  }
}

export default Form.create({})(ConsumedEdit);
