import React, { Component } from 'react'
import CustomLayout from '../Layouts/CustomLayout';
import { Link } from 'react-router-dom'
import Tableconsumed from './Table'
import { consumptionActions } from '../../actions/ConsumptionActions';
import LoadingSpinner from '../../helpers/LoadingSpinner';

class ConsumedLists extends Component {
  constructor() {
    super()
    this.state = { consumedItems: [], isLoading: true }
  }

  componentDidMount() {
    consumptionActions.fetchConsumedItems().then(response => {
      this.setState({ consumedItems: response.data, isLoading: false });
  }).catch(error => console.log('error', error));
  }

  showTable() {
    const { consumedItems } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <Tableconsumed table_data={consumedItems} /> 
      )
    }
  }

  render() {
    return (
      <CustomLayout sidebarSelectedKey='consumed'>
        <h1>Consumptions</h1>
        <Link to="/consumptions/new" className="btn btn-outline-primary mar-pad float-right">
          Create Consumption
                   </Link>
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default ConsumedLists

