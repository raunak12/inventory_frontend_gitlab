import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { Modal } from 'antd';
import { shipmentActions } from '../../actions/ShipmentActions';
import { consumptionActions } from '../../actions/ConsumptionActions';

const { confirm } = Modal;

class ViewConsumed extends Component {
    constructor() {
        super();
        this.state = { consumed: {}};
    }

    componentDidMount() {
        consumptionActions.showConsumedItem(this.props.match.params.id).then( response => {
            this.setState({ consumed: response.data });
        }).catch(error => console.log('error', error));
    }

    handleDelete() {
        shipmentActions.deleteShipment(this.state.consumed.id).then(response => {
            if (response.status === 204){
              success('Consumed product was successfully deleted!')
              this.props.history.push("/consumptions")
            }else{
              error('Unable to delete a consumend Product!')
            }
          }).catch(error => console.log('error', error));
    }

    showDeleteConfirm() {
        confirm({
            title: 'Are you sure to delete this Purchase?',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: this.handleDelete.bind(this),
            onCancel: () => {
                console.log('Cancel');
            },
        });
    }

    render() {
        return (
            <CustomLayout>
                <div>
                    <table className="table table-sm ">
                        <tr>
                            <td><h5>Consuemd Id:</h5> </td>
                            <td><h5>{this.state.consumed.id}</h5></td>
                            <td><h5>Consumption Date:</h5> </td>
                            <td><h5>{this.state.consumed.date}</h5></td>
                        </tr>
                    </table>
                    <table className="table table-sm view-table">
                        <tr>
                            <td><h5>Building No:</h5> </td>
                            <td><h5>{this.state.consumed.building_no}</h5></td>
                            <td><h5>Consumed Product:</h5></td>
                            <td><h5>{this.state.consumed.product}</h5></td>
                        </tr>

                        <tr>
                            <td><h5> Consumed Product's Category:</h5></td>
                            <td><h5>{this.state.consumed.category}</h5></td>
                            <td><h5>Consumed Product's Quantity:</h5></td>
                            <td><h5>{this.state.consumed.quantity}</h5></td>
                        </tr>
                        <tr>
                            <td><h5>Unit:</h5></td>
                            <td><h5>{this.state.consumed.unit}</h5></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    <table className="table table-sm view-table">
                        <tr>
                            <td><h5>Remarks:</h5></td>
                            <td><h5>{this.state.consumed.remarks}</h5></td>
                            <td><h5>Source Type:</h5></td>
                            <td><h5>{this.state.consumed.source_type}</h5></td>
                        </tr>
                    </table>
                    <table className="table  table-sm view-table">

                        <tr>
                            <td><h5>Source Name:</h5></td>
                            <td><h5>{this.state.consumed.source_name}</h5></td>
                            <td><h5>Destination Type:</h5></td>
                            <td><h5>{this.state.consumed.destination_type}</h5></td>
                        </tr>
                        <tr>
                            <td><h5>Destination Name:</h5></td>
                            <td><h5>{this.state.consumed.destination_name}</h5></td>
                        </tr>
                    </table>
                    <p>
                        <button onClick={this.showDeleteConfirm.bind(this)} className="btn btn-danger">Delete</button>
                        <Link to="/consumptions" className="btn btn-primary">Back</Link>
                    </p>
                    <hr />
                </div>
            
            </CustomLayout>

        )

    }


}

export default ViewConsumed;