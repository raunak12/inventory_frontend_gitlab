import React, { Component } from 'react';
import { Form, Input, Select, Button, DatePicker, Row, Col } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import moment from 'moment';
import { success, error } from '../../helper/notifications';
import { consumptionActions } from '../../actions/ConsumptionActions';
import { contractorActions } from '../../actions/ContractorActions'
import { siteActions } from '../../actions/SiteActions';
import { categoryActions } from '../../actions/CategoryActions';
import { isAdmin } from '../../helpers/userPolicy';
import Cookie from "js.cookie";


const { Option } = Select;
let id = 0


class ConsumeAdd extends Component {
  constructor() {
    super();
    this.state = {
      comsumptionForm: [0],
      categories: [],
      products: [],
      category: {},
      source_names: [],
      destination_names: []
    };
  }

  componentDidMount() {
    categoryActions.fetchCategories().then(response => {
      this.setState({ categories: response.data });
    }).catch(error => console.log('error', error));

    siteActions.fetchSites().then(response => {
      this.setState({ source_names: response.data });
    }).catch(error => console.log('error', error));
  }



  handleChange(value, k) {
    this.props.form.resetFields(`consumed_items_attributes[${k}][shipment_attributes][product_id]`)
    categoryActions.fetchProductsCategory(value).then(response => {
      this.setState({ products: response.data })
    }).catch(error => console.log('error', error));

    categoryActions.showCategory(value).then(response => {
      this.setState({category: response.data})
      this.props.form.setFieldsValue({[`consumed_items_attributes[${k}][shipment_attributes][unit]`]: response.data.unit })
    }).catch(error => console.log('error', error));
  }

  handleChangeDestinationType(value) {
    this.props.form.resetFields('destination_id')
    if (value === "Contractor") {
      contractorActions.fetchContractors().then(response => {
        this.setState({ destination_names: response.data })
      }).catch(error => console.log('error', error));
    }else{
      this.setState({
        destination_names: []
      })
    }
  }

  handelSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        consumptionActions.createConsumption(values).then(response => {
          if (response.status === 201) {
            success("Successfully Added Consumption Item!")
            this.props.history.push(`/consumptions`);
            return response.json()
          } else {
            error("Unable to add Consumption Item!")
          }
        }).catch(error => console.log('error', error));
      }
    })
  }

  handleCancel() {
    this.props.history.push('/consumptions')
  }

  addForm() {
    const { comsumptionForm } = this.state
    const newKey = (comsumptionForm.length > 0) ? (comsumptionForm[comsumptionForm.length - 1] + 1) : 0
    this.setState({ comsumptionForm: [...comsumptionForm, newKey] })
  }

  removeForm(key) {
    const { comsumptionForm } = this.state
    this.setState({
      comsumptionForm: comsumptionForm.filter(k => k !== key)
    })
  }

  remove(k) {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    if (keys.length === 1) {
      return;
    }
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }

  add() {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
      keys: nextKeys,
    });
  }

  render() {
    const { form } = this.props
    return (
      <CustomLayout>
        <br />
        <Col>
          <h3>ADD NEW CONSUMPTION</h3>
        </Col>
        <div>
          <Form onSubmit={this.handelSubmit.bind(this)}>
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label='Consumption Date'>
                    {
                      form.getFieldDecorator('date', {
                        rules: [{ required: true, message: 'Please Enter Date' }],
                        initialValue: moment()
                      })(
                        <DatePicker />
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label='Building No:'>
                    {
                      form.getFieldDecorator('building_no', {
                      })(
                        <Input
                          placeholder="Building Number"
                          name='building_no'
                        />
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
            </Row>

            {this.state.comsumptionForm.map((k, idx) => {
              return (
                <div key={k}>
                  <fieldset key={idx}>
                    <Row>
                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label='Category:'>
                            {
                              form.getFieldDecorator(`consumed_items_attributes[${idx}][shipment_attributes][category_id]`, {
                                rules: [{ required: true, message: 'Please Select the category' }],
                                onChange: (value) => this.handleChange(value, idx)
                              })(
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select a Category"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }>
                                  {
                                    this.state.categories.map(category => {
                                      return (
                                        <Option value={category.id} key={category.id}>
                                          {category.name}
                                        </Option>
                                      )
                                    })
                                  }
                                </Select>,
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label='Product'>
                            {
                              form.getFieldDecorator(`consumed_items_attributes[${idx}][shipment_attributes][product_id]`, {
                                rules: [{ required: true, message: 'Please Enter Product Name' }],
                              })(
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select Product"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }
                                >
                                  {
                                    this.state.products.map(product => {
                                      return (
                                        <Option value={product.id} key={product.id}>
                                          {product.name}
                                        </Option>
                                      )
                                    })
                                  }
                                </Select>,
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label='Remarks'>
                          {
                            form.getFieldDecorator(`consumed_items_attributes[${idx}][shipment_attributes][remarks]`, {
                            })(
                              <Input
                                placeholder="Remarks"
                                name='remarks'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>


                      <Col span={4} offset={1} >
                        <Form.Item label='Quantity'>
                          {
                            form.getFieldDecorator(`consumed_items_attributes[${idx}][shipment_attributes][quantity]`, {
                              rules: [{ required: true, message: 'Please Enter Quantity' }],
                            })(
                              <Input
                                placeholder="Quantity"
                                name='quantity'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1} >
                        <Form.Item label='Unit'>
                          {
                            form.getFieldDecorator(`consumed_items_attributes[${idx}][shipment_attributes][unit]`, {
                              initialValue: this.state.category.unit
                            })(
                              <Input
                                placeholder="Unit"
                                name='unit'
                                readOnly='readOnly'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>
                      <Col span={4} offset={1} > </Col>
                    </Row>
                    <Row>
                      <Col span={4} offset={1}>
                        <Button onClick={(e) => this.removeForm(k)} type="danger" >Remove Form</Button>
                      </Col>
                    </Row>
                  </fieldset>
                  <br />
                </div>
              )
            })}
            <br />
            <Row>
              <Col span={23}>
                <Button onClick={(e) => this.addForm(e)} type='primary' style={{ float: 'right' }}>Add New Product</Button>
              </Col>
            </Row>
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label='Source Name'>
                    {
                      form.getFieldDecorator('source_id', {
                        rules: [{ required: true, message: 'Please Enter Source Name' }],
                      })(
                        <Select
                          showSearch
                          style={{ width: 200 }}
                          placeholder="Select a Source Name"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {
                            isAdmin() ?
                              this.state.source_names.map(site => {
                                return (
                                  <Option value={site.id} key={site.id}>
                                    {site.name}
                                  </Option>
                                )
                              })
                              :
                              this.state.source_names.map(site => {
                                if (site.id === Cookie.get('site_id')) {
                                  return (
                                    <Option value={site.id} key={site.id}>
                                      {site.name}
                                    </Option>
                                  )
                                }else {
                                  return null;
                                }
                              })
                          }
                        </Select>,
                      )
                    }
                  </Form.Item>
                </div>
              </Col>

              <Col span={4} offset={1}>
                <div>
                  <Form.Item label=''>
                    {
                      form.getFieldDecorator('source_type', {
                        initialValue: "Site"
                      })(
                        <Input placeholder="Site" type="hidden" setfieldsvalue="Site" style={{ width: 200 }} />
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label='Destination_type:'>
                    {
                      form.getFieldDecorator('destination_type', {
                      })(
                        <Select
                          showSearch
                          style={{ width: 200 }}
                          placeholder="Select Destination type"
                          optionFilterProp="children"
                          onChange={this.handleChangeDestinationType.bind(this)}
                          filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          <Option value="">None</Option>
                          <Option value="Contractor">Contractor</Option>
                        </Select>
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label='Destination Name'>
                    {
                      form.getFieldDecorator('destination_id', {
                      })(
                        <Select
                          showSearch
                          style={{ width: 200 }}
                          placeholder="Select a Destination Name"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {
                            this.state.destination_names.map(destination => {
                              return (
                                <Option value={destination.id} key={destination.id}>
                                  {destination.name}
                                </Option>
                              )
                            })
                          }
                        </Select>,
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
            </Row>

            <Row>
              <Col>
                <div className="btn-group">
                  <Button htmlType="submit" className="btn btn-dark">Create Consumption</Button>
                  &nbsp;
                  <Button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</Button>
                </div>
              </Col>
            </Row>
            <br />
          </Form>

        </div>
      </CustomLayout>
    );
  }
}

export default Form.create({})(ConsumeAdd);
