import React, { Component } from "react";
import {
  Table,
  Icon,
  Divider,
  Modal,
  Button,
  Row,
  Col,
  Descriptions
} from "antd";
import { Link } from "react-router-dom";
import { success, error } from "../../helper/notifications";
import { consumptionActions } from "../../actions/ConsumptionActions";
import { shipmentActions } from "../../actions/ShipmentActions";
import { sortText, sortDate } from "../../helpers/sort";
import { getColumnSearchProps } from "../../helpers/tableSearch";

const { confirm } = Modal;

class ConsumptionTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      consumptions: props.table_data,
      visible: false,
      consumed: {}
    };
  }

  handleDelete(shipment_id) {
    shipmentActions.deleteShipment(shipment_id).then(response => {
      if (response.status === 204) {
        consumptionActions
          .fetchConsumedItems()
          .then(response => {
            this.setState({ consumptions: response.data });
          })
          .catch(error => console.log("error", error));
        success("Consumed product was successfully deleted!");
      } else {
        error("Unable to delete Consumption!");
      }
    });
  }

  showDeleteConfirm(shipment_id) {
    confirm({
      className: "show-modal-delete",
      title: "Are you sure to delete this Consumption?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk: () => this.handleDelete(shipment_id),
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }

  showModal = value => {
    consumptionActions.showConsumedItem(value).then(response => {
      this.setState({ consumed: response.data,
      visible: true
      });
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  render() {
    const columns = [
      {
        title: "Date",
        dataIndex: "shipment_date",
        key: "shipment_date",
        sorter: (a, b) => sortDate(a.shipment_date, b.shipment_date),
        ...getColumnSearchProps("shipment_date", this)
      },
      {
        title: "Product Name",
        dataIndex: "product",
        key: "product",
        sorter: (a, b) => sortText(a.product, b.product),
        ...getColumnSearchProps("product", this)
      },
      {
        title: "Category",
        dataIndex: "category",
        key: "category"
      },
      {
        title: "Quantity",
        dataIndex: "quantity",
        key: "quantity"
      },
      {
        title: "Unit",
        dataIndex: "unit",
        key: "unit"
      },
      {
        title: "Source Type",
        dataIndex: "source_type",
        key: "source_type"
      },
      {
        title: "Source Name",
        dataIndex: "source_name",
        key: "source_name",
        sorter: (a, b) => sortText(a.source_name, b.source_name),
        ...getColumnSearchProps("source_name", this)
      },
      {
        title: "Destination Type",
        dataIndex: "destination_type",
        key: "destination_type"
      },
      {
        title: "Destination Name",
        dataIndex: "destination_name",
        key: "destination_name",
        ...getColumnSearchProps("destination_name", this)
      },
      {
        title: "",
        width: "8%",
        key: "action",
        render: (text, record) => {
          return (
            <span>
              <Link to={`consumptions/${record.consumption_id}/edit`}>
                <Icon 
                title="Edit" 
                type="edit" 
                />
              </Link>
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon 
                  type="snippets"
                  title="View"
                  onClick={() => this.showModal(`${record.id}`)}
                />
              </span>
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon
                  type="delete"
                  title="Delete"
                  onClick={() => this.showDeleteConfirm(record.shipment_id)}
                />
              </span>
            </span>
          );
        }
      }
    ];
    const consumptions = this.state.consumptions;
    const view_table =
      consumptions && consumptions.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={consumptions} size="middle"  rowKey={column => column.id}/>
        </div>
      ) : (
        "No Data Found"
      );
    return (
      <div>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>
          ]}
        >
          <Row>
            <Col span={24} offset={6}>
              <h3>CONSUMED PRODUCT DETAIL</h3>
            </Col>
          </Row>

          <Row>
            <Col span={10}>
              <h4>Date: {this.state.consumed.date}</h4>
            </Col>
            <Col span={6} />
            <Col span={8} />
          </Row>
          <Descriptions title="" bordered>
            <Descriptions.Item label="Product Name">
              {this.state.consumed.product}
            </Descriptions.Item>
            <Descriptions.Item label="Quantity">
              {this.state.consumed.quantity}
            </Descriptions.Item>
            <Descriptions.Item label="Unit">
              {this.state.consumed.unit}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Category">
              {this.state.consumed.category}
            </Descriptions.Item>
            <Descriptions.Item label="Building Number">
              {this.state.consumed.building_no}
            </Descriptions.Item>
            <Descriptions.Item label="Source Name">
              {this.state.consumed.source_name}
            </Descriptions.Item>
            <Descriptions.Item label="Destination Type">
              {this.state.consumed.destination_type}
            </Descriptions.Item>
            <Descriptions.Item label="Destination Name">
              {this.state.consumed.destination_name}
            </Descriptions.Item>
            <Descriptions.Item label="Remarks">
              {this.state.consumed.remarks}
            </Descriptions.Item>
          </Descriptions>
        </Modal>
        {view_table}
      </div>
    );
  }
}
export default ConsumptionTable;
