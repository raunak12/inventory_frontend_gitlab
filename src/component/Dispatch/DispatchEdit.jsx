import React, { Component } from 'react';
import { Form, Input, Select, Button, DatePicker, Icon, Row, Col, message } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import moment from 'moment';
import { Modal } from 'antd';
import { productActions } from '../../actions/ProductActions';
import { siteActions } from '../../actions/SiteActions';
import { dispatchActions } from '../../actions/DispatchActions';
import { categoryActions } from '../../actions/CategoryActions';
import { shipmentActions } from '../../actions/ShipmentActions';
import { isAdmin } from '../../helpers/userPolicy';
import Cookie from "js.cookie";


const { confirm } = Modal;
const { Option } = Select;

const success = () => {
  message.success('Dispatched has been sucessfully updated.', 5);
};

const error = () => {
  message.error('Plese try again.', 5);
};
let id = 0


class DispatchEdit extends Component {
  constructor() {
    super();
    this.state = {
      dispatchForms: [],
      categories: [],
      sites: [],
      products: [],
      category: {},
      dispatch: {}
    };
  }

  componentDidMount() {
    dispatchActions.dispatches(this.props.match.params.id).then(response => {
      this.setState({
        dispatch: response.data,
        dispatchForms: response.data.dispatched_items.map((_d, index) => index)
      });
      id = response.data.chalan_no.length
    }).catch(error => console.log('error', error));


    siteActions.fetchSites().then(response => {
      this.setState({ sites: response.data });
    }).catch(error => console.log('error', error));

    categoryActions.fetchCategories().then(response => {
      this.setState({ categories: response.data })
    }).catch(error => console.log('error', error));

    productActions.fetchproducts().then(response => {
      this.setState({ products: response.data })
    }).catch(error => console.log('error', error));

  }


  handleChange(value, k) {
    this.props.form.setFieldsValue({ [`dispatched_items_attributes[${k}][shipment_attributes][product_id]`]: '' })
    categoryActions.fetchProductsCategory(value).then(response => {
      this.setState({ products: response.data })
    }).catch(error => console.log('error', error));

    categoryActions.showCategory(value).then(response => {
      this.setState({ category: response.data })
      this.props.form.setFieldsValue({ [`dispatched_items_attributes[${k}][shipment_attributes][unit]`]: response.data.unit })
    }).catch(error => console.log('error', error));
  }

  handelSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        dispatchActions.updateDispatch(this.state.dispatch.id, values)
        .then(response => {
          if (response.status === 200) {
            success('Dispatched product was successfully updated!')
            this.props.history.push(`/dispatches`);
            return response.json()
          }
        }).catch(error => console.log('error', error));
      }
    })
  }

  handleCancel() {
    this.props.history.push('/dispatches')

  }

  addForm() {
    const { dispatchForms } = this.state
    const newKey = (dispatchForms.length > 0) ? (dispatchForms[dispatchForms.length - 1] + 1) : 0
    this.setState({ dispatchForms: [...dispatchForms, newKey] })
  }

  showDeleteConfirm(k) {
    confirm({
      title: 'Are you sure to remove this Shipment?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () => this.removeForm(k),
    });
  }

  removeForm(k) {
    const { dispatchForms } = this.state
    const dispatchedItem = this.state.dispatch.dispatched_items[k]
    if (dispatchedItem && dispatchedItem.shipment_id) {
      shipmentActions.deleteShipment(this.state.dispatch.dispatched_items[k].shipment_id).then(response => {
        if (response.status === 204) {
          success('Dispatched product was successfully deleted!')
        } else {
          error('Unable to delete a Dispatched Product.!')
        }
      }).catch(error => console.log('error', error));
    }
    this.setState({
      dispatchForms: dispatchForms.filter(key => k !== key)
    })
  }

  remove(k) {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    if (keys.length === 1) {
      return;
    }
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }

  add() {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
      keys: nextKeys,
    });
  }

  render() {
    const { form } = this.props
    const { getFieldDecorator, getFieldValue } = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 },
      },
    };

    const chalanNumbers = this.state.dispatch.chalan_no ? this.state.dispatch.chalan_no : []
    const keysInitialValues = chalanNumbers.map((_chalan, index) => index)

    getFieldDecorator('keys', { initialValue: keysInitialValues });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => (
      <Form.Item
        {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        label={index === 0 ? 'Chalan' : ''}
        required={false}
        key={k}
      >
        {getFieldDecorator(`chalan_no[${k}]`, {
          validateTrigger: ['onChange', 'onBlur'],
          rules: [
            {
              whitespace: true
            },
          ],
          initialValue: chalanNumbers[k] && String(chalanNumbers[k])
        })(<Input placeholder="Add Chalan Number" style={{ width: '90%', marginRight: 8 }}/>)}
        {keys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
          />
        ) : null}
      </Form.Item>
    ));

    return (
      <CustomLayout>
        <br />
        <Col>
          <h3>EDIT DISPATCH</h3>
        </Col>
        <div>
          <Form onSubmit={this.handelSubmit.bind(this)}>
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label='Dispatch Date'>
                    {
                      form.getFieldDecorator('date', {
                        rules: [{ required: true, message: 'Please Enter Date' }],
                        initialValue: moment.utc(this.state.dispatch.date)
                      })(
                        <DatePicker />
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label='Bill_No'>
                    {
                      form.getFieldDecorator('bill_no', {
                        rules: [{ required: true, message: 'Please Enter Bill Number' }],
                        initialValue: this.state.dispatch.bill_no
                      })(
                        <Input
                          placeholder="Bill Number"
                          name='bill_no'
                        />
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
            </Row>
            {this.state.dispatchForms.map((k, index) => {
              const dispatchedItem = this.state.dispatch.dispatched_items[k]
              return (
                <div key={k}>
                  <fieldset key={k}>
                    <Row>
                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label='Category:'>
                            {
                              form.getFieldDecorator(`dispatched_items_attributes[${k}][shipment_attributes][category_id]`, {
                                rules: [{ required: true, message: 'Please Select the category' }],
                                initialValue: dispatchedItem && dispatchedItem['category_id'],
                                onChange: (value) => this.handleChange(value, k)
                              })(
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select a Category"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }>
                                  {
                                    this.state.categories.map(category => {
                                      return (
                                        <Option value={category.id} key={category.id}>
                                          {category.name}
                                        </Option>
                                      )
                                    })
                                  }
                                </Select>,
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label='Product'>
                            {
                              form.getFieldDecorator(`dispatched_items_attributes[${k}][shipment_attributes][product_id]`, {
                                rules: [{ required: true, message: 'Please Enter Product Name' }],
                                initialValue: dispatchedItem && dispatchedItem['product_id']
                              })(
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select Product"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }
                                >
                                  {
                                    this.state.products.map(product => {
                                      return (
                                        <Option value={product.id} key={product.id}>
                                          {product.name}
                                        </Option>
                                      )
                                    })
                                  }
                                </Select>,
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Description'>
                            {
                              form.getFieldDecorator(`dispatched_items_attributes[${k}][shipment_attributes][description]`, {
                                rules: [{ required: true, message: 'Please describe the product' }],
                                initialValue: dispatchedItem && dispatchedItem['description']
                              })(
                                <Input
                                  placeholder="Description"
                                  name='description'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <Form.Item label='Brand'>
                          {
                            form.getFieldDecorator(`dispatched_items_attributes[${k}][shipment_attributes][brand]`, {
                              initialValue: dispatchedItem && dispatchedItem['brand']
                            })(
                              <Input
                                placeholder="Brand Name"
                                name='brand'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1} >
                        <Form.Item label='Unit'>
                          {
                            form.getFieldDecorator(`dispatched_items_attributes[${k}][shipment_attributes][unit]`, {
                              initialValue: dispatchedItem && dispatchedItem['unit']
                            })(
                              <Input
                                placeholder="Unit"
                                name='unit'
                                readOnly='readOnly'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1} >
                        <Form.Item label='Quantity'>
                          {
                            form.getFieldDecorator(`dispatched_items_attributes[${k}][shipment_attributes][quantity]`, {
                              rules: [{ required: true, message: 'Please Enter Quantity' }],
                              initialValue: dispatchedItem && dispatchedItem['quantity']
                            })(
                              <Input
                                placeholder="Quantity"
                                name='quantity'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label='Remarks'>
                          {
                            form.getFieldDecorator(`dispatched_items_attributes[${k}][shipment_attributes][remarks]`, {
                              initialValue: dispatchedItem && dispatchedItem['remarks']
                            })(
                              <Input
                                placeholder="Remarks"
                                name='remarks'
                              />
                            )
                          }
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item>
                            {
                              form.getFieldDecorator(`dispatched_items_attributes[${k}][shipment_attributes][id]`, {
                                initialValue: dispatchedItem && dispatchedItem['shipment_id']
                              })(
                                <Input
                                  type='hidden'
                                  placeholder="Unit"
                                  name='unit'
                                  readOnly='readOnly'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>
                      
                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item>
                            {
                              form.getFieldDecorator(`dispatched_items_attributes[${k}][id]`, {
                                initialValue: dispatchedItem && dispatchedItem['dispatched_item_id']
                              })(
                                <Input
                                  type='hidden'
                                  placeholder="Unit"
                                  name='unit'
                                  readOnly='readOnly'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col offset={1} >
                        <Button onClick={(e) => this.showDeleteConfirm(k)} type='danger'>Remove Form</Button>
                      </Col>
                    </Row>
                  </fieldset>
                  <br />
                </div>
              )
            })}
            <Row>
              <Col span={23}>
                <Button onClick={(e) => this.addForm(e)} type='primary' style={{ float: 'right' }}>Add New Product</Button>
              </Col>
            </Row>
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label='Vehicle Number'>
                    {
                      form.getFieldDecorator('vehicle_no', {
                        initialValue: this.state.dispatch.vehicle_no
                      })(
                        <Input
                          placeholder="Vehicle Number"
                          name='vehicle_no'
                        />
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
              <Col span={6} offset={1}>
                <Form.Item {...formItemLayoutWithOutLabel}>
                  <Button type="dashed" onClick={this.add.bind(this)} style={{ width: '90%'}}>
                    <Icon type="plus" />
                    Add Chalan Number
                  </Button>
                </Form.Item>
                {formItems}
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label='Source'>
                    {
                      form.getFieldDecorator(`source_id`, {
                        rules: [{ required: true, message: 'Please Select the Source' }],
                        initialValue: this.state.dispatch.source_id
                      })(
                        <Select
                          showSearch
                          style={{ width: 200 }}
                          placeholder="Select Source Site"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {
                            isAdmin() ?
                              this.state.sites.map(site => {
                                return (
                                  <Option value={site.id} key={site.id}>
                                    {site.name}
                                  </Option>
                                )
                              })
                              :
                              this.state.sites.map(site => {
                                if (site.id === Cookie.get('site_id')) {
                                  return (
                                    <Option value={site.id} key={site.id}>
                                      {site.name}
                                    </Option>
                                  )
                                }else {
                                  return null;
                                }
                              })
                          }
                        </Select>
                      )
                    }

                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label='Destination'>
                    {
                      form.getFieldDecorator(`destination_id`, {
                        rules: [{ required: true, message: 'Please Select the Destiantion' }],
                        initialValue: this.state.dispatch.destination_id
                      })(
                        <Select
                          showSearch
                          style={{ width: 200 }}
                          placeholder="Select Destination Site"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {
                            this.state.sites.map(site => {
                              return (
                                <Option value={site.id} key={site.id}>
                                  {site.name}
                                </Option>
                              )
                            })
                          }
                        </Select>
                      )
                    }
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="btn-group">
                  <Button htmlType="submit" className="btn btn-dark">Update Dispatch</Button>&nbsp;
                  <Button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</Button>
                </div>
              </Col>
            </Row>
            <br />
          </Form>

        </div>
      </CustomLayout>
    );
  }
}

export default Form.create({})(DispatchEdit);
