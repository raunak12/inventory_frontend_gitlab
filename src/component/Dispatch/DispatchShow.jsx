import React, { Component } from 'react';
import { success, error } from '../../helper/notifications';
import { Modal, Button, Row, Col, Descriptions } from 'antd';

import { shipmentActions } from '../../actions/ShipmentActions';
import { dispatchActions } from '../../actions/DispatchActions';

const { confirm } = Modal;

class DispatchShow extends Component {
  constructor() {
    super();
    this.state = { purchase: {} };
  }

  componentDidMount() {
    dispatchActions.fetchDispatched(this.props.match.params.id).then( response => {
      this.setState({dispatches: response.data});
    }).catch(error => console.log('error', error));
  }

  handleDelete() {
       shipmentActions.deleteShipment(this.state.dispatches.id).then(response => {
        if (response.status === 204){
          success('Dispatched product was successfully deleted!')
          this.props.history.push("/dispatches")
        }else{
          error('Unable to delete a purchase!')
        }
      }).catch(error => console.log('error', error));
  }

  showDeleteConfirm() {
    confirm({
      title: 'Are you sure to delete this dispatch?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleDelete.bind(this),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  render() {
    return (
      <Modal
        visible={this.state.visible}
        onCancel={this.handleCancel}
        footer={[
          <Button key="back" onClick={this.handleCancel}>
            Back
          </Button>,
          <Button
            key="submit"
            type="danger"
            onClick={this.handleDelete.bind(this)}
          >
            Delete
          </Button>
        ]}
      >
        <Row>
       
          <Col span={24} offset={6}>
            <h3>DISPATCHED PRODUCT DETAIL</h3>
          </Col>
      
        </Row>

        <Row>
          <Col span={10}>
            <h4>Date: {this.state.purchase.bill_date}</h4>
          </Col>
          <Col span={6} />
          <Col span={8}>
            <h4> Bill No. : {this.state.purchase.bill_no}</h4>
          </Col>
        </Row>

        <Descriptions title="" bordered>
          <Descriptions.Item label="Product Name">
            {this.state.purchase.product_name}
          </Descriptions.Item>
          <Descriptions.Item label="Quantity">
            {this.state.purchase.quantity}
          </Descriptions.Item>
          <Descriptions.Item label="Rate">
            {this.state.purchase.rate}
          </Descriptions.Item>
          <Descriptions.Item label="Discount">
            {this.state.purchase.discount}{" "}
          </Descriptions.Item>
          <Descriptions.Item label="excise:">
            {this.state.purchase.excise}
          </Descriptions.Item>
          <Descriptions.Item label="Amount">
            {" "}
            {this.state.purchase.amount}{" "}
          </Descriptions.Item>
          <Descriptions.Item label="Unit">
            {this.state.purchase.unit}{" "}
          </Descriptions.Item>
          <Descriptions.Item label="Additional units">
            {this.renderShowAdditionalInformations()}
          </Descriptions.Item>
          <Descriptions.Item label="Category">
            {this.state.purchase.category_name}
          </Descriptions.Item>
          <Descriptions.Item label="Brand">
            {this.state.purchase.brand}{" "}
          </Descriptions.Item>
          <Descriptions.Item label="Description">
            {this.state.purchase.description}
          </Descriptions.Item>
          <Descriptions.Item label="Remarks">
            {this.state.purchase.remarks}
          </Descriptions.Item>
          <Descriptions.Item label="Site Name">
            {this.state.purchase.site_name}
          </Descriptions.Item>
          <Descriptions.Item label="Vehicle Number">
            {this.state.purchase.vehicle_no}
          </Descriptions.Item>
          <Descriptions.Item label="Vendor Name">
            {this.state.purchase.vendor_name}
          </Descriptions.Item>
          <Descriptions.Item label="Chalan Number">
            {this.state.purchase.chalan_no}
          </Descriptions.Item>
        </Descriptions>
      </Modal>
    )
  }
}

export default DispatchShow;
