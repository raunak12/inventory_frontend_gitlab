import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CustomLayout from '../Layouts/CustomLayout'
import Table from './table'
import { dispatchActions } from '../../actions/DispatchActions';
import LoadingSpinner from '../../helpers/LoadingSpinner';

class DispatchLists extends Component {
  constructor() {
    super()
    this.state = { dispatches: [], isLoading: true }
  }

  componentDidMount() {
    dispatchActions.fetchDispatchedItem().then(response => {
      this.setState({ dispatches: response.data, isLoading: false })
    }).catch(error => console.log('error', error));
  }

  showTable() {
    const { dispatches } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <Table table_data={dispatches} />
      )
    }
  }

  render() {
    const { dispatches } = this.state
    if (dispatches === null) return null
    return (
      <CustomLayout sidebarSelectedKey='dispatches'>
        <h1>Dispatches</h1>
        <Link to="/dispatches/new" className="btn btn-outline-primary mar-pad float-right">
          Create dispatches
        </Link>
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default DispatchLists


