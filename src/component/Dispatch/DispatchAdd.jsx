import React, { Component } from "react";
import { Form, Input, Select, Button, DatePicker, Icon, Row, Col } from "antd";
import CustomLayout from "../Layouts/CustomLayout";
import moment from "moment";
import { success, error } from "../../helper/notifications";
import { siteActions } from "../../actions/SiteActions";
import { dispatchActions } from "../../actions/DispatchActions";
import { categoryActions } from "../../actions/CategoryActions";
import { isAdmin } from "../../helpers/userPolicy";
import Cookie from "js.cookie";

const { Option } = Select;
let id = 0;

class DispatchAdd extends Component {
  constructor() {
    super();
    this.state = {
      dispatchForms: [0],
      categories: [],
      sites: [],
      products: [],
      category: {}
    };
  }

  componentDidMount() {
    siteActions
      .fetchSites()
      .then(response => {
        this.setState({ sites: response.data });
      })
      .catch(error => console.log("error", error));

    categoryActions
      .fetchCategories()
      .then(response => {
        this.setState({ categories: response.data });
      })
      .catch(error => console.log("error", error));
  }

  handleChange(value, k) {
    this.props.form.setFieldsValue({
      [`dispatched_items_attributes[${k}][shipment_attributes][product_id]`]: ""
    });
    categoryActions
      .fetchProductsCategory(value)
      .then(response => {
        this.setState({ products: response.data });
      })
      .catch(error => console.log("error", error));

    categoryActions
      .showCategory(value)
      .then(response => {
        this.setState({ category: response.data });
        this.props.form.setFieldsValue({
          [`dispatched_items_attributes[${k}][shipment_attributes][unit]`]: response
            .data.unit
        });
      })
      .catch(error => console.log("error", error));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        dispatchActions
          .createDistatch(values)
          .then(response => {
            if (response.status === 201) {
              success("Dispatch has been sucessfully created.");
              this.props.history.push(`/dispatches`);
            } else {
              error("Something went wrong. Please try again.");
            }
          })
          .catch(error => console.log("error", error));
      }
    });
  }

  handleCancel() {
    this.props.history.push("/dispatches");
  }

  addForm() {
    const { dispatchForms } = this.state;
    const newKey =
      dispatchForms.length > 0
        ? dispatchForms[dispatchForms.length - 1] + 1
        : 0;
    this.setState({ dispatchForms: [...dispatchForms, newKey] });
  }

  removeForm(key) {
    const { dispatchForms } = this.state;
    this.setState({
      dispatchForms: dispatchForms.filter(k => k !== key)
    });
  }

  remove(k) {
    const { form } = this.props;
    const keys = form.getFieldValue("keys");
    if (keys.length === 1) {
      return;
    }
    form.setFieldsValue({
      keys: keys.filter(key => key !== k)
    });
  }

  add() {
    const { form } = this.props;
    const keys = form.getFieldValue("keys");
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
      keys: nextKeys
    });
  }

  render() {
    const { form } = this.props;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 }
      }
    };

    getFieldDecorator("keys", { initialValue: [] });
    const keys = getFieldValue("keys");
    const formItems = keys.map((k, index) => (
      <Form.Item
        {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        label={index === 0 ? "Chalan" : ""}
        required={false}
        key={k}
      >
        {getFieldDecorator(`chalan_no[${k}]`, {
          validateTrigger: ["onChange", "onBlur"],
          rules: [
            {
              whitespace: true
            }
          ]
        })(
          <Input
            placeholder="Add Chalan Number"
            style={{ width: "90%", marginRight: 8 }}
          />
        )}
        {keys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
          />
        ) : null}
      </Form.Item>
    ));

    return (
      <CustomLayout>
        <br />
        <Col>
          <h3>ADD NEW DISPATCH</h3>
        </Col>
        <div>
          <Form onSubmit={this.handleSubmit.bind(this)}>
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label="Dispatch Date">
                    {form.getFieldDecorator("date", {
                      rules: [{ required: true, message: "Please Enter Date" }],
                      initialValue: moment()
                    })(<DatePicker />)}
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="Bill_No">
                    {form.getFieldDecorator("bill_no", {
                      rules: [
                        { required: true, message: "Please Enter Bill Number" }
                      ]
                    })(<Input placeholder="Bill Number" name="bill_no" />)}
                  </Form.Item>
                </div>
              </Col>
            </Row>
            {this.state.dispatchForms.map((k, idx) => {
              return (
                <div key={k}>
                  <fieldset key={k}>
                    <Row>
                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label="Category:">
                            {form.getFieldDecorator(
                              `dispatched_items_attributes[${k}][shipment_attributes][category_id]`,
                              {
                                rules: [
                                  {
                                    required: true,
                                    message: "Please Select the category"
                                  }
                                ],
                                onChange: value => this.handleChange(value, k)
                              }
                            )(
                              <Select
                                showSearch
                                style={{ width: 200 }}
                                placeholder="Select a Category"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                  option.props.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                                }
                              >
                                {this.state.categories.map(category => {
                                  return (
                                    <Option
                                      value={category.id}
                                      key={category.id}
                                    >
                                      {category.name}
                                    </Option>
                                  );
                                })}
                              </Select>
                            )}
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label="Product">
                            {form.getFieldDecorator(
                              `dispatched_items_attributes[${k}][shipment_attributes][product_id]`,
                              {
                                rules: [
                                  {
                                    required: true,
                                    message: "Please Enter Product Name"
                                  }
                                ]
                              }
                            )(
                              <Select
                                showSearch
                                style={{ width: 200 }}
                                placeholder="Select Product"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                  option.props.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                                }
                              >
                                {this.state.products.map(product => {
                                  return (
                                    <Option value={product.id} key={product.id}>
                                      {product.name}
                                    </Option>
                                  );
                                })}
                              </Select>
                            )}
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label="Description">
                            {form.getFieldDecorator(
                              `dispatched_items_attributes[${k}][shipment_attributes][description]`,
                              {}
                            )(
                              <Input
                                placeholder="Description"
                                name="description"
                              />
                            )}
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label="Brand">
                          {form.getFieldDecorator(
                            `dispatched_items_attributes[${k}][shipment_attributes][brand]`,
                            {}
                          )(<Input placeholder="Brand Name" name="brand" />)}
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label="Unit">
                          {form.getFieldDecorator(
                            `dispatched_items_attributes[${k}][shipment_attributes][unit]`,
                            {
                              initialValue: this.state.category.unit
                            }
                          )(
                            <Input
                              placeholder="Unit"
                              name="unit"
                              readOnly="readOnly"
                            />
                          )}
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label="Quantity">
                          {form.getFieldDecorator(
                            `dispatched_items_attributes[${k}][shipment_attributes][quantity]`,
                            {
                              rules: [
                                {
                                  required: true,
                                  message: "Please Enter Quantity"
                                }
                              ]
                            }
                          )(<Input placeholder="Quantity" name="quantity" />)}
                        </Form.Item>
                      </Col>

                      <Col span={4} offset={1}>
                        <Form.Item label="Remarks">
                          {form.getFieldDecorator(
                            `dispatched_items_attributes[${k}][shipment_attributes][remarks]`,
                            {}
                          )(<Input placeholder="Remarks" name="remarks" />)}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={4} offset={1}>
                        <Button onClick={e => this.removeForm(k)} type="danger">
                          Remove Form
                        </Button>
                      </Col>
                    </Row>
                  </fieldset>
                  <br />
                </div>
              );
            })}
            <br />
            <Row>
              <Col span={23}>
                <Button
                  onClick={e => this.addForm(e)}
                  type="primary"
                  style={{ float: "right" }}
                >
                  Add New Product
                </Button>
              </Col>
            </Row>
            <br />
            <Row>
              <Col span={4}>
                <div>
                  <Form.Item label="Vehicle Number">
                    {form.getFieldDecorator("vehicle_no", {})(
                      <Input placeholder="Vehicle Number" name="vehicle_no" />
                    )}
                  </Form.Item>
                </div>
              </Col>
              <Col span={6} offset={1}>
                <Form.Item {...formItemLayoutWithOutLabel}>
                  <Button
                    type="dashed"
                    onClick={this.add.bind(this)}
                    style={{ width: "90%" }}
                  >
                    <Icon type="plus" />
                    Add Chalan Number
                  </Button>
                </Form.Item>
                {formItems}
              </Col>

              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="Source">
                    {form.getFieldDecorator(`source_id`, {
                      rules: [
                        { required: true, message: "Please Select the Source" }
                      ]
                    })(
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select Source Site"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {isAdmin()
                          ? this.state.sites.map(site => {
                              return (
                                <Option value={site.id} key={site.id}>
                                  {site.name}
                                </Option>
                              );
                            })
                          : this.state.sites.map(site => {
                              if (site.id === Cookie.get("site_id")) {
                                return (
                                  <Option value={site.id} key={site.id}>
                                    {site.name}
                                  </Option>
                                );
                              } else {
                                return null;
                              }
                            })}
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </Col>
              <Col span={4} offset={1}>
                <div>
                  <Form.Item label="Destination">
                    {form.getFieldDecorator(`destination_id`, {
                      rules: [
                        {
                          required: true,
                          message: "Please Select the Destiantion"
                        }
                      ]
                    })(
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select Destination Site"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.state.sites.map(site => {
                          return (
                            <Option value={site.id} key={site.id}>
                              {site.name}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="btn-group">
                  <Button htmlType="submit" className="btn btn-dark">
                    Create Dispatch
                  </Button>
                  &nbsp;
                  <Button
                    type="button"
                    onClick={this.handleCancel.bind(this)}
                    className="btn btn-secondary"
                  >
                    Cancel
                  </Button>
                </div>
              </Col>
            </Row>
            <br />
          </Form>
        </div>
      </CustomLayout>
    );
  }
}

export default Form.create({})(DispatchAdd);
