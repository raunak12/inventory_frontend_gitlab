import React, { Component } from "react";
import { Table, Icon, Divider, Modal, Button, Row, Col, Descriptions } from "antd";
import { Link } from "react-router-dom";
import { success, error } from "../../helper/notifications";
import { dispatchActions } from "../../actions/DispatchActions";
import { shipmentActions } from "../../actions/ShipmentActions";
import { sortText, sortDate } from "../../helpers/sort";
import { getColumnSearchProps } from "../../helpers/tableSearch";

const { confirm } = Modal;

class DispatchTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      dispatches: props.table_data,
      visible: false,
      dispatch: []
    };
  }

  handleDelete(shipment_id) {
    shipmentActions.deleteShipment(shipment_id).then(response => {
      if (response.status === 204) {
        dispatchActions
          .fetchDispatchedItem()
          .then(response => {
            this.setState({ dispatches: response.data });
          })
          .catch(error => console.log("error", error));
        success("Dispatched product was successfully deleted!");
      } else {
        error('Unable to delete a dispatch!')
      }
    });
  }
  showDeleteConfirm(shipment_id) {
    confirm({
      title: "Are you sure to delete this Dispatch?",
      okText: "Yes",
      okType: "danger",
      className: "show-modal-delete",
      cancelText: "No",
      onOk: () => this.handleDelete(shipment_id),
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }

  showModal = value => {
    dispatchActions.fetchDispatched(value).then( response => {
      this.setState({
        dispatch: response.data,
        visible: true
      })
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  render() {
    const columns = [
      {
        title: "Date",
        dataIndex: "shipment_date",
        key: "shipment_date",
        sorter: (a, b) => sortDate(a.shipment_date, b.shipment_date),
        ...getColumnSearchProps("shipment_date", this)
      },
      {
        title: "Bill Number",
        dataIndex: "bill_no",
        key: "bill_no"
      },
      {
        title: "Product Name",
        dataIndex: "product_name",
        key: "product_name",
        sorter: (a, b) => sortText(a.product_name, b.product_name),
        ...getColumnSearchProps("product_name", this)
      },
      {
        title: 'Category',
        dataIndex: 'category',
        key: 'category',
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description"
      },
      {
        title: "Quantity",
        dataIndex: "quantity",
        key: "quantity"
      },
      {
        title: "Unit",
        dataIndex: "unit",
        key: "unit"
      },
      {
        title: "Source",
        dataIndex: "source",
        key: "source",
        sorter: (a, b) => sortText(a.source, b.source),
        ...getColumnSearchProps("source", this)
      },
      {
        title: "Destination",
        dataIndex: "destination",
        key: "destination",
        sorter: (a, b) => sortText(a.destination, b.destination),
        ...getColumnSearchProps("destination", this)
      },
      {
        title: "",
        width: "8%",
        key: "action",
        render: (text, record) => {
          return (
            <span>
              <Link to={`dispatches/${record.dispatch_id}/edit`}>
                <Icon title="Edit" type="edit" />
              </Link>
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon title="View" onClick={() => this.showModal(`${record.id}`)} type="snippets" />
              </span>
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon
                  type="delete"
                  title="Delete"
                  onClick={() => this.showDeleteConfirm(record.shipment_id)}
                />
              </span>
            </span>
          );
        }
      }
    ];
    const dispatches = this.state.dispatches;
    const view_table =
      dispatches && dispatches.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={dispatches} size="middle" rowKey={column => column.id} />
        </div>
      ) : (
        "No Data Found"
      );
    return (
      <div>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>
          ]}
        >
          <Row>
            <Col span={24} offset={6}>
              <h3>DISPATCHED PRODUCT DETAIL</h3>
            </Col>
          </Row>

          <Row>
            <Col span={10}>
              <h4>Date: {this.state.dispatch.shipment_date}</h4>
            </Col>
            <Col span={6} />
            <Col span={8}>
              <h4> Bill No. : {this.state.dispatch.bill_no}</h4>
            </Col>
          </Row>

          <Descriptions title="" bordered>
            <Descriptions.Item label="Product Name">
              {this.state.dispatch.product_name}
            </Descriptions.Item>
            <Descriptions.Item label="Quantity">
              {this.state.dispatch.quantity}
            </Descriptions.Item>
            <Descriptions.Item label="Unit">
              {this.state.dispatch.unit}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Category">
              {this.state.dispatch.category}
            </Descriptions.Item>
            <Descriptions.Item label="Brand">
              {this.state.dispatch.brand}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="Chalan Number">
              {this.state.dispatch.chalan_no}
            </Descriptions.Item>
            <Descriptions.Item label="Source Name">
              {this.state.dispatch.source}
            </Descriptions.Item>
            <Descriptions.Item label="Destination Name">
              {this.state.dispatch.destination}
            </Descriptions.Item>
            <Descriptions.Item label="Vehicle Number">
              {this.state.dispatch.vehicle_no}
            </Descriptions.Item>
            <Descriptions.Item label="Remarks">
              {this.state.dispatch.remarks}
            </Descriptions.Item>
            <Descriptions.Item label="Description">
              {this.state.dispatch.description}
            </Descriptions.Item>
          </Descriptions>
        </Modal>
        {view_table}
      </div>
    );
  }
}
export default DispatchTable;
