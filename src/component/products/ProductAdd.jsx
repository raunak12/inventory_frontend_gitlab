import React, { Component } from 'react';
import { Form, Input, Select } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { categoryActions } from '../../actions/CategoryActions';
import { productActions } from '../../actions/ProductActions';

const { Option } = Select;

class ProductAdd extends Component {
  constructor() {
    super();
    this.state = {
      categories: []
    };
  }

  componentDidMount() {
    categoryActions.fetchCategories().then(response => {
      this.setState({categories: response.data})
    }).catch(error => console.log('error', error))
    fetch(`/categories.json`)
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        productActions.createProduct(values).then( response => {
          if(response.status === 201) {
            success('Product successfully created.');
            this.props.history.push(`/products`);
          }
          else{
            error('Unable to create product.')
          }
        }).catch(error => console.log('error', error));
      }
    })
  }

  handleCancel() {
    this.props.history.push("/products");
  }


  render() {
    const { form } = this.props
    return (
      <CustomLayout>
        <h1>Create Product</h1>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <Form.Item label='Product Name'className='short-text-field'>
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input product name!' }],
              })(
                <Input
                  placeholder="Product Name"
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Category Name' className='short-text-field'>
            {
              form.getFieldDecorator('category_id', {
                rules: [{ required: true, message: 'Please select a category!' }],
              })(
                <Select placeholder='Select Cateogry'>
                  {
                    this.state.categories.map(category => {
                      return(
                        <Option value={category.id} key={category.id}>
                          {category.name}
                        </Option>
                      )    
                    })
                  }
                </Select>
              )
            }
          </Form.Item>

          <div className="btn-group">
            <button type="submit" className="btn btn-dark">Create Product</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(ProductAdd);