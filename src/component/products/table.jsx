import React, { Component } from "react";
import {
  Table,
  Icon,
  Divider,
  Modal,
  Button,
  Row,
  Col,
  Descriptions
} from "antd";
import { Link } from "react-router-dom";
import { success, error } from "../../helper/notifications";
import { productActions } from "../../actions/ProductActions";
import { sortText } from "../../helpers/sort";
import { getColumnSearchProps } from "../../helpers/tableSearch";
import { isAdmin } from '../../helpers/userPolicy'

const { confirm } = Modal;

class ProductTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      products: props.table_data,
      product: {}
    };
  }

  handleDelete(productId) {
    productActions.deleteProduct(productId).then(response => {
      if (response.status === 204) {
        productActions
          .fetchproducts()
          .then(response => {
            this.setState({ products: response.data });
          })
          .catch(error => console.log("error", error));
        success("Product was successfully deleted!");
      } else {
        error("Unable to delete Product!");
      }
    });
  }
  showDeleteConfirm(productId) {
    confirm({
      className: "show-modal-delete",
      title: "Are you sure to delete this Product?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk: () => this.handleDelete(productId),
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }

 
  showModal = value => {
    productActions.showProduct(value).then(response => {
      this.setState({
        product: response.data,
        visible: true
      });
    });
    
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  render() {
    const columns = [
      {
        title: "Product Name",
        dataIndex: "name",
        key: "name",
        sorter: (a, b) => sortText(a.name, b.name),
        ...getColumnSearchProps("name", this)
      },
      {
        title: "Category",
        dataIndex: "category_name",
        key: "category_name",
        sorter: (a, b) => sortText(a.category_name, b.category_name),
        ...getColumnSearchProps("category_name", this)
      },
      {
        title: "Unit",
        dataIndex: "unit",
        key: "unit"
      },
      {
        title: "",
        width: "8%",
        key: "action",
        render: (text, record) => {
          return (
            <span>
              {
                isAdmin() ? 
                  <Link to={`products/${record.id}/edit`}>
                    <Icon title='Edit' type="edit" />
                  </Link>
                :''
              }
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon 
                  title="View"
                  type="snippets" 
                  onClick={()=> this.showModal(record.id)}/>
              </span>
              <Divider type="vertical" />
              {
                isAdmin() ? 
                  <span style={{ color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent' }}>
                    <Icon
                      type="delete"
                      title="Delete"
                      onClick={() => this.showDeleteConfirm(record.id)}
                    />
                  </span> : ''
              }
           
            </span>
          );
        }
      }
    ];
    const products = this.state.products;
    const view_table =
      products && products.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={products} size="middle" rowKey={column=> column.id} />
        </div>
      ) : (
        "No Data Found"
      );
    return (
      <div>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>
          ]}
        >
          <Row>
            <Col span={24} offset={6}>
              <h3> PRODUCT DETAIL</h3>
            </Col>
          </Row>

          <Row>
            <Col span={10}>
            </Col>
            <Col span={6} />
            <Col span={8} />
          </Row>
          <Descriptions title="" bordered>
            <Descriptions.Item label="Product Name">
              {this.state.product.name}
            </Descriptions.Item>
            <Descriptions.Item label="Category">
              {this.state.product.category_name}
            </Descriptions.Item>
            <Descriptions.Item label="Unit">
              {this.state.product.unit}{" "}
            </Descriptions.Item>
          </Descriptions>
        </Modal>
        {view_table}
      </div>
    );
  }
}
export default ProductTable;
