import React, { Component } from 'react';
import { Form, Input, Select } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { productActions } from '../../actions/ProductActions';
import { categoryActions } from '../../actions/CategoryActions';
const { Option } = Select;

class ProductEdit extends Component {
  constructor() {
    super();
    this.state = {
      product: {},
      categories: []
    };
  }

  componentDidMount() {
    productActions.showProduct(this.props.match.params.id).then(response => {
      this.setState({ product: response.data});
    }).catch(error => console.log('error', error))

    categoryActions.fetchCategories().then(response => {
      this.setState({categories: response.data})
    }).catch(error => console.log('error', error))
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if(!err) {
        productActions.updateProduct(this.state.product.id, values).then(response => {
          if(response.status === 200){
            this.props.history.push(`/products`);
            success('Product was sucessfully updated.')
          }else{
            this.props.history.push(`/products/${response.data.id}/show`);
            error('Unable to update the product')
          }
        })
      }
    })
  }

  handleCancel() {
    this.props.history.push(`/products/${this.state.product.id}/show`);
  }

  render() {
    const { form } =  this.props
    return (
      <CustomLayout>
        <h1>Edit Product</h1>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <Form.Item label= 'Product Name' >
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input product name!'}],
                initialValue: this.state.product.name
              })(
                <Input
                  placeholder='Product Name'
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Category Name'>
            {
              form.getFieldDecorator('category_id', {
                rules: [{ required: true, message: 'Please select a category!' }],
                initialValue: this.state.product.category_id
              })(
                <Select placeholder='Select Cateogry'>
                  {
                    this.state.categories.map(category => {
                      return(
                        <Option value={category.id} key={category.id}>
                          {category.name}
                        </Option>
                      )    
                    })
                  }
                </Select>
              )
            }
          </Form.Item>

          <div className="btn-group">
            <button type="submit" className="btn btn-dark">Update Product</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(ProductEdit);
