import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { Modal } from 'antd';
import { productActions } from '../../actions/ProductActions';
import { isAdmin } from '../../helpers/userPolicy'

const { confirm } = Modal;

class ProductShow extends Component {
  constructor() {
    super();
    this.state = { product: {} } 
  }


  componentDidMount() {
    productActions.showProduct(this.props.match.params.id).then(response => {
      this.setState({product: response.data})
    }).catch(error => console.log('error', error));
  }

  handleDelete() {
    productActions.deleteProduct(this.state.product.id).then(response => {
      if (response.status === 200){
        success('Prodct was sucessfully deleted!')
        this.props.history.push(`/products`)
      }else{
        error('Unable to delete a product!')
      }
    })
  }

  showDeleteConfirm() {
    confirm({
      title: 'Are you sure to delete this Product?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleDelete.bind(this),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  render() {
    return (
      <CustomLayout>
          <table className="table table-sm list-table">
          <tr>
            <td><h5>Product Id:</h5> </td>
            <td><h5>{this.state.product.id}</h5></td>
            <td><h5>Product Name:</h5></td>
            <td><h5>{this.state.product.name}</h5></td>
          </tr>
          <tr>
            <td><h5>Category Name:</h5> </td>
            <td><h5>{this.state.product.category_name}</h5></td>
            <td><h5>Category Unit:</h5></td>
            <td><h5>{this.state.product.unit}</h5></td>
          </tr>
          
          <tr>
            <td>
            { isAdmin() ? 
              <p>
              <Link to={`/products/${this.state.product.id}/edit`} className="btn btn-primary">Edit</Link> 
              <button onClick={this.showDeleteConfirm.bind(this)} className="btn btn-danger">Delete</button> 
              </p> : ''
            }
              <Link to="/products" className="btn btn-primary">Back</Link>
            </td>
            <td></td>
            <td></td>
            <td></td>
          </tr> 
        </table>
        <p>
          
        </p>
        <hr/>
      </CustomLayout>
    )
  }
}

export default ProductShow;
