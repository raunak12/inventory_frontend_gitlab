import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CustomLayout from '../Layouts/CustomLayout'
import Table from './table'
import { productActions } from '../../actions/ProductActions';
import {isAdmin} from '../../helpers/userPolicy'
import LoadingSpinner from '../../helpers/LoadingSpinner';

class ProductList extends Component {
  constructor() {
    super()
    this.state = { products: [], isLoading: true }
  }

  componentDidMount() {
    productActions.fetchproducts().then(response => {
      this.setState({products: response.data, isLoading: false })
    }).catch(error => console.log('error', error))
  }

  showTable() {
    const { products } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <Table table_data={products} />
      )
    }
  }

  render() {
    const { products } = this.state
    if (products === null) return null
    return (
      <CustomLayout sidebarSelectedKey='products'>
        <h1>Products</h1>
        {
          isAdmin() ? 
          <Link to="/products/new" className="btn btn-outline-primary mar-pad float-right">
          Create Product
        </Link>
        : ''
        }
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default ProductList
