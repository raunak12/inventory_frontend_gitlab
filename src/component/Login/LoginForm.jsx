import React, { Component } from "react";
import { Form, Icon, Input, Button } from "antd";
import "antd/dist/antd.css";
import { userService } from "../../services/user.service";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      submitted: false
    };
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { email, password } = this.state;

    this.setState({
      submitted: true
    });
    if (email && password) {
      userService.login(email, password, this.props.history);
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <article  class="mw6 center bg-white br3 pa4 pa4-ns mv6 ba b--black-10" style={{height: '23rem'}}>
        <h4 style={{ textAlign: "center" }}>Login</h4>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item>
            {getFieldDecorator("email", {
              rules: [
                {
                  required: true,
                  message: "Please input your email!"
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Email"
                name="email"
                onChange={this.handleChange}
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("password", {
              rules: [
                {
                  required: true,
                  message: "Please input your Password!"
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="Password"
                name="password"
                onChange={this.handleChange}
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("remember", {
              valuePropName: "checked",
              initialValue: true
            })}
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              style={{ width: "-webkit-fill-available" }}
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
      </article>
    );
  }
}

export default Form.create()(LoginForm);
