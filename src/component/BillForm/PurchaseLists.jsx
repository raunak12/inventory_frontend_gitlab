import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CustomLayout from '../Layouts/CustomLayout'
import PurchaseTable from './table'
import { billActions } from '../../actions/BillActions';
import LoadingSpinner from '../../helpers/LoadingSpinner';

class PurchaseLists extends Component {
  constructor() {
    super()
    this.state = { bills: [], isLoading: true }
  }

  componentDidMount() {
    billActions.fetchBillItems().then(response => {
      this.setState({
        bills: response.data,
        isLoading: false
      })
    }).catch(error => console.log('error', error))
  }

  showTable() {
    const { bills } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <PurchaseTable table_data={bills} /> 
      )
    }
  }
  render() {
    const { bills } = this.state
    if (bills === null) return null
    return (
      <CustomLayout sidebarSelectedKey='purchases'>
        <h1>Purchases</h1>
        <Link to="/bills/new" className="btn btn-outline-primary mar-pad float-right">
          Create Bill
        </Link>
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default PurchaseLists
