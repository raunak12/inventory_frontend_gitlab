import React, { Component } from 'react';
import { Table, Icon, Divider, Modal, Button, Row, Col, Descriptions } from 'antd';
import { Link } from 'react-router-dom';
import { success, error } from '../../helper/notifications';
import { billActions } from '../../actions/BillActions';
import { shipmentActions } from '../../actions/ShipmentActions';
import { sortText, sortDate } from '../../helpers/sort';
import { getColumnSearchProps } from '../../helpers/tableSearch';

const { confirm } = Modal;
class PurchaseTable extends Component {
 
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      bills: props.table_data,
      visible: false,
      purchase: {}
    };
  }
 
  handleDelete(shipment_id) {
    shipmentActions.deleteShipment(shipment_id).then(response => {
      if (response.status === 204) {
        billActions.fetchBillItems().then(response => {
          this.setState({bills: response.data})
        }).catch(error => console.log('error', error))
        success('Purchased product was successfully deleted!')
      } else {
        error('Unable to delete a purchase!')
      }
    })
  }

  showDeleteConfirm(shipment_id) {
    confirm({
      className: "show-modal-delete",
      title: 'Are you sure to delete this Purchase?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () => this.handleDelete(shipment_id),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  showModal = value => {
    billActions.showPurchaseProduct(value).then(response => {
      this.setState({ 
        purchase: response.data ,
        visible: true
      })
    });

  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  renderShowAdditionalInformations() {
    const { additional_units } = this.state.purchase;
    if (additional_units) {
      return Object.keys(additional_units).map(additionalInformationKey=> {
        const label = additionalInformationKey;
        const value = additional_units[additionalInformationKey];
        return <div key={additionalInformationKey}>{`${label.toUpperCase()}: ${value}`}</div>;
      });
    } else {
      return "";
    }
  }

    render() {
    const columns = [
      {
        title: 'Date',
        dataIndex: 'shipment_date',
        key: 'shipment_date',
        sorter: (a, b) => sortDate(a.shipment_date, b.shipment_date),
        ...getColumnSearchProps('shipment_date', this),
      },
      {
        title: 'Bill Number',
        dataIndex: 'bill_no',
        key: 'bill_no'
      },
      {
        title: 'Product Name',
        dataIndex: 'product_name',
        key: 'product_name',
        sorter: (a, b) => sortText(a.product_name, b.product_name),
        ...getColumnSearchProps('product_name', this),
      },
      {
        title: 'Category',
        dataIndex: 'category_name',
        key: 'category_name',
      },
      {
        title: 'Description',
        dataIndex: 'description',
        key: 'description'
      },
      {
        title: 'Quantity',
        dataIndex: 'quantity',
        key: 'quantity'
      },
      {
        title: 'Unit',
        dataIndex: 'unit',
        key: 'unit'
      },
      {
        title: 'Rate',
        dataIndex: 'rate',
        key: 'rate'
      },
      {
        title: 'Total Amount',
        dataIndex: 'amount',
        key: 'amount'
      },
      {
        title: 'Vendor Name',
        dataIndex: 'vendor_name',
        key: 'vendor_name',
        sorter: (a, b) => sortText(a.vendor_name, b.vendor_name),
        ...getColumnSearchProps('vendor_name', this),

      },
      {
        title: 'Site Name',
        dataIndex: 'site_name',
        key: 'site_name',
        sorter: (a, b) => sortText(a.site_name, b.site_name),
        ...getColumnSearchProps('site_name', this),

      },
      {
        title: '',
        width: '8%', 
        key: 'action',
        render: (text, record) => {
          return (
            <span>
              <Link to ={`bills/${record.bill_id}/edit`} ><Icon type="edit" title="Edit"/></Link>
              <Divider type= "vertical"/>
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon title="View" onClick={() => this.showModal(`${record.id}`)} type="snippets"  />
              </span>
              <Divider type= "vertical"/>
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon  title="Delete" type="delete" onClick= {() => this.showDeleteConfirm(`${record.shipment_id}`)}/>
              </span>
            </span>
          )
        },
      }
    ]
    const bills = this.state.bills
    const view_table =
      bills && bills.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={bills} size="middle" rowKey={column => column.id} />
        </div>
      ) : (
        'No Data Found'
      )
    return (
      <div  >
      <Modal
        visible={this.state.visible}
        onCancel={this.handleCancel}
        footer={[
          <Button key="back" onClick={this.handleCancel}>
            Back
          </Button>
        ]}
      >
        <Row>
          <Col span={24} offset={6}  >
            <h3>PURCHASED PRODUCT DETAIL</h3>
          </Col>
      
        </Row>

        <Row>
          <Col span={10}>
            <h4>Date: {this.state.purchase.bill_date}</h4>
          </Col>
          <Col span={6} />
          <Col span={8}>
            <h4> Bill No. : {this.state.purchase.bill_no}</h4>
          </Col>
        </Row>

        <Descriptions title="" bordered>
          <Descriptions.Item label="Product Name">
            {this.state.purchase.product_name}
          </Descriptions.Item>
          <Descriptions.Item label="Quantity">
            {this.state.purchase.quantity}
          </Descriptions.Item>
          <Descriptions.Item label="Rate">
            {this.state.purchase.rate}
          </Descriptions.Item>
          <Descriptions.Item label="Discount">
            {this.state.purchase.discount}{" "}
          </Descriptions.Item>
          <Descriptions.Item label="excise:">
            {this.state.purchase.excise}
          </Descriptions.Item>
          <Descriptions.Item label="Amount">
            {" "}
            {this.state.purchase.amount}{" "}
          </Descriptions.Item>
          <Descriptions.Item label="Unit">
            {this.state.purchase.unit}{" "}
          </Descriptions.Item>
          <Descriptions.Item label="Additional units">
            {this.renderShowAdditionalInformations()}
          </Descriptions.Item>
          <Descriptions.Item label="Category">
            {this.state.purchase.category_name}
          </Descriptions.Item>
          <Descriptions.Item label="Brand">
            {this.state.purchase.brand}{" "}
          </Descriptions.Item>
          <Descriptions.Item label="Chalan Number">
            {this.state.purchase.chalan_no}
          </Descriptions.Item>
          <Descriptions.Item label="Vehicle Number">
            {this.state.purchase.vehicle_no}
          </Descriptions.Item>
          <Descriptions.Item label="Site Name">
            {this.state.purchase.site_name}
          </Descriptions.Item>
          <Descriptions.Item label="Vendor Name">
            {this.state.purchase.vendor_name}
          </Descriptions.Item>
          <Descriptions.Item label="Remarks">
            {this.state.purchase.remarks}
          </Descriptions.Item>
          <Descriptions.Item label="Description">
            {this.state.purchase.description}
          </Descriptions.Item>
        </Descriptions>
      </Modal>
    {view_table}</div>
    )
  }
}
export default PurchaseTable
