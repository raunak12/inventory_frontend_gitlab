import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { Modal } from 'antd';
import { billActions } from '../../actions/BillActions';
import { shipmentActions } from '../../actions/ShipmentActions';

const { confirm } = Modal;

class PurchaseShow extends Component {
  constructor() {
    super();
    this.state = {
    }
  }


  componentDidMount() {
    billActions.showPurchaseProduct(this.props.match.params.id).then(response => {
      this.setState({ purchase: response.data })
    })
  }

  handleDelete() {
    shipmentActions.deleteShipment(this.state.purchase.shipment_id).then(response => {
      if (response.status === 204) {
        success('Purchased product was successfully deleted!')
        this.props.history.push("/bills")
      } else {
        error('Unable to delete a purchase!')
      }
    })
  }

  renderShowAdditionalInformations() {
    const { additional_units } = this.state.purchase
    return (
      Object.keys(additional_units).map(additionalInformationKey => {
        const label = additionalInformationKey;
        const value = additional_units[additionalInformationKey];
        return (
          <div>
            {
              `${label.toUpperCase()}: ${value}`
            }
          </div>
        )
      })

    )
  }

  showDeleteConfirm() {
    confirm({
      title: 'Are you sure to delete this Purchase?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleDelete.bind(this),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  renderPurchase() {
    if (this.state.purchase) {
      return (
        <div>
          <table className="table table-sm ">
            <tr>
              <td><h5>Purchase Date:</h5> </td>
              <td><h5>{this.state.purchase.shipment_date}</h5></td>
            </tr>
          </table>
          <table className="table table-sm view-table">
            <tr>
              <td><h5>Purchase Id:</h5> </td>
              <td><h5>{this.state.purchase.id}</h5></td>
              <td><h5>Purchased Product:</h5></td>
              <td><h5>{this.state.purchase.product_name}</h5></td>
            </tr>

            <tr>
              <td><h5>Product's Category:</h5></td>
              <td><h5>{this.state.purchase.category_name}</h5></td>
              <td><h5>Product's Description:</h5></td>
              <td><h5>{this.state.purchase.description}</h5></td>
            </tr>
            <tr>
              <td><h5>Product's Brand:</h5></td>
              <td><h5>{this.state.purchase.brand}</h5></td>
              <td></td>
              <td></td>
            </tr>
          </table>
          <table className="table table-sm view-table">
            <tr>
              <td><h5>Total Quantity:</h5></td>
              <td><h5>{this.state.purchase.quantity}</h5></td>
              <td><h5>Additional Informations:</h5></td>
              <td><h5>{this.renderShowAdditionalInformations()}</h5></td>
            </tr>
            <tr>
              <td><h5>Rate:</h5></td>
              <td><h5>{this.state.purchase.rate}</h5></td>
              <td><h5>Discount:</h5></td>
              <td><h5> {this.state.purchase.discount}</h5></td>
            </tr>
            <tr>
              <td><h5>Total Amount:</h5></td>
              <td><h5>{this.state.purchase.total_amount}</h5></td>
              <td><h5>Bill Number:</h5></td>
              <td><h5>{this.state.purchase.bill_no}</h5></td>
            </tr>
            <tr>
              <td><h5>Vehicle Number:</h5></td>
              <td><h5>{this.state.purchase.vehicle_no}</h5></td>
              <td><h5>Chalan Number: </h5></td>
              <td><h5>{this.state.purchase.chalan_no}</h5></td>
            </tr>
          </table>
          <table className="table  table-sm view-table">

            <tr>
              <td><h5>Vendor Name:</h5></td>
              <td><h5>{this.state.purchase.vendor_name}</h5></td>
              <td><h5>Site Name:</h5></td>
              <td><h5>{this.state.purchase.site_name}</h5></td>
            </tr>
            <tr>
              <td><h5>Remarks:</h5></td>
              <td><h5>{this.state.purchase.remarks}</h5></td>
            </tr>
          </table>
          <p>
            <button onClick={this.showDeleteConfirm.bind(this)} className="btn btn-danger">Delete</button>
            <Link to="/bills" className="btn btn-primary">Back</Link>
          </p>
          <hr />
        </div>
      )
    }
  }

  render() {

    return (
      <CustomLayout>
        {
          this.renderPurchase()
        }

      </CustomLayout>
    )
  }
}

export default PurchaseShow;