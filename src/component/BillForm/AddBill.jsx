import React, { Component } from 'react';
import { Form, DatePicker, Button, Input, Select, Icon } from 'antd';
import CustomLayout from '../Layouts/CustomLayout'
import moment from 'moment';
import 'antd/dist/antd.css';
import { Row, Col } from 'antd'
// eslint-disable-next-line 
import App from '../../App.css'
import { success, error } from '../../helper/notifications.js'
import { billActions } from '../../actions/BillActions';
import { siteActions } from '../../actions/SiteActions';
import { vendorActions } from '../../actions/VendorActions';
import { categoryActions } from '../../actions/CategoryActions';
import { isAdmin } from '../../helpers/userPolicy';
import Cookie from "js.cookie";

let id = 0
const { Option } = Select;

class AddBill extends Component {
  constructor() {
    super();
    this.state = {
      billForms: [0], categories: [], sites: [], products: [], vendors: [], category: {}, quantity: 0, rate: 0, discount: 0, excise: 0, total: 0
    };
  }

  componentDidMount() {
    siteActions.fetchSites().then(response => {
      this.setState({ sites: response.data });
    }).catch(error => console.log('error', error));

    vendorActions.fetchVendors().then(response => {
      this.setState({ vendors: response.data });
    }).catch(error => console.log('error', error));

    categoryActions.fetchCategories().then(response => {
      this.setState({ categories: response.data })
    }).catch(error => console.log('error', error));
  }

  handleChange(category_id, k) {
    this.props.form.resetFields(`bill_items_attributes[${k}][shipment_attributes][product_id]`)
    categoryActions.fetchProductsCategory(category_id).then(response => {
      this.setState({ products: response.data });
    }).catch(error => console.log('error', error));

    categoryActions.showCategory(category_id).then(response => {
      this.setState({ category: response.data });
      this.props.form.setFieldsValue({ [`bill_items_attributes[${k}][shipment_attributes][unit]`]: response.data.unit })
    }).catch(error => console.log('error', error));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        billActions.addBill(values).then(response => {
          if (response.status === 201) {
            this.props.history.push(`/bills`);
            success('Products successcully Purchased!')
          } else {
            error('Unable to create Purchase!')
          }
        })
      }
    })
  }

  calculateTotal(k, options) {
    const { getFieldValue, setFieldsValue } = this.props.form
    const quantity = parseFloat(options.hasOwnProperty('quantity') ? options.quantity : getFieldValue(`bill_items_attributes[${k}][shipment_attributes][quantity]`))
    const rate = parseFloat(options.hasOwnProperty('rate') ? options.rate : getFieldValue(`bill_items_attributes[${k}][rate]`))
    const excise = parseFloat(options.hasOwnProperty('excise') ? options.excise : getFieldValue(`bill_items_attributes[${k}][excise]`)) || 0
    const discount = parseFloat(options.hasOwnProperty('discount') ? options.discount : getFieldValue(`bill_items_attributes[${k}][discount]`)) || 0
    const vatRate = 1.13
    if (quantity && rate) {
      const total = vatRate * (quantity * rate + excise - discount)
      const roundedTotal = Math.round(total * 100) / 100
      setFieldsValue({ [`bill_items_attributes[${k}][amount]`]: roundedTotal })
    } else {
      setFieldsValue({ [`bill_items_attributes[${k}][amount]`]: 0 })
    }
  }

  additionalFieldValue(event) {
    console.log(event.target.value)
  }

  handleCancel() {
    this.props.history.push('/bills')

  }



  onVolumeChanged(k, options = {}) {
    const { getFieldValue, setFieldsValue } = this.props.form
    const additionalInfoKey = `bill_items_attributes[${k}][shipment_attributes][additional_units]`
    const length = parseInt(options.length || getFieldValue(`${additionalInfoKey}[length]`))
    const breadth = parseInt(options.breadth || getFieldValue(`${additionalInfoKey}[breadth]`))
    const height = parseInt(options.height || getFieldValue(`${additionalInfoKey}[height]`))
    if (length && breadth && height) {
      const quantityKey = `bill_items_attributes[${k}][shipment_attributes][quantity]`
      setFieldsValue({ [quantityKey]: ((length * breadth * height) / 1728) })
      this.setState({ quantity: 0 })
    }
  }

  renderAdditionalInformations(k) {
    const { form } = this.props
    const categoryId = form.getFieldValue(`bill_items_attributes[${k}][shipment_attributes][category_id]`)
    const category =
      this.state
        .categories
        .filter(category => category.id === categoryId)[0]
    const additionalInfoKey = `bill_items_attributes[${k}][shipment_attributes][additional_units]`
    if (category && category.additional_field_required && category.additional_fields) {
      return (
        category.additional_fields.map((field) => {
          return (
            <Col span={4} offset={1} key={field.field}>
              <Form.Item label={field.field}>
                {
                  form.getFieldDecorator(`${additionalInfoKey}[${field.field}]`, {
                  })(
                    <Input type={field.type} onChange={(event) => this.onVolumeChanged(k, { [field.field]: event.target.value })} />
                  )
                }
                {field.unit}
              </Form.Item>
            </Col>
          )
        })
      )
    }
  }

  addForm() {
    const { billForms } = this.state
    const newKey = (billForms.length > 0) ? (billForms[billForms.length - 1] + 1) : 0
    this.setState({ billForms: [...billForms, newKey] })
  }

  removeForm(key) {
    const { billForms } = this.state
    this.setState({
      billForms: billForms.filter(k => k !== key)
    })
  }

  remove(k) {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    if (keys.length === 1) {
      return;
    }
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }

  add() {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
      keys: nextKeys,
    });
  }

  render() {
    const { form } = this.props
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 },
      },
    };

    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => (
      <Form.Item
        {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        label={index === 0 ? 'Chalan' : ''}
        required={false}
        key={k}
      >
        {getFieldDecorator(`chalan_no[${k}]`, {
          validateTrigger: ['onChange', 'onBlur'],
          rules: [
            {
              whitespace: true
            },
          ],
        })(<Input placeholder="Add Chalan Number" style={{ width: '90%', marginRight: 8 }} />)}
        {keys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
          />
        ) : null}
      </Form.Item>
    ));
    return (
      <CustomLayout>
        <br />
        <Col>
          <h3>ADD PURCHASES</h3>
        </Col>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <Row >
            <Col span={4} >
              <div>
                <Form.Item label='Date of Purchase'>
                  {
                    form.getFieldDecorator('date', {
                      rules: [{ required: true, message: 'Please Enter Date' }],
                      initialValue: moment()
                    })(
                      <DatePicker />
                    )
                  }
                </Form.Item>
              </div>
            </Col>

            <Col span={4} offset={1} >
              <div>
                <Form.Item label='Bill Number'>
                  {
                    form.getFieldDecorator('bill_no', {
                      rules: [{ required: true, message: 'Please Enter Bill Number' }],
                    })(
                      <Input
                        placeholder="Bill Number"
                        name='bill_no'
                      />
                    )
                  }
                </Form.Item>
              </div>
            </Col>

            <Col span={4} offset={1}>
              <div>
                <Form.Item label='Vendor'>
                  {
                    form.getFieldDecorator('vendor_id', {
                      rules: [{ required: true, message: 'Please Enter Vendor Name' }],
                    })(
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select a vendor"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {
                          this.state.vendors.map(vendor => {
                            return (
                              <Option value={vendor.id} key={vendor.id}>
                                {vendor.name}
                              </Option>
                            )
                          })
                        }
                      </Select>,
                    )
                  }
                </Form.Item>
              </div>
            </Col>
          </Row>

          {
            this.state.billForms.map((k, index) => {
              return (
                <div key={index}>
                  <fieldset>
                    <Row>
                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label='Category'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][shipment_attributes][category_id]`, {
                                rules: [{ required: true, message: 'Please Enter Category Name' }],
                                onChange: (value) => this.handleChange(value, k)
                              })(
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select Category"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }
                                >
                                  {
                                    this.state.categories.map(category => {
                                      return (
                                        <Option value={category.id} key={category.id}>
                                          {category.name}
                                        </Option>
                                      )
                                    })
                                  }
                                </Select>,
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1}>
                        <div>
                          <Form.Item label='Product'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][shipment_attributes][product_id]`, {
                                rules: [{ required: true, message: 'Please Enter Product Name' }],
                              })(
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select Product"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }
                                >
                                  {
                                    this.state.products.map(product => {
                                      return (
                                        <Option value={product.id} key={product.id}>
                                          {product.name}
                                        </Option>
                                      )
                                    })
                                  }
                                </Select>,
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Description'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][shipment_attributes][description]`, {
                              })(
                                <Input
                                  placeholder="Description"
                                  name='description'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Brand'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][shipment_attributes][brand]`, {
                              })(
                                <Input
                                  placeholder="Brand Name"
                                  name='brand'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>
                    </Row>

                    <Row>
                      {this.renderAdditionalInformations(k)}
                    </Row>

                    <Row>
                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Unit'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][shipment_attributes][unit]`, {
                                initialValue: this.state.category.unit
                              })(
                                <Input
                                  placeholder="Unit"
                                  name='unit'
                                  readOnly='readOnly'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Quantity'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][shipment_attributes][quantity]`, {
                                rules: [{ required: true, message: 'Please Enter Quantity' }],
                                onChange: (event) => this.calculateTotal(k, { quantity: event.target.value })
                              })(
                                <Input
                                  placeholder="Quantity"
                                  name='quantity'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Rate'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][rate]`, {
                                onChange: (event) => this.calculateTotal(k, { rate: event.target.value })
                              })(
                                <Input
                                  placeholder="Rate"
                                  name='rate'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Excise'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][excise]`, {
                                onChange: (event) => this.calculateTotal(k, { excise: event.target.value })
                              })(
                                <Input
                                  placeholder="Excise"
                                  name='excise'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Discount'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][discount]`, {
                                onChange: (event) => this.calculateTotal(k, { discount: event.target.value })
                              })(
                                <Input
                                  placeholder="Discount"
                                  name='discount'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Total Amount'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][amount]`, {
                                initialValue: 0
                              })(
                                <Input
                                  placeholder="Total"
                                  name='total'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>

                      <Col span={4} offset={1} >
                        <div>
                          <Form.Item label='Remarks'>
                            {
                              form.getFieldDecorator(`bill_items_attributes[${k}][shipment_attributes][remarks]`, {
                              })(
                                <Input
                                  placeholder="Remarks"
                                  name='remarks'
                                />
                              )
                            }
                          </Form.Item>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={4} offset={1}>
                        <Button onClick={(e) => this.removeForm(k)} type="danger" >Remove Form</Button>
                      </Col>
                    </Row>
                  </fieldset>
                  <br />
                </div>
              )
            })
          }

          <br />
          <Row>
            <Col span={23}>
              <Button onClick={(e) => this.addForm(e)} type='primary' style={{ float: 'right' }}>Add New Product</Button>
            </Col>
          </Row>

          <Row>
            <Col span={4} >
              <div>
                <Form.Item label='Vehicle Number'>
                  {
                    form.getFieldDecorator('vehicle_no', {
                    })(
                      <Input
                        placeholder="Vehicle Number"
                        name='vehicle_no'
                      />
                    )
                  }
                </Form.Item>
              </div>
            </Col>

            <Col span={6} offset={1} >
              <Form.Item {...formItemLayoutWithOutLabel}>
                <Button type="dashed" onClick={this.add.bind(this)} style={{ width: '90%' }}>
                  <Icon type="plus" />
                  Add Chalan Number
                </Button>
              </Form.Item>
              {formItems}
            </Col>

            <Col span={4} offset={1}>
              <div>
                <Form.Item label='Site'>
                  {
                    form.getFieldDecorator('site_id', {
                      rules: [{ required: true, message: 'Please Enter Site Name' }],
                    })(
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select Product"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {
                          isAdmin() ?
                            this.state.sites.map(site => {
                              return (
                                <Option value={site.id} key={site.id}>
                                  {site.name}
                                </Option>
                              )
                            })
                            :
                            this.state.sites.map(site => {
                              if (site.id === Cookie.get('site_id')) {
                                return (
                                  <Option value={site.id} key={site.id}>
                                    {site.name}
                                  </Option>
                                )
                              }else {
                                return null;
                              }
                            })
                        }
                      </Select>,
                    )
                  }
                </Form.Item>
              </div>
            </Col>
          </Row>

          <Row>
            <Col>
              <div className="btn-group">
                <Button htmlType="submit" className="btn btn-dark">Create Bill</Button>
                &nbsp;
                <Button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</Button>
              </div>
            </Col>
          </Row>
          <br />
        </Form>
      </CustomLayout>
    )
  }
}

export default Form.create()(AddBill)