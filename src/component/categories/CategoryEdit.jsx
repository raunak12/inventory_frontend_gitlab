import React, { Component } from 'react';
import { Form, Input } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { categoryActions } from '../../actions/CategoryActions';

class CategoryEdit extends Component {
  constructor() {
    super();
    this.state = { category: {} };

  }

  componentDidMount() {
    categoryActions.showCategory(this.props.match.params.id).then(response => {
      this.setState({ category: response.data })
  }).catch(error => console.log('error', error));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
    if(!err) {
      categoryActions.updateCategory(this.state.category.id, values).then(response => {
        if(response.status === 200){
          this.props.history.push(`/categories`);
          success('Category was successfully Updated!')
        }else{
          error('Unable to Edit Category!')
        }
      })
      }
    })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCancel() {
    this.props.history.push(`/categories`);
  }


  render() {
    const { form } =  this.props
    console.log(this.state.category.additional_field_required)
    return (
      <CustomLayout>
        <h1>Edit Category</h1>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <Form.Item label= 'Category Name' >
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input category name!'}],
                initialValue: this.state.category.name
              })(
                <Input
                  placeholder='Category Name'
                  name='name'
                />
              )
            }
          </Form.Item>

          <Form.Item label='Unit'>
            {  
              form.getFieldDecorator('unit', {
                rules: [{ required: true, message: 'Please input unit!'}],
                initialValue: this.state.category.unit
              })(
                <Input
                  placeholder='Category Name'
                  name='name'
                />
              )
            }
          </Form.Item>

          <div className="btn-group">
            <button type="submit" className="btn btn-dark">Update Category</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(CategoryEdit);
