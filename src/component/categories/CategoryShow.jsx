import React, { Component } from 'react';
import CustomLayout from '../Layouts/CustomLayout';
import { Link } from 'react-router-dom';
import { success, error } from '../../helper/notifications';
import { categoryActions } from '../../actions/CategoryActions';
import { isAdmin } from '../../helpers/userPolicy'

import { Modal } from 'antd';

const { confirm } = Modal;

class CategoryShow extends Component {
  constructor() {
    super();
    this.state = { category: {} };
  }

  componentDidMount() {
    categoryActions.showCategory(this.props.match.params.id).then(response => {
      this.setState({ category: response.data })
    }).catch(error => console.log('error', error));
  }
  handleDelete() {
    categoryActions.deleteCategory(this.state.category.id).then(response => {
      if (response.status === 204) {
        success('Category was successfully deleted!')
        this.props.history.push(`/categories`)
      } else {
        error('Unable to delete a category!')
      }
    })
  }

  showDeleteConfirm() {
    confirm({
      title: 'Are you sure to delete this Category?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: this.handleDelete.bind(this),
      onCancel: () => {
        console.log('Cancel');
      },
    });
  }

  renderShowAdditionalInformations() {
    const { additional_field } = this.state.category
    if (additional_field) {
      return (
        additional_field.map(field => {
          return (
            <div>
              {
                `Field: ${field.field}, Unit: ${field.unit}, Type: ${field.type}`
              }
            </div>
          )
        })
      )
    }
  }

  render() {
    return (
      <CustomLayout>
        <table className='table table-sm'>
          <tr>
            <td><h5>Category Name: {this.state.category.name}</h5></td>
            <td><h5>Category Unit: {this.state.category.unit}</h5></td>
            <td><h5>Additional Field Required: {`${this.state.category.additional_field_required}`}</h5></td>
            <td><h5>Additional Fields: {this.renderShowAdditionalInformations()}</h5></td>
          </tr>
          <tr>
            <td>
            { isAdmin() ? 
              <p>
                <Link to={`/categories/${this.state.category.id}/edit`} className="btn btn-primary">Edit</Link>
                <button onClick={this.showDeleteConfirm.bind(this)} className="btn btn-danger">Delete</button>
              </p>: 
              ''}
               <Link to="/categories" className="btn btn-primary">Back</Link>
            </td>
          </tr>
        </table>


        <hr />
      </CustomLayout>
    )
  }
}

export default CategoryShow;