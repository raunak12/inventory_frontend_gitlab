import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CustomLayout from '../Layouts/CustomLayout'
import {categoryActions} from '../../actions/CategoryActions'
import { isAdmin} from '../../helpers/userPolicy'
import Table from './table'
import LoadingSpinner from '../../helpers/LoadingSpinner';

class CategoriesList extends Component {
  constructor() {
    super()
    this.state = { categories: [], isLoading: true }
  }

  componentDidMount() {
    categoryActions.fetchCategories().then(response => {
      this.setState({ categories: response.data, isLoading: false })
    }).catch(error => console.log('error', error));
  }

  showTable() {
    const { categories } = this.state
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <Table table_data={categories} /> 
      )
    }
  }

  render() {
    const { categories } = this.state
    if (categories === null) return null
    return (
      <CustomLayout sidebarSelectedKey='categories'>
        <h1>Categories</h1>
        {
        isAdmin()
	  		      ?  <Link to='/categories/new' className="btn btn-outline-primary mar-pad float-right">
          Create Category
        </Link>
	  				  :''
	  	  }
        { this.showTable() }
      </CustomLayout>
    )
  }
}

export default CategoriesList
