import React, { Component } from "react";
import {
  Table,
  Icon,
  Divider,
  Modal,
  Row,
  Col,
  Descriptions,
  Button
} from "antd";
import { Link } from "react-router-dom";
import { success, error } from "../../helper/notifications";
import { categoryActions } from "../../actions/CategoryActions";
import { sortText } from "../../helpers/sort";
import { getColumnSearchProps } from "../../helpers/tableSearch";
import { isAdmin } from '../../helpers/userPolicy'

const { confirm } = Modal;

class CategoryTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      categories: props.table_data,
      category: {}
    };
  }

  handleDelete(categoryId) {
    categoryActions.deleteCategory(categoryId).then(response => {
      if (response.status === 204) {
        categoryActions
          .fetchCategories()
          .then(response => {
            this.setState({ categories: response.data });
          })
          .catch(error => console.log("error", error));
        success("Product was successfully deleted!");
      } else {
        error("Unable to delete Product!");
      }
    });
  }

  showDeleteConfirm(categoryId) {
    confirm({
      className: "show-modal-delete",
      title: "Are you sure to delete this Category?",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk: () => this.handleDelete(categoryId),
      onCancel: () => {
        console.log("Cancel");
      }
    });
  }


  showModal = value => {
    categoryActions.showCategory(value).then(response => {
      this.setState({
        visible: true,
        category: response.data
      });
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  renderShowAdditionalInformations() {
    const { additional_fields } = this.state.category;
    if (additional_fields) {
      return additional_fields.map((field, index) => {
        return (
          <div key={index+1}>
            Field: {index + 1} <br />
            {`Field Name: ${field.field}, Unit: ${field.unit}, Type: ${
              field.type
            }`}
          </div>
        );
      });
    }
  }

  render() {
    const columns = [
      {
        title: "Category Name",
        dataIndex: "name",
        key: "name",
        sorter: (a, b) => sortText(a.name, b.name),
        ...getColumnSearchProps("name", this)
      },
      {
        title: "Unit",
        dataIndex: "unit",
        key: "unit"
      },
      {
        title: "Additional Field Required",
        dataIndex: "additional_field_required",
        render: (text, record) => {
          return record.additional_field_required.toString();
        },
        key: "additional_field_required"
      },
      {
        title: "Additional Fields",
        dataIndex: "additional_fields",
        render: (text, record) => {
          return (
            <div>
              {record.additional_fields.map((field, index) => {
                return (
                  <div key={index}>
                    Field {index + 1}
                    <div> Feild Name: {field.field} </div>
                    <div> Unit: {field.unit} </div>
                    <div> Type: {field.type} </div>
                  </div>
                );
              })}
            </div>
          );
        },
        key: "additional_fields"
      },
      {
        title: "Limiting Quantity",
        dataIndex: "quantity_limit",
        key: "quantity_limit"
      },
      {
        title: "",
        width: "8%",
        key: "action",
        render: (text, record) => {
          return (
            <span>
              {
                isAdmin() ?
                 <Link to={`categories/${record.id}/edit`}>
                  <Icon type="edit" title="edit" />
                </Link> : ''
              }
              <Divider type="vertical" />
              <span style={{color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent'}}>
                <Icon title="View" onClick={() => this.showModal(`${record.id}`)} type="snippets" />
              </span>
              <Divider type="vertical" />
              { 
                isAdmin() ? 
                  <span style={{ color: '#007bff', textDecoration: 'none', backgroundColor: 'transparent' }}>
                    <Icon
                      type="delete"
                      title="Delete"
                      onClick={() => this.showDeleteConfirm(record.id)}
                    />
                  </span> : ''

              }
              
            </span>
          );
        }
      }
    ];
    const categories = this.state.categories;
    const view_table =
      categories && categories.length > 0 ? (
        <div>
          <br />
          <Table columns={columns} dataSource={categories} size="middle" rowKey={column=> column.id} />
        </div>
      ) : (
        "No Data Found"
      );
    return (
      <div>
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Back
            </Button>
          ]}
        >
          <Row>
            <Col span={24} offset={6}>
              <h3> CATEGORY DETAIL</h3>
            </Col>
          </Row>
          <Descriptions title="" bordered>
            <Descriptions.Item label="Category Name">
              {this.state.category.name}
            </Descriptions.Item>
            <Descriptions.Item label="Unit">
              {this.state.category.unit}
            </Descriptions.Item>
            <Descriptions.Item label="Additional Units">
              {this.renderShowAdditionalInformations()}
            </Descriptions.Item>
            <Descriptions.Item label="Limiting Quantity">
              {this.state.category.quantity_limit}
            </Descriptions.Item>
          </Descriptions>
        </Modal>
        {view_table}
      </div>
    );
  }
}
export default CategoryTable;
