import React, { Component } from 'react';
import { Checkbox, Form, Input, Button, Icon } from 'antd';
import CustomLayout from '../Layouts/CustomLayout';
import { success, error } from '../../helper/notifications';
import { categoryActions } from '../../actions/CategoryActions';


let id = 0

class CategoriesAdd extends Component {
  constructor() {
    super();
    this.state = { name: '', unit: ''};
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if(!err) {
        categoryActions.createCategory(values).then(response => {
          if(response.status === 201){
            success('Category was successfully Created!')
            this.props.history.push(`/categories`);
          }else{
            error('Unable to create Category!')
          }
      })
      }
    })
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCancel() {
    this.props.history.push(`/categories`);
  }

  remove(k) {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    id--;
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }

  add() {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
      keys: nextKeys,
    });
  }

  renderAdditionalFields() {
    const { form } = this.props
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 },
      },
    };

    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, _index) => (
      <div key = {`${k}`}>
        { `Additional Field-${k+1}` }
        <Icon
          className="dynamic-delete-button"
          type="minus-circle-o"
          onClick={() => this.remove(k)}
        />
        <Form.Item
          required={false}
          label='Field'
        >
          {
            getFieldDecorator(`additional_fields[${k}][field]`, {
              validateTrigger: ['onChange', 'onBlur'],
              rules: [
                {
                  required: true,
                  whitespace: true,
                  message: "Please input Required field",
                },
              ],
            })(<Input placeholder='Field' />)
          }
        </Form.Item>

        <Form.Item
          required={false}
          label='Type'
        >
          {
            getFieldDecorator(`additional_fields[${k}][type]`, {
              validateTrigger: ['onChange', 'onBlur'],
              rules: [
                {
                  required: true,
                  whitespace: true,
                  message: "Please input Required field",
                },
              ],
            })(<Input placeholder='Field Type' />)
          }
        </Form.Item>

        <Form.Item
          required={false}
          label='Unit'
        >
          {
            getFieldDecorator(`additional_fields[${k}][unit]`, {
              validateTrigger: ['onChange', 'onBlur'],
              rules: [
                {
                  required: true,
                  whitespace: true,
                  message: "Please input Required field",
                },
              ],
            })(<Input placeholder='Unit' />)
          }
        </Form.Item>

      </div>
    ));
    if (form.getFieldValue('additional_field_required')) {
      return (
        <Form.Item>
          {
           <div className="col-lg-4">
            {formItems}
            <Form.Item {...formItemLayoutWithOutLabel}>
              <Button type="dashed" onClick={this.add.bind(this)}>
                <Icon type="plus" />
                Add Additional Fields
              </Button>
            </Form.Item>
          </div>
          }
        </Form.Item>
      )
    }
  }

  render() {
    const {form} = this.props
    return (
      <CustomLayout> 
        <Form onSubmit= { this.handleSubmit.bind(this) }>
          <Form.Item label='Category Name' className='short-text-field'>
            {
              form.getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input category name!' }],
              })(
                <Input
                  placeholder="Category Name"
                  name='name'
                  
                />
              )
            }
          </Form.Item>

          <Form.Item label='Unit' className='short-text-field'>
            {
              form.getFieldDecorator('unit', {
                rules: [{ required: true, message: 'Please input unit name!' }],
              })(
                <Input
                  placeholder="Unit"
                  name='unit'    
                />
              )
            }
          </Form.Item>

           <Form.Item label='Limit Quantity' className='short-text-field'>
            {
              form.getFieldDecorator('quantity_limit', {
                rules: [{ required: true, message: 'Please input quantity limit!' }],
              })(
                <Input
                  placeholder="Quantity Limit"
                  name='unit'    
                />
              )
            }
          </Form.Item>

          <Form.Item label= "Additional Field">
            {
              form.getFieldDecorator('additional_field_required',  {
                initialValue: false,
              })(
                <Checkbox  >Additional Field</Checkbox>
              )
            }
          </Form.Item>

          { this.renderAdditionalFields() }

          <div className='btn-group'>
            <button type="submit" className="btn btn-dark">Create Category</button>
            <button type="button" onClick={this.handleCancel.bind(this)} className="btn btn-secondary">Cancel</button>
          </div>
        </Form>
      </CustomLayout>
    );
  }
}

export default Form.create({})(CategoriesAdd);
