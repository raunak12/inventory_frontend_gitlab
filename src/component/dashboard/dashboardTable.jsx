import React, { Component } from 'react'
import { Table, Tag } from 'antd'

class DashboardTable extends Component {
  render() {
    const columns = [
      {
        title: 'Product Name',
        dataIndex: 'product_name'
      },
      {
        title: 'Quantity',
        dataIndex: 'closing',
        render: closing => <Tag color = 'red'>{closing}</Tag> 
      },
      {
        title: 'Unit',
        dataIndex: 'unit'
      },
      {
        title: 'Limiting Value',
        dataIndex: 'quantity_limit',
        render: quantity_limit => <Tag color = 'green'>{quantity_limit}</Tag> 
      },
    ]
    const data = this.props.data_table
    const view_table =
      data && data.length > 0 ? (
        <div>
          <Table columns={columns} dataSource={data} size="middle" rowKey={column => column.product_name} />
        </div>
      ) : (
        'No Data Found'
      )
    return <div>{view_table}</div>
  }
}

export default DashboardTable
