import React, { Component } from 'react';
import CustomLayout from '../Layouts/CustomLayout';
import DashboardTable from './dashboardTable';
import { reportActions } from '../../actions/ReportActions';
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom'
import LoadingSpinner from '../../helpers/LoadingSpinner';

export default class Dashboard extends Component {
  constructor() {
    super()
    this.state = { 
      outofstockreport: [],
      isLoading: true
    }
  }

  componentDidMount() {
    reportActions.dashboardReport().then( response => {
      this.setState({ outofstockreport: response.data, isLoading: false})
    })
  }

  showTable() {
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <LoadingSpinner />
      )
    } else {
      return (
        <DashboardTable data_table = { this.state.outofstockreport } />
      )
    }
  }

  render() {
    return (
      <CustomLayout sidebarSelectedKey='dashboard'>
        <Row>
          <h2> BUDDHANILKANTHA - OUT OF STOCK PRODUCTS</h2>
        </Row>
        { this.showTable() }
      </CustomLayout>
    );
  };
}



