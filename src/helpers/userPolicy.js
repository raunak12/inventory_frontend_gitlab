import Cookie from "js.cookie";

export const isAdmin = () => {
    return ['admin'].includes(Cookie.get('role'))
  }