import axios from 'axios'
import { baseURL } from '../constants/backendserver.constants.js'
import Cookie from "js.cookie";
import { error } from '../helper/notifications.js';

let customHttp = axios.create({
    baseURL: `${baseURL.URL}${baseURL.PORT}` ,
    validateStatus: function (status) {
        if (status === 401) {
            Cookie.remove('accesstoken', { path: '/' })
            Cookie.remove('client', { path: '/' })
            Cookie.remove('expiry', { path: '/' })
            Cookie.remove('uid', { path: '/' })
            Cookie.remove('role', { path: '/' })
            Cookie.remove('site_id', { path: '/' })
            if (window.location.href.match(/\/login/)){
                error('Invalid Credentials. Please try again.')
            }else{
                window.location.href = '/login'
            }
        }
        return status // default
    }
})

export default customHttp
