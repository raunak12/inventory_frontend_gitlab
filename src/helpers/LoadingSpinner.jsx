import { Spin } from 'antd';
import React, { Component }  from 'react';

class LoadingSpinner extends Component {
  render() {
    return (
      <Spin size = "large" />
    )
  }
}

export default LoadingSpinner 
