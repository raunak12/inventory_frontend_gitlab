import React from 'react';
import { Icon, Button, Input } from 'antd';
import Highlighter from 'react-highlight-words';

export const getColumnSearchProps = (dataIndex, tableComponent) => ({
  filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
    <div style={{ padding: 8 }}>
      <Input
        ref={node => {
          tableComponent.searchInput = node;
        }}
        placeholder={`Search ${dataIndex}`}
        value={selectedKeys[0]}
        onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
        onPressEnter={() => handleSearch(selectedKeys, confirm, tableComponent)}
        style={{ width: 188, marginBottom: 8, display: 'block' }}
      />
      <Button
        type="primary"
        onClick={() => handleSearch(selectedKeys, confirm, tableComponent)}
        icon="search"
        size="small"
        style={{ width: 90, marginRight: 8 }}
      >
        Search
      </Button>
      <Button onClick={() => handleReset(clearFilters, tableComponent)} size="small" style={{ width: 90 }}>
        Reset
      </Button>
    </div>
  ),
  filterIcon: filtered => (
    <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
  ),
  onFilter: (value, record) => {
    const fieldValue = record[dataIndex]
    if (fieldValue) {
      return fieldValue.toString()
                       .toLowerCase()
                       .includes(value.toLowerCase())
    }
  },
  onFilterDropdownVisibleChange: visible => {
    if (visible) {
      setTimeout(() => tableComponent.searchInput.select());
    }
  },
  render: text => (
    <Highlighter
      highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
      searchWords={[tableComponent.state.searchText]}
      autoEscape
      textToHighlight={text && text.toString()}
    />
  ),
});

const handleSearch = (selectedKeys, confirm, tableComponent) => {
  confirm();
  tableComponent.setState({ searchText: selectedKeys[0] });
};

const handleReset = (clearFilters, tableComponent) => {
  clearFilters();
  tableComponent.setState({ searchText: '' });
};
