export const sortText = (text1, text2) => {
  const lowerCaseText1 = text1 && text1.toLowerCase()
  const lowerCaseText2 = text2 && text2.toLowerCase() 
  if(lowerCaseText1 < lowerCaseText2) {
    return -1;
  }
  else if(lowerCaseText1 > lowerCaseText2) {
    return 1;
  }
  else {
    return 0;
  }
}

export const sortDate = (date1, date2) => {
  const dateObject1 = (new Date(date1)).getTime()
  const dateObject2 = (new Date(date2)).getTime()
  if(dateObject1 < dateObject2) {
    return -1;
  }
  else if(dateObject1 > dateObject2) {
    return 1;
  }
  else {
    return 0;
  }
}

