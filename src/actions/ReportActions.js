import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const reportActions = {
    productReport,
    stockReport,
    vendorReport,
    contractorReport,
    dashboardReport
}



function productReport(start_date, end_date, product_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/reports?start_date=${start_date}&end_date=${end_date}&product_id=${product_id}`,
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function stockReport(start_date, end_date, product_id, site_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/reports?start_date=${start_date}&end_date=${end_date}&product_id=${product_id}&site_id=${site_id}`,
    }

    return customHttp(requestOptions).then(response => {
        return response
    })
}

function vendorReport(start_date, end_date, product_id, vendor_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/vendor_report?start_date=${start_date}&end_date=${end_date}&product_id=${product_id}&vendor_id=${vendor_id}`,
    }

    return customHttp(requestOptions).then(response => {
        return response
    })
}

function contractorReport(start_date, end_date, product_id, contractor_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/contractor_report?start_date=${start_date}&end_date=${end_date}&product_id=${product_id}&contractor_id=${contractor_id}`, 
    }

    return customHttp(requestOptions).then(response => {
        return response
    })
}

function dashboardReport() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/reports_quantity_limit?site_id=6`,
    }

    return customHttp(requestOptions).then(response => {
        return response
    })
}
