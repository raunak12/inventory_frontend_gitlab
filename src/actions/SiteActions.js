import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const siteActions = {
  fetchSites,
  createSite,
  showSite,
  updateSite,
  deleteSite
};

function fetchSites() {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
    url: `/sites.json`,
  };
  return customHttp(requestOptions).then(
    response => {
      return response;
    }
  );
}
function createSite(values) {
  const requestOptions = {
    method: "POST",
    headers: authHeader(),
    url: `/sites`,
    data: values
  };
  return customHttp( requestOptions).then(
    response => {
      return response;
    }
  );
}

function showSite(site_id) {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
    url: `/sites/${site_id}`,
  };
  return customHttp(requestOptions).then(
    response => {
      return response;
    }
  );
}

function updateSite(site_id, values) {
  const requestOptions = {
    method: "PATCH",
    headers: authHeader(),
    url: `/sites/${site_id}`,
    data: values
  };
  return customHttp(
    
    requestOptions
  ).then(response => {
    return response;
  });
}

function deleteSite(site_id) {
  const requestOptions = {
    method: "DELETE",
    headers: authHeader(),
    url: `/sites/${site_id}`,
  };
  return customHttp(
    requestOptions
  ).then(response => {
    return response;
  });
}
