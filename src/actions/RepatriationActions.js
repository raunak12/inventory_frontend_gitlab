import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const repatriationActions = {
    fetchRepatriations,
    showRepatriation,
    createRepatriation,
    updateRepatriation,
    destroyRepatriation,
    fetchRepatriatedItems,
    showRepatriatedItem
}

function fetchRepatriations(){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/repatriations.json`,
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function showRepatriation(repatriation_id){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/repatriations/${repatriation_id}`,
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function createRepatriation(values){
    const requestOptions = {
      method: 'POST',
      headers: authHeader(),
      url: `/repatriations.json`,
      data: values

    }
    return customHttp(requestOptions).then(response => {
      return response
    })
  }

function updateRepatriation(repatriation_id, values){
    const requestOptions = {
        method: 'PATCH',
        headers: authHeader(),
        url: `/repatriations/${repatriation_id}`,
        data: values
    }
    return customHttp( requestOptions).then(response => {
        return response
    })
}

function destroyRepatriation(contractor_id){
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
        url: `/contractors/${contractor_id}`,
    }
    return customHttp( requestOptions).then(response => {
        return response
    })
}

function fetchRepatriatedItems(){
        const requestOptions = {
            Method: 'GET',
            headers: authHeader(),
            url: `/repatriated_items.json`, 
        }
        return customHttp(requestOptions).then(response => {
            return response
        })
}

function showRepatriatedItem(repatriated_item_id, callback){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/repatriated_items/${repatriated_item_id}`,
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}