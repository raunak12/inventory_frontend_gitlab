import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const shipmentActions = {
    deleteShipment
}

function deleteShipment(shipment_id){
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
        url: `/shipments/${shipment_id}`,
      }
    return customHttp(requestOptions).then(response => {
        return response
      })
}