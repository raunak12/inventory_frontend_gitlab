import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const categoryActions = {
    fetchCategories,
    createCategory,
    updateCategory,
    fetchProductsCategory,
    showCategory,
    deleteCategory
};


function fetchCategories() {
    const requestOptions = {
      method: 'GET',
      headers: authHeader(),
      url: `/categories.json`,
    }
    return customHttp( requestOptions).then(response => {
      return response
    })
  }

  function createCategory(values){
    const requestOptions = {
      method: 'POST',
      headers: authHeader(),
      url: `/categories.json`,
      data: values

    }
    return customHttp( requestOptions).then(response => {
      return response
    })
  }

  function updateCategory(category_id, values){
    const requestOptions = {
      method: 'PATCH',
      headers: authHeader(),
      url: `/categories/${category_id}`,
      data: values
    }
    return customHttp(requestOptions).then(response => {
      return response
    })
  }
 
  function fetchProductsCategory(category_id){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/categories/${category_id}/products`,
      }
      return customHttp( requestOptions).then(response => {
        return response
      })
  }

  function showCategory(category_id){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/categories/${category_id}`,
      }
       return customHttp( requestOptions).then(response => {
        return response
      })
  }

  function deleteCategory(category_id){
    const requestOptions = {
      method: 'DELETE',
      headers: authHeader(),
      url: `/categories/${category_id}`,
    }
    return customHttp( requestOptions).then(response => {
      return response
    })
  }

 
