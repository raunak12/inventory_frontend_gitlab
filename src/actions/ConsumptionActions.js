import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const consumptionActions = {
  fetchConsumedItems,
  createConsumption,
  showConsumption,
  updateConsumption,
  showConsumedItem
};

function fetchConsumedItems() {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
    url: `/consumed_items.json`,
  };
  return customHttp(
    requestOptions
  ).then(response => {
    return response;
  });
}

function createConsumption(values) {
  const requestOptions = {
    method: "POST",
    headers: authHeader(),
    url: `/consumptions`,
    data: values
  };
  return customHttp(
    requestOptions
  ).then(response => {
    return response;
  });
}

function showConsumption(consumption_id) {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
    url: `/consumptions/${consumption_id}`,
  };
  return customHttp(
    requestOptions
  ).then(response => {
    return response;
  });
}

function updateConsumption(consumption_id, values) {
  const requestOptions = {
    method: "PATCH",
    headers: authHeader(),
    url: `/consumptions/${consumption_id}`,
    data: values
  };
  return customHttp(
    requestOptions
  ).then(response => {
    return response;
  });
}

function showConsumedItem(consumed_item_id) {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
    url: `/consumed_items/${consumed_item_id}`,
  };
  return customHttp(
    requestOptions
  ).then(response => {
    return response;
  });
}
