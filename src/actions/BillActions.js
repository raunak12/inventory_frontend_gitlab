import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const billActions = {
  fetchBillItems,
  showPurchaseProduct,
  addBill,
  updatePurchaseProduct,
  deletePurchaseProduct,
  editBill,
}

function fetchBillItems() {
  const requestOptions = {
    Method: 'GET',
    headers: authHeader(),
    url : `/bill_items.json`
  }
  return customHttp(requestOptions).then(response => {
    return response
  })
}

function showPurchaseProduct(bill_items_id) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
    url : `/bill_items/${bill_items_id}`
  }
   return customHttp(requestOptions).then(response => {
    return response
  })
}

function addBill(values) {
  const requestOptions = {
    method: 'POST',
    headers: authHeader(),
    url : `/bills`,
    data: values
  }
  return customHttp(`/bills`, requestOptions).then(response => {
    return response
  })
}

function updatePurchaseProduct(billId, values) {
  const requestOptions = {
    method: 'PUT',
    headers: authHeader(),
    url: `/bills/${billId}`,
    data: values
  }
  return customHttp(requestOptions).then(response => {
    return response
  })
}



function deletePurchaseProduct(shipment_id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader(),
    url: `/shipments/${shipment_id}`,
  }
  return customHttp(requestOptions).then(response => {
    return response
  })
}

function editBill(bill_id) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
    url: `/bills/${bill_id}`
  }
  return customHttp( requestOptions).then(response => {
    return response
  })
}