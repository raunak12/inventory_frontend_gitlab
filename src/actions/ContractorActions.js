import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const contractorActions = {
    fetchContractors,
    createContractor,
    showContractor,
    updateContractor,
    deleteContractor
}

function fetchContractors(){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/contractors.json`,
        }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function createContractor(values){
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        url: `/contractors`,
        data: values
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function showContractor(contractor_id){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/contractors/${contractor_id}`,
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function updateContractor(contractor_id, values){
    const requestOptions = {
        method: 'PATCH',
        headers: authHeader(),
        url: `/contractors/${contractor_id}`,
        data: values
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function deleteContractor(contractor_id){
    const requestOptions = {
      method: 'DELETE',
      headers: authHeader(),
      url: `/contractors/${contractor_id}`,
    }
    return customHttp(requestOptions).then(response => {
      return response
    })
  }

