import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const productActions = {
    fetchproducts,
    showProduct,
    createProduct,
    updateProduct,
    deleteProduct,

};


function fetchproducts() {
    const requestOptions = {
      method: 'GET',
      headers: authHeader(),
      url: `/products.json`,
    }
    return customHttp( requestOptions).then(response => {
      return response
    })
  }

  function showProduct(product_id){
    const requestOptions = {
      method: 'GET',
      headers: authHeader(),
      url: `/products/${product_id}`,
    }
    return customHttp(requestOptions).then(response => {
      return response
    })
  }

  function createProduct(values){
    const requestOptions = {
      method: 'POST',
      headers: authHeader(),
      url: `/products`,
      data: values
    }
    return customHttp(requestOptions).then(response => {
      return response
    })
  }

  function updateProduct(product_id, values){
    const requestOptions = {
      method: 'PATCH',
      headers: authHeader(),
      url: `/products/${product_id}`,
      data: values
    }
    return customHttp(requestOptions).then(response => {
      return response
    })
  }

  function deleteProduct(product_id){
    const requestOptions = {
      method: 'DELETE',
      headers: authHeader(),
      url: `/products/${product_id}`,
    }
    return customHttp(requestOptions).then(response => {
      return response
    })
  }
