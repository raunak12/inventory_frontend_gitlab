import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const dispatchActions = {
  fetchDispatchedItem,
  fetchDispatched,
  createDistatch,
  updateDispatch,
  removeProductForm,
  dispatches
};

function fetchDispatchedItem() {
  const requestOptions = {
    method: "get",
    headers: authHeader(),
    url: `/dispatched_items.json`,
  };
  return customHttp(
    
    requestOptions
  ).then(response => {
    return response;
  });
}

function fetchDispatched(dispatched_items_id, callback) {
  const requestOptions = {
    method: "get",
    headers: authHeader(),
    url: `/dispatched_items/${dispatched_items_id}`,
  };
  return customHttp(
    
    requestOptions
  ).then(response => {
    return response;
  });
}

function updateDispatch(dispatched_id, value) {
  const requestOptions = {
    method: "PATCH",
    headers: authHeader(),
    url: `/dispatches/${dispatched_id}`,
    data: JSON.stringify(value)
  };
  return customHttp(
   
    requestOptions
  ).then(response => {
    return response;
  });
}

function removeProductForm(shipment_id) {
  const requestOptions = {
    method: "DELETE",
    headers: authHeader(),
    url: `/shipments/${shipment_id}`,
  };
  return customHttp(
    requestOptions
  ).then(response => {
    return response;
  });
}

function createDistatch(values) {
  const requestOptions = {
    method: "POST",
    headers: authHeader(),
    url: `/dispatches`,
    data: JSON.stringify(values)
  };
  return customHttp(requestOptions).then(
    response => {
      return response;
    }
  );
}

function dispatches(dispatch_id) {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
    url: `/dispatches/${dispatch_id}`,
  };
  return customHttp(
    requestOptions
  ).then(response => {
    return response;
  });
}
