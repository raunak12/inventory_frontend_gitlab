import customHttp from '../helpers/customHttp'
import { authHeader } from "../helpers/auth-header";

export const vendorActions = {
    fetchVendors,
    createVendor,
    showVendor,
    updateVendor,
    deleteVendor
}

function fetchVendors(){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/vendors.json`,
    }
    return customHttp( requestOptions).then(response => {
        return response
    })
}

function createVendor(values){
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        url: `/vendors`,
        data: values
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function showVendor(vendor_id){
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `/vendors/${vendor_id}`,
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function updateVendor(vendor_id, values){
    const requestOptions = {
        method: 'PATCH',
        headers: authHeader(),
        url: `/vendors/${vendor_id}`,
        data: values
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}

function deleteVendor(vendor_id){
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
        url: `/vendors/${vendor_id}`,
    }
    return customHttp(requestOptions).then(response => {
        return response
    })
}