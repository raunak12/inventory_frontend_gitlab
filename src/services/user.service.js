import { authHeader } from '../helpers/auth-header'
import Cookie from "js.cookie";
import customHttp from '../helpers/customHttp'
import { success, error } from '../helper/notifications.js';

export const userService = {
  login,
  logout
}

function login(email, password ,historyPush) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify({email, password})
  }
  return customHttp(`/auth/sign_in`, requestOptions).then(response => {
    if(response.status === 200){
    Cookie.set('accesstoken', response.headers['access-token'],{ expires: 7, path: '/' })
    Cookie.set('client', response.headers['client'],{ expires: 7, path: '/' })
    Cookie.set('expiry', response.headers['expiry'],{ expires: 7, path: '/' })
    Cookie.set('uid', response.headers['uid'],{ expires: 7, path: '/' })
    Cookie.set('site_id', response.data.data['site_id'],{ expires: 7, path: '/' })
    Cookie.set('role', response.data.data.role , { expires: 7,path: '/' })
    authHeader(response)
    historyPush.push('/')
    success('Login Sucessfull')
    }else{
      error('Invalid!!')
    }
  })
}


function logout() {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader(),
    url: `/auth/sign_out`,
  }
  Cookie.remove('accesstoken',{ path: '/'})
  Cookie.remove('client',{ path: '/'})
  Cookie.remove('expiry',{ path: '/'})
  Cookie.remove('uid',{ path: '/'})
  Cookie.remove('role', {path: '/'})
  return customHttp(requestOptions).then(response => {
    if(response.status === 200){
      window.location.href ='/login'
    }
  })
}

    
